package android.app.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.LayoutRes;
import android.support.v4.content.ContextCompat;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.OpenMtaSDK.R;
import com.blankj.utilcode.util.LogUtils;
import com.nil.sdk.ui.BaseAppCompatActivity;
import com.nil.sdk.ui.BaseUtils;
import com.nil.sdk.utils.AppUtils;
import com.nil.sdk.utils.AskPermissionUtil;
import com.nil.sdk.utils.PropertiesUtil;
import com.nil.sdk.utils.StringUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 前往授权前的中转界面<br>
*/
public class PermissionTransferUI extends BaseAppCompatActivity implements View.OnClickListener {
    private final static String TAG = PermissionTransferUI.class.getSimpleName();
    private ImageView ivLogo;
    private TextView tvContent;
    private TextView tvOK;
    private TextView tvCancel;
    private AskPermissionUtil.DefFullCallback defFullCallback;

    private PtVO pt;    //中转界面实体类
    private String [] permissions;  //权限列表
    public static AskPermissionUtil.DefFullCallback sCallback; //授权回调：授权成功，执行某操作。

    /**
     * 中转界面实体类<br>
     */
    public static class PtVO implements Serializable{
        public final static int DEF_COLOR = 0x123456;  //0x00000000-0xffFFFFFF
        private final static String TAG = PermissionTransferUI.class.getSimpleName();
        private static final long serialVersionUID = -7249788985638238765L;
        private String title;
        private int imgLogoID = -1;
        private int iImageTintColor = -1, iBgResID = -1, iBgColor = DEF_COLOR, iVisibility = -1;
        private ImageView.ScaleType iScaleType; //缩放方式

        private String content, ok, cancel;

        private float cTextSize = -1;    //字体大小
        private int cTypeface = -1;  //字体样式：正常、加粗、斜体
        private int cTextColor = DEF_COLOR, cBgColor = DEF_COLOR, cGravity = -1, cVisibility = -1;  //content文本配置

        private String askMsgTemplate;  //AskPermissionUtil中消息的模板
        private String askTitle, askMsg, askOk, askCancel; //AskPermissionUtil对话框文本配置

        private boolean isFinish = true;    //授权完成后，默认finish自己
        private int layoutResID;    //布局ID
        private boolean isGrantedSucTip = true; //授权成功提示

        @Override
        public String toString() {
            return "PtVO{" +
                    "title='" + title + '\'' +
                    ", imgLogoID=" + imgLogoID +
                    ", iImageTintColor=" + iImageTintColor +
                    ", iBgID=" + iBgResID +
                    ", iBgColor=" + iBgColor +
                    ", iVisibility=" + iVisibility +
                    ", iScaleType=" + iScaleType +
                    ", content='" + content + '\'' +
                    ", ok='" + ok + '\'' +
                    ", cancel='" + cancel + '\'' +
                    ", cTextSize=" + cTextSize +
                    ", cTypeface=" + cTypeface +
                    ", cTextColor=" + cTextColor +
                    ", cBgColor=" + cBgColor +
                    ", cGravity=" + cGravity +
                    ", cVisibility=" + cVisibility +
                    ", askMsgTemplate='" + askMsgTemplate + '\'' +
                    ", askTitle='" + askTitle + '\'' +
                    ", askMsg='" + askMsg + '\'' +
                    ", askOk='" + askOk + '\'' +
                    ", askCancel='" + askCancel + '\'' +
                    ", isFinish=" + isFinish +
                    ", layoutResID=" + layoutResID +
                    ", isGrantedSucTip=" + isGrantedSucTip +
                    '}';
        }

        public String getTitle() {
            return title;
        }
        public PtVO setTitle(Object title) {
            this.title = BaseUtils.getS(title);
            return this;
        }
        public String getContent() {
            return content;
        }
        public PtVO setContent(Object content) {
            this.content = BaseUtils.getS(content);
            return this;
        }
        public String getOk() {
            return ok;
        }
        public PtVO setOk(Object ok) {
            this.ok = BaseUtils.getS(ok);
            return this;
        }
        public String getCancel() {
            return cancel;
        }
        public PtVO setCancel(Object cancel) {
            this.cancel = BaseUtils.getS(cancel);
            return this;
        }
        public int getImgLogoID() {
            return imgLogoID;
        }
        public PtVO setImgLogoID(@DrawableRes int imgLogoID) {
            this.imgLogoID = imgLogoID;
            return this;
        }
        public String getAskMsgTemplate() {
            return askMsgTemplate;
        }
        public PtVO setAskMsgTemplate(Object askMsgTemplate) {
            this.askMsgTemplate = BaseUtils.getS(askMsgTemplate);
            return this;
        }
        public String getAskTitle() {
            return askTitle;
        }
        public PtVO setAskTitle(Object askTitle) {
            this.askTitle = BaseUtils.getS(askTitle);
            return this;
        }
        public String getAskMsg() {
            return askMsg;
        }
        public PtVO setAskMsg(Object askMsg) {
            this.askMsg = BaseUtils.getS(askMsg);
            return this;
        }
        public String getAskOk() {
            return askOk;
        }
        public PtVO setAskOk(Object askOk) {
            this.askOk = BaseUtils.getS(askOk);
            return this;
        }
        public String getAskCancel() {
            return askCancel;
        }
        public PtVO setAskCancel(String askCancel) {
            this.askCancel = BaseUtils.getS(askCancel);
            return this;
        }

        public float getcTextSize() {
            return cTextSize;
        }
        public PtVO setcTextSize(int cTextSize) {
            this.cTextSize = cTextSize;
            return this;
        }
        public int getcTypeface() {
            return cTypeface;
        }
        public PtVO setcTypeface(int cTypeface) {
            this.cTypeface = cTypeface;
            return this;
        }
        public int getcTextColor() {
            return cTextColor;
        }
        public PtVO setcTextColor(@ColorInt int cTextColor) {
            this.cTextColor = cTextColor;
            return this;
        }
        public int getcBgColor() {
            return cBgColor;
        }
        public PtVO setcBgColor(@ColorInt int cBgColor) {
            this.cBgColor = cBgColor;
            return this;
        }
        public int getcGravity() {
            return cGravity;
        }
        public PtVO setcGravity(int cGravity) {
            this.cGravity = cGravity;
            return this;
        }
        public boolean isFinish() {
            return isFinish;
        }
        public PtVO setFinish(boolean finish) {
            isFinish = finish;
            return this;
        }
        public int getLayoutResID() {
            return layoutResID;
        }
        public PtVO setLayoutResID(@LayoutRes int layoutResID) {
            this.layoutResID = layoutResID;
            return this;
        }
        public boolean isGrantedSucTip() {
            return isGrantedSucTip;
        }
        public PtVO setGrantedSucTip(boolean grantedSucTip) {
            isGrantedSucTip = grantedSucTip;
            return this;
        }
        public int getiImageTintColor() {
            return iImageTintColor;
        }
        public PtVO setiImageTintColor(@ColorRes int iImageTintColor) {
            this.iImageTintColor = iImageTintColor;
            return this;
        }
        public int getiBgResID() {
            return iBgResID;
        }
        public PtVO setiBgResID(@DrawableRes int iBgResID) {
            this.iBgResID = iBgResID;
            return this;
        }
        public int getiVisibility() {
            return iVisibility;
        }
        public PtVO setiVisibility(int iVisibility) {
            this.iVisibility = iVisibility;
            return this;
        }
        public ImageView.ScaleType getiScaleType() {
            return iScaleType;
        }
        public PtVO setiScaleType(ImageView.ScaleType iScaleType) {
            this.iScaleType = iScaleType;
            return this;
        }
        public int getcVisibility() {
            return cVisibility;
        }
        public PtVO setcVisibility(int cVisibility) {
            this.cVisibility = cVisibility;
            return this;
        }
        public int getiBgColor() {
            return iBgColor;
        }
        public PtVO setiBgColor(@ColorInt int iBgColor) {
            this.iBgColor = iBgColor;
            return this;
        }
    }

    public static void start(Activity act, String[] permissions,
                             AskPermissionUtil.DefFullCallback callback) {
        start(act, new PtVO(), permissions, callback);
    }
    public static void start(Activity act, PtVO pt, String[] permissions,
                             AskPermissionUtil.DefFullCallback callback) {
        try {
            if (AppUtils.hasPermission(permissions) && callback != null) {
                callback.onGranted(Arrays.asList(permissions));
            } else {
                Intent it = new Intent(act, PermissionTransferUI.class);
                it.putExtra(PtVO.TAG, pt);
                it.putExtra("permissions", permissions);
                sCallback = callback;
                BaseUtils.startActivity(act,it);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //获取传值：
        permissions = getIntent().getStringArrayExtra("permissions");
        pt = (PtVO) getIntent().getSerializableExtra(PtVO.TAG);
        if(pt == null){
            LogUtils.wTag(TAG, "PtVO is null...");
            pt = new PtVO();
        }

        int idi = pt.getLayoutResID();
        if (idi > 0) {
            setContentView(idi);
        } else {
            setContentView(R.layout.xmta_permission_transfer_ui);
        }
        setDisplayHomeAsUpEnabled(true);
        setElevation();

        //初始化控件：
        ivLogo = findViewById(R.id.img_logo);
        tvContent = findViewById(R.id.tv_content);
        tvOK = findViewById(R.id.tv_ok);
        tvCancel = findViewById(R.id.tv_cancel);
        tvOK.setOnClickListener(this);
        tvCancel.setOnClickListener(this);

        //获取序列化值，进行赋值：
        String t = pt.getTitle();
        if (StringUtils.noNullStr(t)) setTitle(t);

        t = pt.getContent();
        if (t != null) tvContent.setText(t);

        t = pt.getOk();
        if (StringUtils.noNullStr(t)) tvOK.setText(t);

        t = pt.getCancel();
        if (StringUtils.noNullStr(t)) tvCancel.setText(t);

        /*ivLogo信息配置...begin*/
        idi = pt.getImgLogoID();
        if (idi != -1) ivLogo.setImageResource(idi);

        idi = pt.getiImageTintColor();
        if (idi != -1 && Build.VERSION.SDK_INT >= 21) {
            ivLogo.setImageTintList(ContextCompat.getColorStateList(this, idi));
        }

        idi = pt.getiBgResID();
        if (idi != -1) ivLogo.setBackgroundResource(idi);
        idi = pt.getiBgColor();
        if (idi != PtVO.DEF_COLOR) ivLogo.setBackgroundColor(idi);

        ImageView.ScaleType type = pt.getiScaleType();
        if(type != null) ivLogo.setScaleType(type);

        idi = pt.getiVisibility();
        if (idi != -1) ivLogo.setVisibility(idi);//VISIBLE = 0
        /*tvContent信息配置...end*/

        /*ivLogo信息配置...begin*/
        float idf = pt.getcTextSize();
        if (idf > -1) tvContent.setTextSize(TypedValue.COMPLEX_UNIT_SP, idf);

        idi = pt.getcTypeface();
        if (idi > -1) tvContent.setTypeface(null/*使用默认字体*/, idi);//NORMAL = 0

        idi = pt.getcTextColor();
        if (idi != PtVO.DEF_COLOR) tvContent.setTextColor(idi);//TRANSPARENT = 0，color可能为负数

        idi = pt.getcBgColor();
        if (idi != PtVO.DEF_COLOR) tvContent.setBackgroundColor(idi);//TRANSPARENT = 0

        idi = pt.getcGravity();
        if (idi != -1) tvContent.setGravity(idi);//NO_GRAVITY = 0

        idi = pt.getcVisibility();
        if (idi != -1) tvContent.setVisibility(idi);//VISIBLE = 0
        /*tvContent信息配置...end*/

        defFullCallback = new AskPermissionUtil.DefFullCallback() {
            @Override
            public void onGranted(List<String> permissionsGranted) {
                //弹出授权成功提示：
                if(pt != null && pt.isGrantedSucTip()) BaseUtils.popMyToast(R.string.ask_permission_granted_sucess_tip);

                //先 finish 在 startActivity 是比较安全的
                if(pt != null && pt.isFinish()) PermissionTransferUI.this.finish();

                //执行回调操作：
                if(sCallback != null) sCallback.onGranted(permissionsGranted);
            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();

        //授权完成，直接中转回调方法：
        if (AppUtils.hasPermission(permissions) && defFullCallback != null) {
            defFullCallback.onGranted(Arrays.asList(permissions));
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        sCallback = null;
        defFullCallback = null;
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.tv_ok){
            //根据permissions生成权限名列表：
            String msg = StringUtils.getValue(pt.getAskMsgTemplate(),
                    getResources().getString(R.string.xmat_permission_ask_msg_template_default));
            msg = msg.replace("${per_list}", getPerValues(permissions, "、"));

            //弹出授权对话框：
            AskPermissionUtil.popPermissionTipDlg(this,
                    pt.getAskTitle(), StringUtils.getValue(pt.getAskMsg(), msg), pt.getAskOk(), pt.getAskCancel(),
                    permissions, defFullCallback);
        }else if(v.getId() == R.id.tv_cancel){
            finish();
        }
    }

    public String getPerValues(String[] pers, String split){
        if(pers == null || pers.length < 1) return "";

        //权限列表利用Map进行去重：
        Map<String, String> map = new HashMap<>();
        for(String p : pers) {
            String v = PropertiesUtil.getInstance().getValue(R.raw.xmta_permission_code, p);
            map.put(v, p);
        }
        ArrayList<String> vs = new ArrayList<>(map.values());

        if(vs.size() <= 1){
            return PropertiesUtil.getInstance().getValue(R.raw.xmta_permission_code, vs.get(0))+"权限";
        } else {
            StringBuffer buf = new StringBuffer();
            for (String p : vs) {
                String v = PropertiesUtil.getInstance().getValue(R.raw.xmta_permission_code, p);
                buf.append(v + "权限").append(AppUtils.hasPermission(p) ? "[已授权]" : "[未授权]");
                buf.append(split);
            }
            buf.append("##"); //用于去除末尾多余的split
            return buf.toString().replace(split+"##", "");
        }
    }
}