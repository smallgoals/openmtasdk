package android.app.ui;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.OpenMtaSDK.R;
import com.blankj.utilcode.util.ClipboardUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.cazaea.sweetalert.SweetAlertDialog;
import com.nil.sdk.ui.BaseAppCompatActivity;
import com.nil.sdk.ui.BaseUtils;
import com.nil.sdk.utils.ACacheUtils;
import com.nil.sdk.utils.AppUtils;
import com.nil.sdk.utils.AutoUpdateUtil;
import com.nil.sdk.utils.StringUtils;
import com.nil.vvv.utils.AdSwitchUtils;

/**
 * 版本限制界面<br>
 */
public class VersionLimitUI extends BaseAppCompatActivity implements View.OnClickListener {
    Button btn_version_limit_check_update,btn_version_limit_contact_us,btn_version_limit_app_code,btn_version_limit_close_app;
    TextView tv_appInfo;

    public static void start(Activity act) {
        try {
            //清除栈内全部Activity(跳转至主界面，使用FLAG_ACTIVITY_CLEAR_TOP标识)
            Intent startMain = new Intent(act, VersionLimitUI.class);
            startMain.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            BaseUtils.startActivity(act,startMain);
            act.finish();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.xmta_version_limit);
        hideActionBar();

        btn_version_limit_check_update = findViewById(R.id.btn_version_limit_check_update);
        btn_version_limit_contact_us = findViewById(R.id.btn_version_limit_contact_us);
        btn_version_limit_app_code = findViewById(R.id.btn_version_limit_app_code);
        btn_version_limit_close_app = findViewById(R.id.btn_version_limit_close_app);

        btn_version_limit_check_update.setOnClickListener(this);
        btn_version_limit_contact_us.setOnClickListener(this);
        btn_version_limit_app_code.setOnClickListener(this);
        btn_version_limit_close_app.setOnClickListener(this);

        tv_appInfo = findViewById(R.id.tv_appInfo);
        tv_appInfo.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                BaseUtils.showInnerLog(getActivity());
                return true;
            }
        });
        tv_appInfo.setText(String.format("%s | Version %s", AppUtils.getAppName(getActivity()), AppUtils.getVersionName(getActivity())));
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btn_version_limit_check_update){
            AutoUpdateUtil.checkUpdate(getActivity(), false);
        }else if(v.getId() == R.id.btn_version_limit_contact_us){
            String defQQ = "1283705915";
            ClipboardUtils.copyText(StringUtils.getValue(AdSwitchUtils.Vs.kf_qq.value, defQQ));
            ToastUtils.showLong("QQ号已经复制成功，请到手机QQ添加好友咨询");
        }else if(v.getId() == R.id.btn_version_limit_app_code){
            new SweetAlertDialog(getActivity(), SweetAlertDialog.NORMAL_TYPE)
                .setTitleText("机器码")
                .setContentText("联系客服时请告之你的机器码以方便帮你解决问题:\n" + ACacheUtils.getCode())
                .setConfirmText("复制机器码")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        ClipboardManager myClipboard;
                        myClipboard = (ClipboardManager) getActivity().getSystemService(Activity.CLIPBOARD_SERVICE);
                        ClipData myClip;
                        String text = ACacheUtils.getCode();
                        myClip = ClipData.newPlainText("text", text);
                        myClipboard.setPrimaryClip(myClip);

                        ToastUtils.showLong("机器码已经复制");
                        sweetAlertDialog.dismiss();
                    }
                })
                .setCancelText("取消")
                .show();
        }else if(v.getId() == R.id.btn_version_limit_close_app){
            onBackPressed();
        }
    }

    @Override
    public void onBackPressed() {
        BaseUtils.onExit(this);
    }
}
