package android.app.ui;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.OpenMtaSDK.R;
import com.nil.sdk.ui.BaseFragment;
import com.nil.sdk.ui.BaseUtils;
import com.nil.sdk.utils.AppUtils;
import com.nil.sdk.utils.OpenMarket;

public class AboutFg extends BaseFragment implements OnClickListener {
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.xmta_about_ui, container, false);
		show(v);
		return v;
	}

	protected void show(View v) {
		View more_rating_good, more_update, more_feedback, more_feedback2,more_user_method;
		TextView version = (TextView) v.findViewById(R.id.tvVersion);
		String versionStr = "V" + AppUtils.getVersionName(getActivity());
		version.setText(versionStr);
		
		try{
			TextView copyright = (TextView) v.findViewById(R.id.tvCopyright);
			String cr = BaseUtils.getRightInfo(getActivity());
			if(cr != null && !"".equals(cr.trim())) copyright.setText(cr);
		}catch(Exception e){e.printStackTrace();};

		more_update = v.findViewById(R.id.more_update);
		more_feedback = v.findViewById(R.id.more_feedback);
		more_rating_good = v.findViewById(R.id.more_rating_good);
		more_feedback2 = v.findViewById(R.id.more_feedback2);
		more_user_method = v.findViewById(R.id.more_user_method);

		more_update.setOnClickListener(this);
		more_feedback.setOnClickListener(this);
		more_rating_good.setOnClickListener(this);
		more_feedback2.setOnClickListener(this);
		more_user_method.setOnClickListener(this);
		
		more_update.setOnLongClickListener(new OnLongClickListener() {
			public boolean onLongClick(View v) {
				if(BaseUtils.getIsDebug(getActivity())) {
					AppUtils.popAddNilAppsDlg(getActivity());
					return true;
				}
				return false;
			}
		});
		more_feedback.setOnLongClickListener(new OnLongClickListener() {
			public boolean onLongClick(View v) {
				if(BaseUtils.getIsDebug(getActivity())) {
					BaseUtils.popAppInfoDlg(getActivity());
					return true;
				}
				return false;
			}
		});
		more_rating_good.setOnLongClickListener(new OnLongClickListener() {
			public boolean onLongClick(View arg0) {
				Activity act = getActivity();
				if(!BaseUtils.hasGoodRate(act) && BaseUtils.getIsDebug(act)){
					BaseUtils.gotoActivity(act, "nilframe.app.all.ui.TopMainUI");
					return true;
				}
				return false;
			}
		});
		more_feedback2.setOnLongClickListener(new OnLongClickListener() {
			public boolean onLongClick(View v) {
				Activity act = getActivity();
				if(!BaseUtils.hasGoodRate(act) && BaseUtils.getIsDebug(act)){
					BaseUtils.gotoActivity(act, "com.nil.crash.utils.DefaultCrashUI");
					return true;
				}
				return false;
			}
		});
	}
	
	@Override
	public void onClick(View v) {
		if(v.getId() == R.id.more_rating_good){// 好评
			OpenMarket.go(getActivity());
		}else if(v.getId() == R.id.more_update){// 自动更新
			BaseUtils.checkUpdate(getActivity());
		}else if(v.getId() == R.id.more_feedback || v.getId() == R.id.more_feedback2){// 反馈
			BaseUtils.gotoFeedbackUI(getActivity());
		}
	}
}
