package android.app.ui;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.RequiresApi;

import com.android.OpenMtaSDK.R;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ServiceUtils;
import com.blankj.utilcode.util.Utils;
import com.nil.sdk.ui.vo.FeedBackBean;
import com.nil.sdk.utils.AppUtils;
import com.nil.sdk.utils.Gid;
import com.nil.sdk.utils.Spu;
import com.nil.sdk.utils.StringUtils;
import com.nil.vvv.utils.AdSwitchUtils;
import com.nil.vvv.utils.Mtas;
import com.xmb.mta.util.XMBApi;
import com.xmb.mta.util.XMBApiCallback;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.Call;
import okhttp3.Response;

/**
 * 后台服务定时通知 https://www.cnblogs.com/chuanwei-zhang/p/4014390.html<br>
 * 桌面角标兼容性解决 https://blog.csdn.net/q1113225201/article/details/79858032<br>
 */
public class FeedbackService extends Service {
    protected final static String TAG = FeedbackService.class.getSimpleName();
    public final static int FEEDBACK_WHAT_ID = 1001;
    public final static int FEEDBACK_NOTIFICATION_ID = 2001;
    public final static String NOTIFICATION_CHANNEL_ID = "channel_id_badge";

    long period = Mtas.defFbPeriod; //10分钟一个周期（单位秒）
    int delay = 0;  //延时执行

    protected static Timer timer = null;
    @SuppressLint("HandlerLeak")
    final Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            if(msg.what == FEEDBACK_WHAT_ID){
                FeedBackBean fb = (FeedBackBean) msg.obj;
                if(fb != null && StringUtils.noNullStr(fb.getContent())) {
                    sendNotification(Utils.getApp(), fb);
                }
            }
        }
    };

    //取消当前通知消息，表示已读
    public static void cancelNotification() {
        if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "cancelNotification....");
        try {
            NotificationManager mn= (NotificationManager) Utils.getApp().getSystemService(NOTIFICATION_SERVICE);
            if(mn != null) mn.cancel(FEEDBACK_NOTIFICATION_ID);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //清除全部通知，并暂停后台服务
    public static void cleanNotification() {
        if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "cleanNotification....");
        try {
            NotificationManager mn= (NotificationManager) Utils.getApp().getSystemService(NOTIFICATION_SERVICE);
            if(mn != null) mn.cancelAll();
            if (timer != null) {
                timer.cancel();
                timer = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            ServiceUtils.stopService(FeedbackService.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //添加通知
    public static void addNotification(){
        if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "addNotification....");
        try {
            //startService异常 https://blog.csdn.net/legend12300/article/details/106413359
            //更新AndroidUtilCode1.30.6，service启动有问题，暂时不更新
            if(!ServiceUtils.isServiceRunning(FeedbackService.class) && AdSwitchUtils.Sws.Fb_Period.flag) {
                //ServiceUtils.startService(FeedbackService.class); //utilcodex1.29.0及之前的版本支持
                Utils.getApp().startService(new Intent(Utils.getApp(), FeedbackService.class));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onCreate() {
        if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "onCreate....");
    }

    @Override
    public IBinder onBind(Intent arg0) {
        if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "onBind....");
        return null;
    }

    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {
        if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "onStartCommand....");
        if (null == timer ) {
            timer = new Timer();
        }

        try {
            period = Long.parseLong(AdSwitchUtils.Vs.fb_period.value);
            period = (period < 2 * 60) ? Mtas.defFbPeriod : period;    //小于2分钟，配置默认10分钟
            //period = 5;
        } catch (Exception e) {
            period = Mtas.defFbPeriod;
            //e.printStackTrace();
        }

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "onStartCommand.schedule-run....");

                XMBApi.queryFeedback(new XMBApiCallback<ArrayList<FeedBackBean>>() {
                    @Override
                    public void onFailure(Call call, Exception e) {
                        if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "onStartCommand.queryFeedback-onFailure....");
                    }

                    public void onResponse(final ArrayList<FeedBackBean> ary, Call call, Response response) {
                        if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "onStartCommand.queryFeedback-onResponse....\n" + ary);

                        try {
                            if (FeedbackUIV2.hasLatestNews(ary)) { //有最新消息时，弹出通知栏通知
                                FeedBackBean f = FeedBackBean.getMaxFB(ary);
                                handler.sendMessage(handler.obtainMessage(FEEDBACK_WHAT_ID, f));
                                FeedbackUIV2.cacheLatesNews(f); //缓存消息的最大ID
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        }, delay, period*1000);
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "onDestroy....");
        super.onDestroy();
    }

    /**
     * 显示一个普通的通知
     * @param context 上下文
     */
    public static void sendNotification(Context context, FeedBackBean fb) {
        try {
            Notification.Builder builder = new Notification.Builder(context);
            int xIcon = Gid.getID(Utils.getApp(), "R.drawable.ic_launcher");
            Notification notification = builder
                    /**设置通知右边的大图标**/
                    .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.xmta_fb_news_kf))
                    /**设置通知左边的小图标**/
                    .setSmallIcon((xIcon> 0) ? xIcon : R.drawable.xmta_fb_news_notice)
                    /**通知首次出现在通知栏，带上升动画效果的**/
                    .setTicker(AppUtils.getAppName(context))
                    /**设置通知的标题**/
                    .setContentTitle(StringUtils.getValue(Gid.getS(Utils.getApp(),
                            R.string.fb_notification_content_title), "新消息"))
                    /**设置通知的内容**/
                    .setContentText(fb.getContent())
                    /**通知产生的时间，会在通知信息里显示**/
                    .setWhen(System.currentTimeMillis())
                    /**设置该通知优先级**/
                    .setPriority(Notification.PRIORITY_DEFAULT)
                    /**设置这个标志当用户单击面板就可以让通知将自动取消**/
                    .setAutoCancel(true)
                    /**设置他为一个正在进行的通知。他们通常是用来表示一个后台任务,用户积极参与(如播放音乐)或以某种方式正在等待,因此占用设备(如一个文件下载,同步操作,主动网络连接)**/
                    .setOngoing(false)
                    /**向通知添加声音、闪灯和振动效果的最简单、最一致的方式是使用当前的用户默认设置，使用defaults属性，可以组合：**/
                    .setDefaults(Notification.DEFAULT_VIBRATE | Notification.DEFAULT_SOUND)
                    .setContentIntent(PendingIntent.getActivity(context, 1,
                            new Intent(context, FeedbackUIV2.class), PendingIntent.FLAG_CANCEL_CURRENT))
                    .build();

            NotificationManager notificationManager = (NotificationManager)
                    context.getSystemService(context.NOTIFICATION_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                initNotificationChanel(notificationManager);

                //targetSdkVersion>=28时，此通道ID与下面的(NotificationChannel)必须一致，否则消息无法弹出
                builder.setChannelId(NOTIFICATION_CHANNEL_ID);
            }

            /**发起通知，每个ID对应一条消息**/
            notificationManager.notify(FEEDBACK_NOTIFICATION_ID, notification);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @RequiresApi(api = Build.VERSION_CODES.O)
    protected static void initNotificationChanel(NotificationManager notificationManager) {
        try {
            // 传入参数：通道ID，通道名字，通道优先级（类似曾经的 builder.setPriority()）
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID,
                    StringUtils.getValue(Gid.getS(Utils.getApp(), R.string.fb_notificationChannel_name), "消息通道名称"),
                    NotificationManager.IMPORTANCE_DEFAULT);

            boolean sw = true;
            //小红点
            notificationChannel.setShowBadge(sw);

            // 设置通知出现时的闪灯（如果 android 设备支持的话）
            notificationChannel.enableLights(sw);
            //notificationChannel.setLightColor(Color.BLUE);

            // 设置通知出现时的震动（如果 android 设备支持的话）
            notificationChannel.enableVibration(sw);
            //notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});

            // 设置通知出现时声音，默认通知是有声音的
            //notificationChannel.setSound(null, null);

            // 配置通知渠道的属性
            notificationChannel.setDescription(StringUtils.getValue(Gid.getS(Utils.getApp(), R.string.fb_notificationChannel_description), "消息通道描述"));
            notificationManager.createNotificationChannel(notificationChannel);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
