package com.weixin.sdk.pay;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;

import com.blankj.utilcode.util.LogUtils;
import com.nil.sdk.ui.BaseAppCompatActivity;
import com.nil.sdk.ui.BaseUtils;
import com.nil.sdk.utils.Spu;
import com.nil.vvv.utils.AdSwitchUtils;
import com.tencent.mm.opensdk.constants.ConstantsAPI;
import com.tencent.mm.opensdk.modelbase.BaseReq;
import com.tencent.mm.opensdk.modelbase.BaseResp;
import com.tencent.mm.opensdk.openapi.IWXAPIEventHandler;
import com.xmb.mta.util.ResultBean;
import com.xmb.mta.util.XMBOkHttp;
import com.xmb.mta.util.XMBSign;
import com.xvx.sdk.payment.utils.PayBusUtils;
import com.xvx.sdk.payment.utils.Urls;
import com.xvx.sdk.payment.vo.OrderBeanV2;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * 微信支付回调WXPayEntryActivity, 原先需要在app包下定义WXPayEntryActivity类文件用于<br>
 * 接收支付的结果信息,此WxPayResultActivity将接管处理,app无需再配置WXPayEntryActivity<br>
 */
@SuppressLint("DefaultLocale")
public class WeiXinPayResultActivity extends BaseAppCompatActivity implements IWXAPIEventHandler {
    private final static String TAG = WeiXinPayResultActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WeixinUtils.getIWXAPI().handleIntent(getIntent(), this);
    }
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        WeixinUtils.getIWXAPI().handleIntent(intent, this);
    }

    @Override
    public void onReq(BaseReq rq) {
        if(Spu.isSucK(TAG)) LogUtils.wTag(TAG, String.format("微信APP支付onReq请求：transaction=%s,openId=%s", rq.transaction,  rq.openId));
    }
    public void onResp(BaseResp rp) {
        if(Spu.isSucK(TAG)) LogUtils.wTag(TAG, String.format("微信APP支付onResp返回：errCode=%d,errStr=%s,transaction=%s,openId=%s"
                , rp.errCode, rp.errStr, rp.transaction, rp.openId));

        //处理支付回调
        if (rp.getType() == ConstantsAPI.COMMAND_PAY_BY_WX) {
            if (rp.errCode == 0) {
                PayBusUtils.post(PayBusUtils.Type.MSG_TIP_SHOW, "开始同步验签...");

                String url = Urls.URL_QUERY_WEIXIN_ORDER + "?data=";
                // 组装一个weixin_response的键值对：
                Map<String, String> map = new HashMap<>();
                map.put("pay_xmb_trade_no", WeixinUtils.getPayXmbTradeNo());
                map.put("weixin_appid", AdSwitchUtils.Vs.weixin_appid.value);
                map.put("weixinpay_sys_key", AdSwitchUtils.Vs.weixinpay_sys_key.value);
                url += XMBSign.buildUrlData(map);
                if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "3.0.微信同步验签:开始请求链接-->"+url);
                XMBOkHttp.doGet(url, new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        if(Spu.isSucK(TAG)) LogUtils.wTag(TAG, "3.1.微信同步验签:手机支付成功-网页请求失败-->"+e.toString());
                        BaseUtils.popDebugToast("微信同步验签，请求异常");
                        PayBusUtils.post(PayBusUtils.Type.MSG_PAY_FAILED, e.toString());
                    }
                    public void onResponse(Call call, Response response) throws IOException {
                        ResultBean resultBean = XMBSign.deSignByResult4Obj(response);
                        if(Spu.isSucK(TAG)) LogUtils.wTag(TAG, "3.1.微信同步验签:手机支付成功-网页请求成功-->"+resultBean);
                        if(resultBean != null && resultBean.getResult_code() == ResultBean.CODE_SUC){
                            //更新订单信息：
                            OrderBeanV2.addOrderBeans(resultBean.getResult_data());

                            //支付成功：服务器result_data返回当前支付的订单信息
                            BaseUtils.popDebugToast("微信同步验签成功");
                            //支付成功，把服务器返回的订单列表发送出去
                            PayBusUtils.post(PayBusUtils.Type.MSG_PAY_SUCCESS, resultBean.getResult_data());
                        }else {
                            BaseUtils.popDebugToast("微信同步验签失败");
                            PayBusUtils.post(PayBusUtils.Type.MSG_PAY_FAILED, (resultBean == null) ? null: resultBean.getResult_data());
                        }
                    }
                });
            } else if (rp.errCode == -1) {
                BaseUtils.popDebugToast("支付失败，请联系客服！失败原因：" + rp.errStr);
                PayBusUtils.post(PayBusUtils.Type.MSG_PAY_FAILED, rp.errStr);
            } else {
                if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "3.1.支付取消-->"+rp.errStr);
                PayBusUtils.post(PayBusUtils.Type.MSG_PAY_CANCEL, rp.errStr);
            }
        }

        finish();
    }
}
