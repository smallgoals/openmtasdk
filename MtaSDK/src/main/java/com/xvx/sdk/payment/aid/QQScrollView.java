package com.xvx.sdk.payment.aid;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.ScrollView;

public class QQScrollView extends ScrollView {
    /*最多移动屏幕的4分之1 ，将移动的距离除以4*/
    private static final int size = 4;
    private View inner;
    private float y;
    private Rect normal = new Rect();;
    public QQScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    @Override
    protected void onFinishInflate() {  //子视图全部加载调用
        super.onFinishInflate();
        if (getChildCount() > 0) {
            inner = getChildAt(0);
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (inner == null) {
            return super.dispatchTouchEvent(ev);
        } else {
            commOnTouchEvent(ev);
        }

        return super.dispatchTouchEvent(ev);
    }

    public void commOnTouchEvent(MotionEvent ev) {
        int action = ev.getAction();
        if (action == MotionEvent.ACTION_DOWN) {
            y = ev.getY();
        } else if (action == MotionEvent.ACTION_UP) {
            if (isNeedAnimation()) {
                animation();
            }
        } else if (action == MotionEvent.ACTION_MOVE) {
            final float preY = y;
            float nowY = ev.getY();
            /*移动的距离除以4，最多移动4分之一屏幕*/
            int deltaY = (int) (preY - nowY) / size;
            y = nowY;
            // 当滚动到最上或者最下时就不会再滚动，这时移动布局
            if (isNeedMove()) {
                if (normal.isEmpty()) {
                    // 保存正常的布局位置
                    normal.set(inner.getLeft(), inner.getTop(),
                            inner.getRight(), inner.getBottom());
                    return;
                }
                int yy = inner.getTop() - deltaY;
                // 移动布局
                inner.layout(inner.getLeft(), yy, inner.getRight(),
                        inner.getBottom() - deltaY);
            }
        } else {
            // to...
        }
    }

    // 开启动画移动

    public void animation() {
        // 开启移动动画
        TranslateAnimation ta = new TranslateAnimation(0, 0, inner.getTop(),
                normal.top);
        ta.setDuration(200);
        inner.startAnimation(ta);
        // 设置回到正常的布局位置
        inner.layout(normal.left, normal.top, normal.right, normal.bottom);
        normal.setEmpty();
    }

    // 是否需要开启动画
    public boolean isNeedAnimation() {
        return !normal.isEmpty();
    }

    // 是否需要移动布局
    public boolean isNeedMove() {
        int offset = inner.getMeasuredHeight() - getHeight();
        int scrollY = getScrollY();

        if (scrollY == 0 || scrollY == offset) {
            return true;
        }
        return false;
    }


}
