package com.xvx.sdk.payment.web;

import java.io.Serializable;

import okhttp3.Call;
import okhttp3.Response;

public interface PayWebApiCallback<T extends Serializable> {
    void onFailure(Call call, Exception e);
    void onResponse(T obj, Call call, Response response);
}
