package com.xvx.sdk.payment;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ui.WebHtmlActivity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.OpenMtaSDK.R;
import com.android.core.XSEUtils;
import com.blankj.utilcode.util.KeyboardUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.blankj.utilcode.util.Utils;
import com.cazaea.sweetalert.SweetAlertDialog;
import com.nil.sdk.ui.BaseAppCompatActivity;
import com.nil.sdk.ui.BaseUtils;
import com.nil.sdk.ui.aid.MyTipDialog;
import com.nil.sdk.utils.ACacheUtils;
import com.nil.sdk.utils.AppUtils;
import com.nil.sdk.utils.Spu;
import com.nil.sdk.utils.StringUtils;
import com.nil.vvv.utils.AdSwitchUtils;
import com.xmb.mta.util.ResultBean;
import com.xvx.sdk.payment.db.UserDb;
import com.xvx.sdk.payment.utils.CountDownTimerUtil;
import com.xvx.sdk.payment.utils.DataCheckUtils;
import com.xvx.sdk.payment.utils.UserIDUtils;
import com.xvx.sdk.payment.vo.OrderBeanV2;
import com.xvx.sdk.payment.vo.SmsCodeVO;
import com.xvx.sdk.payment.vo.UserVO;
import com.xvx.sdk.payment.web.PayWebAPI;
import com.xvx.sdk.payment.web.PayWebApiCallback;

import okhttp3.Call;
import okhttp3.Response;

/**
 * 用户注册界面<br>
 */
@SuppressLint("SetTextI18n")
public class UserRegisterActivity extends BaseAppCompatActivity implements View.OnClickListener {
    private final static String TAG = UserRegisterActivity.class.getSimpleName();
    ImageView ivUsername;
    EditText etUsername;
    RelativeLayout vg1;
    ImageView ivPsk;
    EditText etPsk;
    ImageView ivPsk2;
    EditText etPsk2;
    Button btnRegister;
    TextView btnLogin;
    View progress;
    TextView tvRegisterSign;
    CheckBox cbRegisterSign;

    //验证码相关：
    RelativeLayout rlCode;
    EditText etCode;
    Button btnGetCode;
    CountDownTimerUtil countDownTimerUtil;
    long millisInFuture = 60*1000; //60秒倒计时

    private static boolean mIsBindViP = false;
    public static void start(Activity act) {
        start(act, null);
    }
    public static void start(Activity act, String title) {
        start(act, title, false);
    }
    public static void start(Activity act, String title, boolean isBindViP) {
        Intent it = new Intent(act, UserRegisterActivity.class);
        it.putExtra("title", title);
        it.putExtra("is_bind_vip", isBindViP);
        BaseUtils.startActivity(act,it);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.xuser_activity_regiester);
        setDisplayHomeAsUpEnabled(true);

        ivUsername = findViewById(R.id.iv_username);
        etUsername = findViewById(R.id.et_username);
        vg1 = findViewById(R.id.vg_1);
        ivPsk = findViewById(R.id.iv_psk);
        etPsk = findViewById(R.id.tv_psk);
        ivPsk2 = findViewById(R.id.iv_psk2);
        etPsk2 = findViewById(R.id.tv_psk2);
        btnRegister = findViewById(R.id.btn_register);

        btnLogin = findViewById(R.id.btn_login);
        btnLogin.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);
        btnLogin.getPaint().setAntiAlias(true);//抗锯齿

        progress = findViewById(R.id.progress);

        btnLogin.setOnClickListener(this);
        btnRegister.setOnClickListener(this);

        rlCode = findViewById(R.id.rl_code);
        etCode = findViewById(R.id.et_code);
        btnGetCode = findViewById(R.id.btn_get_code);
        //sms_code_switch开关打开时，才显示验证码布局
        if(Spu.isSucV(AdSwitchUtils.Vs.sms_code_switch.value)) {
            rlCode.setVisibility(View.VISIBLE);
            countDownTimerUtil = new CountDownTimerUtil(btnGetCode, millisInFuture, 1000);

            btnGetCode.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String pkg = AppUtils.getPackageName();
                    String tel = etUsername.getText().toString();
                    String code = UserIDUtils.getNumCode(); //生成随机6位数字
                    String sms_config_key = AdSwitchUtils.getInstance().getOnlineValue("sms_config_key", R.string.sms_config_key);

                    SmsCodeVO smsCodeVO = new SmsCodeVO(pkg, tel, code, sms_config_key);
                    ACacheUtils.setAppCacheObject(code, smsCodeVO); //存储当前code对应的实体

                    //手机号未输对提示：
                    if (!DataCheckUtils.isValidMobile(tel)) {
                        /*KeyboardUtils.hideSoftInput(getActivity()); //隐藏软键盘
                        PopTip.show(R.string.xuser_regiester_tip_error_phone).iconError();*/

                        etUsername.setError(getString(R.string.xuser_error_phone));
                        DataCheckUtils.requestFocus(etUsername);
                        return;
                    }

                    //获取验证码过于频繁提示：
                    if(SmsCodeVO.isSendFrequently()){
                        KeyboardUtils.hideSoftInput(getActivity()); //隐藏软键盘
                        MyTipDialog.popDialog(getActivity(), Utils.getApp().getString(R.string.xuser_regiester_get_code_frequently_title),
                                Utils.getApp().getString(R.string.xuser_regiester_get_code_frequently), Utils.getApp().getString(R.string.xuser_know_btn_text));
                        return;
                    }

                    KeyboardUtils.hideSoftInput(getActivity()); //隐藏软键盘

                    //测试专用
                    //sms_config_key = ((EditText) findViewById(R.id.et_ck)).getText().toString();

                    //发送验证码：
                    PayWebAPI.getSmsCode(new PayWebApiCallback<ResultBean>() {
                        @Override
                        public void onFailure(Call call, Exception e) {
                            BaseUtils.popMyToast(R.string.xuser_connect_fail);
                        }
                        public void onResponse(final ResultBean obj, Call call, Response response) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    //获取验证码成功：
                                    if (obj.getResult_code() == ResultBean.CODE_SUC) {
                                        countDownTimerUtil.start(); //启动倒计时
                                        SmsCodeVO.updateRemark(obj.getResult_data(), true);
                                    } else {
                                        MyTipDialog.popDialog(getActivity(), Utils.getApp().getString(R.string.xuser_regiester_get_code_fail),
                                                obj.getResult_data(), Utils.getApp().getString(R.string.xuser_know_btn_text));
                                    }
                                }
                            });
                        }
                    }, tel, sms_config_key, code/*六位数字验证码*/);
                }
            });
        } else {
            rlCode.setVisibility(View.GONE);
        }

        String title = getIntent().getStringExtra("title");
        if(StringUtils.noNullStr(title)) {
            setTitle(title);
        }else {
            setTitle(getString(R.string.xuser_register_def_title));
        }
        mIsBindViP = getIntent().getBooleanExtra("is_bind_vip", false);

        btnRegister.setText(mIsBindViP ? getString(R.string.xuser_register_and_bind_btn_text) : getString(R.string.xuser_register_btn_text));

        // FIX-ME: 2019/9/9 遗留问题：
        //  1.User_id和Cid查询接口共存问题：判断用户是否已登录，登录过就调User_id接口；反之调用Cid接口；
        //  2.之前的会员信息存储在SD卡，最新的会员信息存储APP缓存还是SD？
        //  3.用户登录非会员账号，ViP信息被缓存，之后无法绑定会员（可以清除用户数据）；
        if(BaseUtils.getIsDebug(this)) {
            //长按注册：快速注册（17766554433、123456）
            btnRegister.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    etUsername.setText(XSEUtils.decode("C1ApTYut8FN0Os9qyDLXe7FR4xtUw%3D%3D"));
                    etPsk.setText(XSEUtils.decode("VD5GDIfYKikEIHdlBDGt30ORmtbbw%3D%3D"));
                    etPsk2.setText(XSEUtils.decode("VD5GDIfYKikEIHdlBDGt30ORmtbbw%3D%3D"));
                    cbRegisterSign.setChecked(true);
                    return true;
                }
            });
        }

        cbRegisterSign = findViewById(R.id.cb_register_sign);
        tvRegisterSign = findViewById(R.id.tv_register_sign);
        /*tvRegisterSign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isSign = cbRegisterSign.isChecked();
                cbRegisterSign.setChecked(!isSign);
            }
        });
        LinearLayout llRegisterSign;
        llRegisterSign = findViewById(R.id.ll_register_sign);
        llRegisterSign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isSign = cbRegisterSign.isChecked();
                cbRegisterSign.setChecked(!isSign);
            }
        });*/

        //注册即同意《隐私政策》和《用户协议》
        String signContent = getString(R.string.xuser_regiester_sign_tip,
                getString(R.string.privacy_policy_title),
                getString(R.string.user_agreement_title));
        callService(signContent, tvRegisterSign);
    }

    private void callService(String content, TextView textView) {
        SpannableStringBuilder builder = new SpannableStringBuilder(content);
        int i = content.indexOf("《");//截取文字开始的下标
        int i2 = content.indexOf("》", i) + 1;

        int j = content.indexOf("《", i2);
        int j2 = content.indexOf("》", i2)+ 1;

        builder.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                WebHtmlActivity.startWithAssets(getActivity(),
                        getString(R.string.privacy_policy_title),
                        getString(R.string.xmta_assets_html_file_name_privacy_policy));
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(getResources().getColor(R.color.colorBlue));       //设置文字颜色
                ds.setUnderlineText(true);      //设置下划线//根据需要添加
            }
        }, i, i2, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                WebHtmlActivity.startWithAssets(getActivity(),
                        getString(R.string.user_agreement_title),
                        getString(R.string.xmta_assets_html_file_name_user_agreement));
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(getResources().getColor(R.color.colorBlue));       //设置文字颜色
                ds.setUnderlineText(true);      //设置下划线//根据需要添加
            }
        }, j, j2, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        textView.setHighlightColor(Color.TRANSPARENT); //设置点击后的颜色为透明，否则会一直出现高亮
        textView.setText(builder);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
    }

    public void onBtnRegisterClicked() {
        final String packageName = AppUtils.getPackageName(this);
        final String user_name = etUsername.getText().toString();
        final String psk = etPsk.getText().toString();
        String psk2 = etPsk2.getText().toString();
        final String code = etCode.getText().toString();

        if (!DataCheckUtils.isValidMobile(user_name)) {
            etUsername.setError(getString(R.string.xuser_error_phone));
            DataCheckUtils.requestFocus(etUsername);
            return;
        }
        if (!DataCheckUtils.isValidPsk(psk)) {
            etPsk.setError(getString(R.string.xuser_error_psk));
            DataCheckUtils.requestFocus(etPsk);
            return;
        }
        if (!psk2.equals(psk)) {
            etPsk2.setError(getString(R.string.xuser_error_psk2));
            DataCheckUtils.requestFocus(etPsk2);
            return;
        }

        //sms_code_switch开关打开时，才进行验证码检测
        if(Spu.isSucV(AdSwitchUtils.Vs.sms_code_switch.value)) {
            if (!DataCheckUtils.isValidCode(code)) {
                etCode.setError(getString(R.string.xuser_regiester_error_code));
                DataCheckUtils.requestFocus(etCode);
                return;
            }
            //验证码合法性检测：
            if (!SmsCodeVO.isLegalCode(user_name, code)) {
                KeyboardUtils.hideSoftInput(getActivity()); //隐藏软键盘
                BaseUtils.popMyToast(R.string.xuser_regiester_tip_error_code);
                return;
            }
        }

        boolean isSign = cbRegisterSign.isChecked();
        if(!isSign){
            // 创建水平缩放抖动动画
            ObjectAnimator animator1 = ObjectAnimator.ofFloat(cbRegisterSign, "scaleX", 0.8f, 0.2f, 1.2f, 0.8f);
            animator1.setInterpolator(new AccelerateInterpolator());
            animator1.setDuration(500);
            // 创建垂直缩放抖动动画
            ObjectAnimator animator2 = ObjectAnimator.ofFloat(cbRegisterSign, "scaleY", 0.8f, 0.2f, 1.2f, 0.8f);
            animator2.setInterpolator(new AccelerateInterpolator());
            animator2.setDuration(500);
            // 创建动画集合，并同时播放两个动画
            AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.playTogether(animator1, animator2);
            animatorSet.start();

            String msg = String.format("请勾选 %s", tvRegisterSign.getText().toString());
            BaseUtils.popMyToast(getActivity(), msg);
            return;
        }

        progress.setVisibility(View.VISIBLE);

        PayWebAPI.registerOrBindViP(new PayWebApiCallback<ResultBean>() {
            @Override
            public void onFailure(Call call, Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ToastUtils.showLong(R.string.xuser_connect_fail);
                        progress.setVisibility(View.INVISIBLE);
                    }
                });
            }
            public void onResponse(final ResultBean obj, Call call, Response response) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "注册：ResultBean="+obj+",OrderBeanV2="+ OrderBeanV2.getOrderBean());
                        progress.setVisibility(View.INVISIBLE);
                        KeyboardUtils.hideSoftInput(getActivity()); //隐藏软键盘

                        if (obj.getResult_code() == ResultBean.CODE_SUC) {
                            ACacheUtils.removeObject(code); //注册成功时，让此Code失效

                            UserVO uu = new UserVO(user_name, XSEUtils.mtaEncode(psk));
                            UserDb.save(uu);

                            final Activity act = getActivity();
                            new SweetAlertDialog(act, SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText(act.getString(R.string.xuser_register_suc_title))
                                    .setContentText(mIsBindViP ? getString(R.string.xuser_register_and_bind_success) : getString(R.string.xuser_register_success))
                                    .setConfirmText(act.getString(R.string.xuser_register_suc_okbtn_text))
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            onBtnLoginClicked();
                                            sweetAlertDialog.dismiss();
                                        }
                                    })
                                    .show();
                        } else {
                            //ToastUtils.showLong(obj.getResult_data());

                            final Activity act = getActivity();
                            new SweetAlertDialog(act, SweetAlertDialog.WARNING_TYPE)
                                    .setTitleText(act.getString(R.string.xuser_register_fail))
                                    .setContentText(obj.getResult_data())
                                    .setConfirmText(act.getString(R.string.xuser_know_btn_text))
                                    .show();
                        }
                    }
                });
            }
        }, packageName, user_name, psk, SmsCodeVO.getRemark(code));
    }

    public void onBtnLoginClicked() {
        //注册成功，跳转到登录界面
        UserLoginActivity.start(this);
        finish();
    }

    @Override
    public void onClick(View v) {
        KeyboardUtils.hideSoftInput(v); //隐藏输入法键盘

        if(v.getId() == R.id.btn_login){
            onBtnLoginClicked();
        }else if(v.getId() == R.id.btn_register){
            onBtnRegisterClicked();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(countDownTimerUtil != null){
            countDownTimerUtil.onStop(countDownTimerUtil);
        }
    }
}
