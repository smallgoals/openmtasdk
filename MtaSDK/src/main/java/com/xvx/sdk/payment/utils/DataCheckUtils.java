package com.xvx.sdk.payment.utils;

import android.widget.EditText;

import com.blankj.utilcode.util.KeyboardUtils;
import com.nil.sdk.utils.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DataCheckUtils {
    public static boolean isValidPsk(String data){
        boolean isOK = false;
        if(StringUtils.noNullStr(data)){
            // 匹配ASCII值从0x21=33-0x7e=126[!-~]的字符
            String reg = "^[\\x21-\\x7e]{6,20}$";
            isOK = Pattern.matches(reg, data);
        }
        return isOK;
    }
    public static boolean isValidUserName(String data){
        boolean isOK = false;
        if(StringUtils.noNullStr(data)){
            // 匹配ASCII值从0x21=33-0x7e=126[!-~]的字符
            String reg = "^[\\x21-\\x7e]{5,20}$";
            isOK = Pattern.matches(reg, data);
        }
        return isOK;
    }
    public static boolean isValidMobile(String data){
        boolean isOK = false;
        if(StringUtils.noNullStr(data)){
            // 匹配合法的手机号：13-19x 1122 3344
            String reg = "^(13|14|15|18|17|19|16)[0-9]{9}$";
            isOK = Pattern.matches(reg, data);
        }
        return isOK;
    }
    public static void requestFocus(EditText editText){
        if(editText != null){
            /*editText.setFocusable(true);
            editText.setFocusableInTouchMode(true);
            editText.findFocus();*/

            //请求焦点：
            editText.requestFocus();
            //弹出软键盘：
            KeyboardUtils.showSoftInput(editText);
        }
    }
    /**
     * 判断字符串包含表情<br>
     */
    public static boolean containsEmoji(String value){
        boolean flag = false;
        try {
            if(value != null) {
                Pattern p = Pattern.compile("[^\\u0000-\\uFFFF]");
                Matcher m = p.matcher(value);
                flag = m.find();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }

    /**
     * 判断是否为非法UTF-8<br>
     */
    public static boolean isIllegalUTF8(String value){
        //非法UTF-8：包含表情字符
        return containsEmoji(value);
    }

    /**
     * 判断是否为6位数字的验证码<br>
     */
    public static boolean isValidCode(String data){
        boolean isOK = false;
        if(StringUtils.noNullStr(data)){
            // 匹配合法的验证码：047521
            String reg = "^[0-9]{6}$";
            isOK = Pattern.matches(reg, data);
        }
        return isOK;
    }
}
