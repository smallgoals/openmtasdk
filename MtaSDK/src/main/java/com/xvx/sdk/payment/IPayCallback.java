package com.xvx.sdk.payment;

import com.xvx.sdk.payment.vo.UserLoginVO;
import com.xvx.sdk.payment.vo.ViPConfigVO.ViPItem;

public interface IPayCallback {
    void onPay(String subject, String price, String dead_time, ViPItem item, UserLoginVO userLoginBean, Object obj);
}
