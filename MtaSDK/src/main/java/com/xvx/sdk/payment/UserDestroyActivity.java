package com.xvx.sdk.payment;

import android.app.Activity;
import android.app.ui.WebHtmlActivity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.android.OpenMtaSDK.R;
import com.android.core.XSEUtils;
import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.KeyboardUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.cazaea.sweetalert.SweetAlertDialog;
import com.nil.sdk.ui.BaseAppCompatActivity;
import com.nil.sdk.ui.BaseUtils;
import com.nil.sdk.utils.ACacheUtils;
import com.nil.sdk.utils.AppUtils;
import com.nil.sdk.utils.Spu;
import com.nil.sdk.utils.StringUtils;
import com.nil.sdk.utils.XMBEventUtils;
import com.xmb.mta.util.ResultBean;
import com.xvx.sdk.payment.db.UserDb;
import com.xvx.sdk.payment.utils.DataCheckUtils;
import com.xvx.sdk.payment.vo.OrderBeanV2;
import com.xvx.sdk.payment.web.PayWebAPI;
import com.xvx.sdk.payment.web.PayWebApiCallback;

import okhttp3.Call;
import okhttp3.Response;

/**
 * 用户注销界面<br>
 */
public class UserDestroyActivity extends BaseAppCompatActivity implements View.OnClickListener {
    private final static String TAG = UserDestroyActivity.class.getSimpleName();
    View progress;
    TextView tv_user_destroy_sign,tv_user_destroy_agreement_tip;
    CheckBox cb_user_destroy_sign;

    Button btn_apply_for_user_destroy,btn_confirm_user_destroy,btn_cancel_user_destroy;
    View ll_apply_for_user_destroy_info, ll_user_destroy_form;
    TextView tv_appContent;
    EditText et_username, et_psk, et_reason;

    private static boolean mIsBindViP = false;
    public static void start(Activity act) {
        start(act, null);
    }
    public static void start(Activity act, String title) {
        Intent it = new Intent(act, UserDestroyActivity.class);
        it.putExtra("title", title);
        BaseUtils.startActivity(act,it);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.xuser_activity_user_destroy);
        setDisplayHomeAsUpEnabled(true);
        String title = getIntent().getStringExtra("title");
        if(StringUtils.noNullStr(title)) {
            setTitle(title);
        }else {
            setTitle(getString(R.string.xuser_destroy_def_title));
        }

        //填单信息：
        tv_appContent = findViewById(R.id.tv_appContent);
        et_username = findViewById(R.id.et_username);
        et_psk = findViewById(R.id.et_psk);
        et_reason = findViewById(R.id.et_reason);

        String app_info = String.format("名称：%s\n包名：%s\n版本：%s", AppUtils.getAppName(this), AppUtils.getPackageName(this), AppUtils.getVersionName(this));
        tv_appContent.setText(app_info);

        //动态界面：
        progress = findViewById(R.id.progress);
        progress.setVisibility(View.GONE);
        ll_apply_for_user_destroy_info = findViewById(R.id.ll_apply_for_user_destroy_info);
        ll_apply_for_user_destroy_info.setVisibility(View.VISIBLE);
        ll_user_destroy_form = findViewById(R.id.ll_user_destroy_form);
        ll_user_destroy_form.setVisibility(View.GONE);

        btn_apply_for_user_destroy = findViewById(R.id.btn_apply_for_user_destroy);
        btn_confirm_user_destroy = findViewById(R.id.btn_confirm_user_destroy);
        btn_cancel_user_destroy = findViewById(R.id.btn_cancel_user_destroy);
        btn_apply_for_user_destroy.setOnClickListener(this);
        btn_confirm_user_destroy.setOnClickListener(this);
        btn_cancel_user_destroy.setOnClickListener(this);

        if(BaseUtils.getIsDebug(this)) {
            btn_cancel_user_destroy.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    et_username.setText(UserDb.getUserName());
                    et_psk.setText(XSEUtils.mtaDecode(UserDb.getUserPsk()));
                    et_reason.setText("账号注销-测试");
                    return true;
                }
            });
        }

        //注册协议相关：
        tv_user_destroy_sign = findViewById(R.id.tv_user_destroy_sign);
        cb_user_destroy_sign = findViewById(R.id.cb_user_destroy_sign);
        //注册即同意《注销须知》
        String signContent = getString(R.string.xuser_destroy_sign_tip,
                getString(R.string.xuser_destroy_agreement_title));
        callService(signContent, tv_user_destroy_sign);

        tv_user_destroy_agreement_tip = findViewById(R.id.tv_user_destroy_agreement_tip);
        String ss = getString(R.string.xuser_destroy_agreement_tip,
                getString(R.string.xuser_destroy_agreement_title));
        tv_user_destroy_agreement_tip.setText(ss);
    }

    private void callService(String content, TextView textView) {
        SpannableStringBuilder builder = new SpannableStringBuilder(content);
        int i = content.indexOf("《");//截取文字开始的下标
        int i2 = content.indexOf("》", i) + 1;

        int j = content.indexOf("《", i2);
        int j2 = content.indexOf("》", i2)+ 1;

        builder.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                WebHtmlActivity.startWithAssets(getActivity(),
                        getString(R.string.xuser_destroy_agreement_title),
                        getString(R.string.xmta_assets_html_file_name_user_destroy_agreement));
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(getResources().getColor(R.color.colorBlue));       //设置文字颜色
                ds.setUnderlineText(true);      //设置下划线//根据需要添加
            }
        }, i, i2, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        textView.setHighlightColor(Color.TRANSPARENT); //设置点击后的颜色为透明，否则会一直出现高亮
        textView.setText(builder);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
    }

    public void gotoLogout() {
        final String packageName = AppUtils.getPackageName(this);
        final String user_name = et_username.getText().toString();
        final String psk = et_psk.getText().toString();
        final String reason = et_reason.getText().toString();

        if (!DataCheckUtils.isValidMobile(user_name)) {
            et_username.setError(getString(R.string.xuser_error_account));
            DataCheckUtils.requestFocus(et_username);
            return;
        }
        if (!DataCheckUtils.isValidPsk(psk)) {
            et_psk.setError(getString(R.string.xuser_error_psk));
            DataCheckUtils.requestFocus(et_psk);
            return;
        }
        //注销原因可以为空，但不能含表情等非法字符
        if(DataCheckUtils.isIllegalUTF8(reason)){
            BaseUtils.popMyToast(getActivity(), R.string.xuser_destroy_illegal_utf8_tip);
            return;
        }
        progress.setVisibility(View.VISIBLE);

        PayWebAPI.userDestroy(new PayWebApiCallback<ResultBean>() {
            @Override
            public void onFailure(Call call, Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ToastUtils.showLong(R.string.xuser_connect_fail);
                        progress.setVisibility(View.INVISIBLE);
                    }
                });
            }
            public void onResponse(final ResultBean obj, Call call, Response response) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "账号注销：ResultBean="+obj+",OrderBeanV2="+ OrderBeanV2.getOrderBean());
                        progress.setVisibility(View.INVISIBLE);
                        KeyboardUtils.hideSoftInput(getActivity()); //隐藏软键盘

                        String oo = String.format("%s|%s|%s", user_name, psk, reason);
                        if (obj.getResult_code() == ResultBean.CODE_SUC) {
                            XMBEventUtils.send(XMBEventUtils.EventType.user_destroy_success, oo);
                            onLogoutFinish();
                        } else {
                            XMBEventUtils.send(XMBEventUtils.EventType.user_destroy_fail, oo);
                            final Activity act = getActivity();
                            new SweetAlertDialog(act, SweetAlertDialog.WARNING_TYPE)
                                    .setTitleText(act.getString(R.string.xuser_destroy_fail))
                                    .setContentText(obj.getResult_data())
                                    .setConfirmText(act.getString(R.string.xuser_know_btn_text))
                                    .show();
                        }
                    }
                });
            }
        }, packageName, user_name, psk, reason);
    }

    public void onLogoutFinish() {
        //注销完成：删除订单、删除登录、IMEI、Cid
        ACacheUtils.clearObject();

        //清除栈内全部Activity，返回到主界面
        ActivityUtils.finishAllActivities();
        BaseUtils.gotoMainUI(this, true);

        //弹出注销成功提示
        ToastUtils.showLong(R.string.xuser_destroy_success);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btn_apply_for_user_destroy){
            boolean isSign = cb_user_destroy_sign.isChecked();
            if(!isSign){
                String msg = String.format("请勾选 %s", tv_user_destroy_sign.getText().toString());
                ToastUtils.showShort(msg);
            }else {
                ll_apply_for_user_destroy_info.setVisibility(View.GONE);
                ll_user_destroy_form.setVisibility(View.VISIBLE);
            }
        }else if(v.getId() == R.id.btn_confirm_user_destroy){
            gotoLogout();
        }else if(v.getId() == R.id.btn_cancel_user_destroy){
            finish();
        }
    }
}
