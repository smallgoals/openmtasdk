package com.xvx.sdk.payment.utils;

/**
 * 标识代码常量类<br>
 */
public class Codes {
    /**
     * 登录状态标识<br>
     */
    public static class LoginState{
        // TK验证通过
        public static final String TK_STATE_SUC = "200";

        // TK验证失败
        public static final String TK_STATE_FAIL = "-1";
        // TK是正确的，但已经被挤下线（同一用户名已在其他设备登录）
        public static final String TK_STATE_TTL_OFFLINE = "-300";
        // TK已经过时
        public static final String TK_STATE_TTL_TIMEOUT = "-504";
        // 内部错误（服务器、数据库异常）
        public static final String TK_STATE_OTHER_ERROR = "-500";
    }

    /**
     * VIP查询类型<br>
     */
    public static class VIPCheckType{
        //【预留】VIP类型（默认为0）：
        // -1：仅通过Cid查询订单信息
        // 1： 仅通过User_id查询订单信息
        // 0： 通过Cid或User_id查询订单信息
        public static final int VIP_TYPE_CID = -1;
        public static final int VIP_TYPE_CID_OR_USER_ID = 0;
        public static final int VIP_TYPE_USER_ID = 1;
    }


}
