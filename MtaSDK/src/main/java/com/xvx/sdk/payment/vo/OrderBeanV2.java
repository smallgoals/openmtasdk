package com.xvx.sdk.payment.vo;

import android.support.annotation.Keep;

import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.LogUtils;
import com.google.gson.reflect.TypeToken;
import com.nil.sdk.nb.utils.NbDataUtils;
import com.nil.sdk.utils.ACacheUtils;
import com.nil.sdk.utils.DateUtils;
import com.nil.sdk.utils.Spu;
import com.nil.sdk.utils.StringUtils;
import com.nil.vvv.utils.AdSwitchUtils;
import com.xmb.mta.util.ResultBean;
import com.xvx.sdk.payment.db.UserLoginDb;
import com.xvx.sdk.payment.utils.PayUtils;
import com.xvx.sdk.payment.web.PayWebAPI;
import com.xvx.sdk.payment.web.PayWebApiCallback;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 对应服务器上的订单
 */
@SuppressWarnings("serial")
@Keep
public class OrderBeanV2 implements Serializable {
    private static final long serialVersionUID = 3749837482311983451L;
    private static final String TAG = OrderBeanV2.class.getSimpleName();

    //检查ViP的类型：（1.cid 2.user 3.cid or user 0.通过类型智能分析）
    public static final String CHECK_VIP_TYPE_KEY = "check_vip_type_key";   //存储的键名
    public static final String CHECK_VIP_TYPE_AUTO = "check_vip_type_auto";
    public static final String CHECK_VIP_TYPE_CID = "check_vip_type_cid";
    public static final String CHECK_VIP_TYPE_USER = "check_vip_type_user"; // 默认以用户方式判断ViP
    public static final String CHECK_VIP_TYPE_CID_OR_USER = "check_vip_type_cid_or_user";
    public static final String CHECK_VIP_TYPE_DEFAULT = "check_vip_type_default";

    private int id;// 自增ID
    private double money;// 金额
    private int pay_count;// 订单下单的数量
    private String pay_xmb_trade_no;// 我们服务器生成的订单号，即支付宝的out_trade_no
    private Date order_time;// 订单生成时间
    private Date dead_time;// 该订单VIP失效时间

    public static final String Cache_KEY = "OrderBean";
    public static final String Cache_KEY_V2 = "OrderBeanV2";
    public static final String Cache_KEY_V2_ARY = "OrderBeanV2s";
    public static void removeOrderBean() {
        ACacheUtils.removeObject(Cache_KEY_V2);
        ACacheUtils.removeObject(Cache_KEY_V2_ARY);
    }
    public static void setOrderBean(OrderBeanV2 orderBeanV2) {
        //存储订单信息：从之前的SD卡转移至APP内部File目录
        //ACacheUtils.setCacheObject(Cache_KEY_V2, orderBeanV2);
        ACacheUtils.setAppFileCacheObject(Cache_KEY_V2, orderBeanV2);
    }
    public static void setOrderBeans( ArrayList<OrderBeanV2> ary) {
        //存储订单信息：从之前的SD卡转移至APP内部File目录
        //ACacheUtils.setCacheObject(Cache_KEY_V2_ARY, ary);
        ACacheUtils.setAppFileCacheObject(Cache_KEY_V2_ARY, ary);
    }
    public static OrderBeanV2 getOrderBean() {
        // 优先使用原缓存信息，没有时进行提取V2的缓存
        OrderBeanV2 r = NbDataUtils.getMyAppFileCacheObject(Cache_KEY, OrderBeanV2.class);
        if(r == null){
            r = NbDataUtils.getMyAppFileCacheObject(Cache_KEY_V2, OrderBeanV2.class);
        }
        return r;
    }
    public static ArrayList<OrderBeanV2> getOrderBeans() {
        ArrayList<OrderBeanV2> orders = null;
        try {
            Type type = new TypeToken<ArrayList<OrderBeanV2>>() {}.getType();
            orders = NbDataUtils.getMyAppFileCacheObject(Cache_KEY_V2_ARY, type);
        }catch (Exception e){
            e.printStackTrace();
        }
        return orders;
    }
    public static void addOrderBeans(String json) {
        if(StringUtils.isNullStr(json)) return;

        try {
            ArrayList<OrderBeanV2> orders = getOrderBeans();
            if (orders == null) {
                orders = new ArrayList<>();
            }
            Type type = new TypeToken<ArrayList<OrderBeanV2>>() {}.getType();
            ArrayList<OrderBeanV2> oo = GsonUtils.fromJson(json, type);
            if (oo != null) {
                orders.addAll(0, oo);
                setOrderBeans(orders);  //更新保存会员列表
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public static boolean hasViP() {
        boolean isOK = false;
        Date date = getTotalDeadtime();//dead_time;
        if(date != null){
            isOK = PayUtils.getCurNetDate().getTime() < date.getTime();
        }
        if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "hasViP-->"+isOK);
        return isOK;
    }
    public static boolean hasPay() {
        boolean isOK = false;
        ViPConfigVO obj = ViPConfigVO.getAllViPConfig();
        if(obj != null){
            isOK = obj.isPaySwitch() && AdSwitchUtils.Sws.C1.flag;  //C1默认跟随广告开关，需要关闭时Um参数中添加“c1_close”
        }
        if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "hasPay-->"+isOK);
        return isOK;
    }
    public static boolean hasPayNoViP(){
        // 已开启支付且用户无ViP
        return hasPay() && !hasViP();
    }
    public static boolean hasViPNoLogin(){
        // 有ViP但不没登录：用于绑定ViP提示
        return OrderBeanV2.hasViP() && !UserLoginDb.isLogin();
    }
    public static boolean hasPayNoLogin(){
        // 有支付但不没登录：用于提示登录
        return OrderBeanV2.hasPay() && !UserLoginDb.isLogin();
    }

    public static void updateOrderBean(){
        updateOrderBean(null);
    }
    public static void updateOrderBean(String checkVipType){
        updateOrderBean(null, null, checkVipType);
    }
    public static void updateOrderBean(PayWebApiCallback<ResultBean> queryCallback, PayWebApiCallback<ViPConfigVO> loadVIPCallback){
        updateOrderBean(queryCallback,loadVIPCallback, null);
    }
    public static void updateOrderBean(PayWebApiCallback<ResultBean> queryCallback, PayWebApiCallback<ViPConfigVO> loadVIPCallback, String checkVipType){
        if(StringUtils.noNullStr(checkVipType)) {
            ACacheUtils.setCacheValue(CHECK_VIP_TYPE_KEY, checkVipType);
        }

        PayWebAPI.updateNetTime();   //更新网络时间
        PayWebAPI.queryOrder(queryCallback);//查询订单信息
        PayWebAPI.loadViPConfig(loadVIPCallback);   //加载ViP配置信息
    }

    public static Date getTotalDeadtime(){
        return getTotalDeadtime(OrderBeanV2.getOrderBeans());
    }
    public static Date getTotalDeadtime(List<OrderBeanV2> orders){
        Date date = new Date(0);
        if(orders != null && !orders.isEmpty()){
            int size = orders.size();
            if(size == 1){
                date = orders.get(0).getDead_time();
            }else{
                long allTime = 0;
                long minOrderTime = orders.get(0).getOrder_time().getTime();
                long maxDeadTime = 0;
                for(int i = 0; i < size; i++){
                    OrderBeanV2 ob = orders.get(i);
                    long oTime = ob.getOrder_time().getTime();
                    long dTime = ob.getDead_time().getTime();
                    // 取出最小的订单时间
                    if(minOrderTime > oTime){
                        minOrderTime = oTime;
                    }
                    // 取出最大的到期时间
                    if(maxDeadTime < dTime){
                        maxDeadTime = dTime;
                    }

                    // 逐步连续拼接订单的间隔时间
                    allTime += (dTime-oTime);
                }
                long result = minOrderTime + allTime;
                if(result < maxDeadTime) {
                    // 防止非连续含到期订单的拼接（可能出现最大到期时间大于连续拼接时间）
                    result = maxDeadTime;
                }
                date = new Date(result);
            }
        }
        return date;
    }

    @Override
    public String toString() {
        return "OrderBeanV2{" +
                "id=" + id +
                ", money=" + money +
                ", pay_count=" + pay_count +
                ", pay_xmb_trade_no='" + pay_xmb_trade_no + '\'' +
                ", order_time=" + DateUtils.getStringByFormat(order_time, DateUtils.dateFormatYMDHMS) +
                ", dead_time=" + DateUtils.getStringByFormat(dead_time, DateUtils.dateFormatYMDHMS) +
                '}';
    }

    public OrderBeanV2(){}
    public OrderBeanV2(int id, double money, int pay_count, String pay_xmb_trade_no, Date
            order_time, Date dead_time) {
        this();
        this.id = id;
        this.money = money;
        this.pay_count = pay_count;
        this.pay_xmb_trade_no = pay_xmb_trade_no;
        this.order_time = order_time;
        this.dead_time = dead_time;
    }

    /**
     * @return 当前是否属于VIP会员
     */
    public boolean isVIP() {
        boolean isOK = false;
        Date date = getTotalDeadtime();//dead_time;
        if(date != null){
            isOK = PayUtils.getCurNetDate().getTime() < date.getTime();
        }
        return isOK;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public int getPay_count() {
        return pay_count;
    }

    public void setPay_count(int pay_count) {
        this.pay_count = pay_count;
    }

    public String getPay_xmb_trade_no() {
        return pay_xmb_trade_no;
    }

    public void setPay_xmb_trade_no(String pay_xmb_trade_no) {
        this.pay_xmb_trade_no = pay_xmb_trade_no;
    }

    public Date getOrder_time() {
        return order_time;
    }

    public void setOrder_time(Date order_time) {
        this.order_time = order_time;
    }

    public Date getDead_time() {
        return dead_time;
    }

    public void setDead_time(Date dead_time) {
        this.dead_time = dead_time;
    }
}
