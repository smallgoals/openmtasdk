package com.xvx.sdk.payment.view;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.OpenMtaSDK.R;

@Deprecated
public class InputDialogZX extends Dialog implements View.OnClickListener {
    Context context;
    TextView tvTitle;
    EditText et;
    Button btnCancel;
    Button btnOk;
    String title, hint, defValue, btnCancelText,
            btnOKText, getBtnCancelText;
    OnOkListener okLsn;

    public InputDialogZX(@NonNull Context context) {
        super(context);
    }

    public InputDialogZX(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    public InputDialogZX(Context context, String title, String hint, String btnOKText, String
            btnCancelText,
                         OnOkListener
                                 okLsn) {
        super(context, R.style.alert_dialog);
        this.title = title;
        this.hint = hint;
        this.btnOKText = btnOKText;
        this.btnCancelText = btnCancelText;
        this.context = context;
        this.okLsn = okLsn;
    }

    public InputDialogZX(Context context, String title, String hint, String defValue, String btnOKText, String
            btnCancelText,
                         OnOkListener
                                 okLsn) {
        super(context, R.style.alert_dialog);
        this.title = title;
        this.hint = hint;
        this.defValue = defValue;
        this.btnOKText = btnOKText;
        this.btnCancelText = btnCancelText;
        this.context = context;
        this.okLsn = okLsn;
    }

    public String getInputString() {
        return et.getText().toString();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.xuser_dialog_input);
        initView();
    }

    private void initView() {
        tvTitle = findViewById(R.id.tv_title);
        et = findViewById(R.id.et);
        btnCancel = findViewById(R.id.btn_cancel);
        btnOk = findViewById(R.id.btn_ok);
        tvTitle.setText(title);
        btnCancel.setText(btnCancelText);
        btnOk.setText(btnOKText);
        et.setHint(hint);
        et.setText(defValue);

        btnCancel.setOnClickListener(this);
        btnOk.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.btn_cancel) {
            this.dismiss();
        } else if (id == R.id.btn_ok) {
            if (okLsn != null) {
                okLsn.onInputBack(this, getInputString());
            }
        }
    }


    public interface OnOkListener {
        void onInputBack(InputDialogZX inputDialogZX, String inputString);
    }

}
