package com.xvx.sdk.payment.utils;

import android.annotation.SuppressLint;
import android.os.CountDownTimer;
import android.support.v4.content.ContextCompat;
import android.widget.Button;
import com.android.OpenMtaSDK.R;

/**
 * 倒计时工具类<br>
 */
@SuppressLint("StringFormatMatches,SetTextI18n")
public class CountDownTimerUtil extends CountDownTimer {
   private Button timeButton;

   public CountDownTimerUtil(Button button, long millisInFuture, long countDownInterval) {
      super(millisInFuture, countDownInterval);
      this.timeButton = button;
   }

   //停止计时器
   public void onStop(CountDownTimer countDownTimer){
      countDownTimer.cancel();   //终止计时器
   }

   //计时过程
   @Override
   public void onTick(long l) {
      //防止计时过程中重复点击
      timeButton.setClickable(false);
      timeButton.setTextColor(ContextCompat.getColor(timeButton.getContext(), R.color.code_color_gray));
      timeButton.setText((l / 1000 + 1) + "秒后重新获取");
   }

   //计时完毕的方法
   @Override
   public void onFinish() {
      //重新给Button设置文字
      timeButton.setText("重新获取");
      timeButton.setTextColor(ContextCompat.getColor(timeButton.getContext(), R.color.code_color_blue));
      //设置可点击
      timeButton.setClickable(true);
   }
}
