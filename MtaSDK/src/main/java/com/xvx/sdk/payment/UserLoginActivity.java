package com.xvx.sdk.payment;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.ui.WebHtmlActivity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.OpenMtaSDK.R;
import com.android.core.XSEUtils;
import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.KeyboardUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.cazaea.sweetalert.SweetAlertDialog;
import com.nil.sdk.nb.utils.NbKeyBoardUtil;
import com.nil.sdk.ui.BaseAppCompatActivity;
import com.nil.sdk.ui.BaseUtils;
import com.nil.sdk.utils.ACacheUtils;
import com.nil.sdk.utils.AppUtils;
import com.nil.sdk.utils.Spu;
import com.nil.sdk.utils.StringUtils;
import com.xmb.mta.util.ResultBean;
import com.xmb.mta.util.XMBGson;
import com.xvx.sdk.payment.db.UserDb;
import com.xvx.sdk.payment.db.UserLoginDb;
import com.xvx.sdk.payment.utils.DataCheckUtils;
import com.xvx.sdk.payment.vo.OrderBeanV2;
import com.xvx.sdk.payment.vo.UserLoginVO;
import com.xvx.sdk.payment.vo.UserVO;
import com.xvx.sdk.payment.web.PayWebAPI;
import com.xvx.sdk.payment.web.PayWebApiCallback;

import okhttp3.Call;
import okhttp3.Response;

/**
 * 用户登录界面<r>
 */
public class UserLoginActivity extends BaseAppCompatActivity implements View.OnClickListener {
    private final static String TAG = UserLoginActivity.class.getSimpleName();
    ImageView ivAvatar;
    ImageView ivUsername;
    EditText etUsername;
    RelativeLayout vg1;
    ImageView ivPsk;
    EditText etPsk;
    Button btnLogin;
    Button btnRegister;
    View progress;
    TextView tvRetrievePassword;

    TextView tvLoginSign;
    CheckBox cbLoginSign;

    public static void start(Activity act) {
        start(act, null);
    }
    public static void start(Activity act, String title) {
        start(act, title, null);
    }
    public static void start(Activity act, String title, UserVO userVO) {
        Intent it = new Intent(act, UserLoginActivity.class);
        it.putExtra("title", title);
        it.putExtra("UserVO", userVO);
        BaseUtils.startActivity(act,it);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.xuser_activity_login);
        setDisplayHomeAsUpEnabled(true);

        ivAvatar = findViewById(R.id.iv_avatar);
        ivUsername =findViewById(R.id.iv_username);
        etUsername =findViewById(R.id.et_username);
        vg1 =findViewById(R.id.vg_1);
        ivPsk =findViewById(R.id.iv_psk);
        etPsk =findViewById(R.id.et_psk);
        btnLogin =findViewById(R.id.btn_login);
        btnRegister =findViewById(R.id.btn_register);
        progress =findViewById(R.id.progress);

        tvRetrievePassword = findViewById(R.id.tv_retrieve_password);
        tvRetrievePassword.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);
        tvRetrievePassword.getPaint().setAntiAlias(true);//抗锯齿

        btnLogin.setOnClickListener(this);
        btnRegister.setOnClickListener(this);
        tvRetrievePassword.setOnClickListener(this);

        String title = getIntent().getStringExtra("title");
        if(StringUtils.noNullStr(title)) {
            setTitle(title);
        }else {
            setTitle(getString(R.string.xuser_login_def_title));
        }

        UserVO userVO = (UserVO) getIntent().getSerializableExtra("UserVO");
        if(userVO == null){
            userVO = UserDb.get();
        }

        if(userVO != null){
            etUsername.setText(userVO.getUser_name());
            etPsk.setText(XSEUtils.mtaDecode(userVO.getUser_psk()));
        }

        UserLoginVO localUserBean = UserLoginDb.get();
        if (localUserBean != null) {
            next();
        }

        cbLoginSign = findViewById(R.id.cb_login_sign);
        tvLoginSign = findViewById(R.id.tv_login_sign);
        /*tvLoginSign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isSign = cbLoginSign.isChecked();
                cbLoginSign.setChecked(!isSign);
            }
        });
        LinearLayout llLoginSign;
        llLoginSign = findViewById(R.id.ll_login_sign);
        llLoginSign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isSign = cbLoginSign.isChecked();
                cbLoginSign.setChecked(!isSign);
            }
        });*/

        //注册即同意《隐私政策》和《用户协议》
        String signContent = getString(R.string.xuser_login_sign_tip,
                getString(R.string.privacy_policy_title),
                getString(R.string.user_agreement_title));
        callService(signContent, tvLoginSign);
    }

    private void callService(String content, TextView textView) {
        SpannableStringBuilder builder = new SpannableStringBuilder(content);
        int i = content.indexOf("《");//截取文字开始的下标
        int i2 = content.indexOf("》", i) + 1;

        int j = content.indexOf("《", i2);
        int j2 = content.indexOf("》", i2)+ 1;

        builder.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                WebHtmlActivity.startWithAssets(getActivity(),
                        getString(R.string.privacy_policy_title),
                        getString(R.string.xmta_assets_html_file_name_privacy_policy));
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(getResources().getColor(R.color.colorBlue));       //设置文字颜色
                ds.setUnderlineText(true);      //设置下划线//根据需要添加
            }
        }, i, i2, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                WebHtmlActivity.startWithAssets(getActivity(),
                        getString(R.string.user_agreement_title),
                        getString(R.string.xmta_assets_html_file_name_user_agreement));
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(getResources().getColor(R.color.colorBlue));       //设置文字颜色
                ds.setUnderlineText(true);      //设置下划线//根据需要添加
            }
        }, j, j2, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        textView.setHighlightColor(Color.TRANSPARENT); //设置点击后的颜色为透明，否则会一直出现高亮
        textView.setText(builder);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
    }

    public void onBtnLoginClicked() {
        final String user_name = etUsername.getText().toString();
        final String psk = etPsk.getText().toString();
        if (!DataCheckUtils.isValidUserName(user_name)) {
            etUsername.setError(getString(R.string.xuser_error_user_name));
            DataCheckUtils.requestFocus(etUsername);
            return;
        }
        if (!DataCheckUtils.isValidPsk(psk)) {
            etPsk.setError(getString(R.string.xuser_error_psk));
            DataCheckUtils.requestFocus(etPsk);
            return;
        }

        boolean isSign = cbLoginSign.isChecked();
        if(!isSign){
            // 创建水平缩放抖动动画
            ObjectAnimator animator1 = ObjectAnimator.ofFloat(cbLoginSign, "scaleX", 0.8f, 0.2f, 1.2f, 0.8f);
            animator1.setInterpolator(new AccelerateInterpolator());
            animator1.setDuration(500);
            // 创建垂直缩放抖动动画
            ObjectAnimator animator2 = ObjectAnimator.ofFloat(cbLoginSign, "scaleY", 0.8f, 0.2f, 1.2f, 0.8f);
            animator2.setInterpolator(new AccelerateInterpolator());
            animator2.setDuration(500);
            // 创建动画集合，并同时播放两个动画
            AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.playTogether(animator1, animator2);
            animatorSet.start();

            String msg = String.format("请勾选 %s", tvLoginSign.getText().toString());
            BaseUtils.popMyToast(getActivity(), msg);
            return;
        }

        //未开启支付（可以用admin、123456进行登录）：
        if(!OrderBeanV2.hasPay()
                && XSEUtils.decode("XWpscKr1dzWME1YoI0ym6ntAT3%2FLg%3D%3D").equals(user_name)
                && XSEUtils.decode("VD5GDIfYKikEIHdlBDGt30ORmtbbw%3D%3D").equals(psk)){
            KeyboardUtils.hideSoftInput(getActivity()); //隐藏软键盘
            NbKeyBoardUtil.getInstance(getActivity()).hide();

            //保存用户信息
            UserVO uu = new UserVO(user_name, XSEUtils.mtaEncode(psk));
            UserDb.save(uu);

            UserLoginDb.save(new UserLoginVO(2022112301, user_name, ""));

            ToastUtils.showShort(R.string.xuser_login_success);
            next();
            return;
        }

        progress.setVisibility(View.VISIBLE);

        final String packageName = AppUtils.getPackageName(this);
        PayWebAPI.userLogin(new PayWebApiCallback<ResultBean>() {
            @Override
            public void onFailure(Call call, Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ToastUtils.showLong(R.string.xuser_connect_fail);
                        progress.setVisibility(View.INVISIBLE);
                    }
                });
            }
            public void onResponse(final ResultBean obj, Call call, Response response) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "登录：ResultBean="+obj+",OrderBeanV2="+ OrderBeanV2.getOrderBean());
                        progress.setVisibility(View.INVISIBLE);
                        KeyboardUtils.hideSoftInput(getActivity()); //隐藏软键盘
                        NbKeyBoardUtil.getInstance(getActivity()).hide();

                        if (obj.getResult_code() == ResultBean.CODE_SUC) {
                            //保存用户信息
                            UserVO uu = new UserVO(user_name, XSEUtils.mtaEncode(psk));
                            UserDb.save(uu);

                            UserLoginVO loginInfo = XMBGson.getGson().fromJson(obj.getResult_data(), UserLoginVO.class);
                            UserLoginDb.save(loginInfo);
                            if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "登录：UserLoginDb="+UserLoginDb.get());

                            //登录成功初始化一些配置信息
                            loginSucConfig();

                            ToastUtils.showShort(R.string.xuser_login_success);
                            next();
                        } else {
                            //ToastUtils.showLong(obj.getResult_data());

                            Activity act = getActivity();
                            new SweetAlertDialog(act, SweetAlertDialog.WARNING_TYPE)
                                    .setTitleText(act.getString(R.string.xuser_login_fail))
                                    .setContentText(obj.getResult_data())
                                    .setConfirmText(act.getString(R.string.xuser_know_btn_text))
                                    .show();
                        }
                    }
                });
            }
        }, packageName,user_name, psk);
    }

    public void next(){
        //【Old】登录成功：重启app
        //BaseUtils.gotoMainUI(this);
        //finish();

        int size = ActivityUtils.getActivityList().size();
        /**
         * size<=1：重新登录时，会清除所有的Activity（除了登录界面），所有只有登录界面时，直接重新启动APP
         * size>1：一般为从个人中心或付费限制功能界面正常进入登录界面，则finish登录界面即可
         */
        if(size <= 1){
            //重新启动APP：
            BaseUtils.gotoActivity(this, ActivityUtils.getLauncherActivity());
        } else {
            //【New】登录成功：直接关闭登录界面
            finish();
        }
    }

    /**
     * 登录成功初始化一些配置信息，未完待续...<br>
     */
    public void loginSucConfig(){
        // 获取成功订单信息
        OrderBeanV2.updateOrderBean();
    }

    /**
     * 退出登录并返回登录界面（用于挤下线|登录过期的重新登录、修改密码使用）<br>
     */
    public static void logoutToLoginUI(){
        logoutToLoginUI(true);
    }
    public static void logoutToLoginUI(boolean isClearLogin){
        if(isClearLogin) {
            // 清理登录数据
            UserLoginDb.delete();

            //清除订单信息：
            OrderBeanV2.removeOrderBean();
            ACacheUtils.removeObject(OrderBeanV2.Cache_KEY);
        }
        // 清除栈内全部Activity
        ActivityUtils.finishToActivity(UserLoginActivity.class, true);
        // 重新启动LoginActivity
        BaseUtils.startActivity(UserLoginActivity.class);
    }

    /**
     * 退出登录并返回启动界面（用于个人中心的退出登录）<br>
     */
    public static void logoutToLauncherUI(Activity act){
        logoutToLauncherUI(act,true);
    }
    public static void logoutToLauncherUI(Activity act, boolean isClearLogin){
        if(isClearLogin) {
            // 清理登录数据
            UserLoginDb.delete();

            //清除订单信息：
            OrderBeanV2.removeOrderBean();
            ACacheUtils.removeObject(OrderBeanV2.Cache_KEY);
        }
        //清除栈内全部Activity，返回到主界面
        ActivityUtils.finishAllActivities();
        //重新启动APP：
        BaseUtils.gotoActivity(act, ActivityUtils.getLauncherActivity());

        //弹出退出登录成功提示
        ToastUtils.showLong(R.string.xuser_logoff_success_tip);
    }

    @Override
    public void onClick(View v) {
        KeyboardUtils.hideSoftInput(v); //隐藏输入法键盘

        if(v.getId() == R.id.btn_register){
            BaseUtils.startActivity(UserRegisterActivity.class);
            finish();
        }else if(v.getId() == R.id.btn_login){
            onBtnLoginClicked();
        }else if(v.getId() == R.id.tv_retrieve_password){
            BaseUtils.sendEmailRetrievePassword(getActivity());
        }
    }
}
