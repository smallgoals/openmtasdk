package com.xvx.sdk.payment;

public interface IPayDlgCallback {
    void sure();
    void cancel();
    void neutral();
}
