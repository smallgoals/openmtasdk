package com.xvx.sdk.payment.web;

import android.content.Context;

import com.android.OpenMtaSDK.R;
import com.android.core.XSEUtils;
import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nil.crash.utils.CrashApp;
import com.nil.sdk.ui.BaseUtils;
import com.nil.sdk.utils.ACache;
import com.nil.sdk.utils.ACacheUtils;
import com.nil.sdk.utils.Spu;
import com.nil.sdk.utils.StringUtils;
import com.nil.vvv.utils.AdSwitchUtils;
import com.nil.vvv.utils.Mtas;
import com.xmb.mta.util.ResultBean;
import com.xmb.mta.util.XMBApiCallback;
import com.xmb.mta.util.XMBGson;
import com.xmb.mta.util.XMBOkHttp;
import com.xmb.mta.util.XMBSign;
import com.xvx.sdk.payment.PayVipActivity;
import com.xvx.sdk.payment.db.UserLoginDb;
import com.xvx.sdk.payment.utils.Codes;
import com.xvx.sdk.payment.utils.PayUtils;
import com.xvx.sdk.payment.utils.Urls;
import com.xvx.sdk.payment.vo.OrderBeanV2;
import com.xvx.sdk.payment.vo.ViPConfigVO;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class PayWebAPI {
    private final static String TAG = PayWebAPI.class.getSimpleName();
    private static final String URL_DATA_HEAD = "?data=";

    /**
     * 加载ViP在线配置信息：get<br>
     */
    public static void loadViPConfig(String url, final PayWebApiCallback<ViPConfigVO> xmbApiCallback) {
        final ViPConfigVO cacheList = (ViPConfigVO) ACacheUtils.getAppCacheObject(ViPConfigVO.Cache_KEY);
        if(cacheList != null) {//优先读取缓存数据
            if(Spu.isSucK(TAG)) LogUtils.dTag(TAG,"优先读取缓存数据，数据本地缓存2小时.");
            if (xmbApiCallback != null) xmbApiCallback.onResponse(cacheList, null, null);
            return;
        }

        try {
            XMBOkHttp.doGet(url, new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    if (xmbApiCallback != null) xmbApiCallback.onFailure(call, e);
                }
                public void onResponse(Call call, Response response) throws IOException {
                    try {
                        Type type = new TypeToken<ViPConfigVO>() {}.getType();
                        final ViPConfigVO vo = new Gson().fromJson(response.body().string(), type);
                        //缓存为空时，开一个线程进行文件缓存
                        new Thread(new Runnable() {
                            public void run() {
                                // 数据本地缓存2小时
                                ACacheUtils.setAppCacheObject(ViPConfigVO.Cache_KEY, vo, ACache.TIME_HOUR*2);
                                if(Spu.isSucK(TAG)) LogUtils.dTag(TAG,"开始本地缓存数据，数据本地缓存2小时.");
                            }
                        }).start();

                        if (xmbApiCallback != null) xmbApiCallback.onResponse(vo, call, response);
                    } catch (Exception e) {
                        e.printStackTrace();
                        if (xmbApiCallback != null) xmbApiCallback.onFailure(call, e);
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            if (xmbApiCallback != null) xmbApiCallback.onFailure(null, e);
        }
    }
    public static void loadViPConfig(Context ctx, PayWebApiCallback<ViPConfigVO> xmbApiCallback){
        try {
            String url = ctx.getResources().getString(R.string.vip_config_url);
            loadViPConfig(url, xmbApiCallback);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void loadViPConfig(PayWebApiCallback<ViPConfigVO> xmbApiCallback){
        loadViPConfig(Utils.getApp(), xmbApiCallback);
    }

    /**
     * 解析订单信息，通过user_id和cid查询订单时可以共用<br>
     */
    private static void parseOrderInfo(ResultBean resultBean){
        if(resultBean == null) return;
        OrderBeanV2.removeOrderBean();
        if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, resultBean);
        if(ResultBean.CODE_SUC == resultBean.getResult_code()){
            UserLoginDb.saveLoginState(Codes.LoginState.TK_STATE_SUC);

            //System.err.println("resultBean-->"+resultBean.toString());

            ArrayList<OrderBeanV2> ary = XMBGson.getGson().fromJson(resultBean.getResult_data(),
                    new TypeToken<ArrayList<OrderBeanV2>>(){}.getType());
            //ACacheUtils.setCacheObject(OrderBeanV2.Cache_KEY_V2_ARY, ary);
            OrderBeanV2.setOrderBeans(ary);

            for (int i = ary.size() - 1; i >= 0; i--) {
                OrderBeanV2 obj = ary.get(i);
                if (obj.isVIP()) {
                    OrderBeanV2.setOrderBean(obj);
                    if(Spu.isSucK(TAG)) LogUtils.iTag(TAG,"会员登录:" + obj.toString());
                    break;
                }
            }
        }else{
            UserLoginDb.saveLoginState(resultBean.getResult_data());
            if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "普通用户登录:"+resultBean);
        }
    }

    /**
     * 通过Cid查询用户订单：get<br>
     */
    public static void queryOrderByCombinedID(final PayWebApiCallback<ResultBean> xmbApiCallback, String combined_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("combined_id", combined_id);
        map.put("app_pkg", AppUtils.getAppPackageName());
        String data = XMBSign.buildUrlData(map);

        try {
            XMBOkHttp.doGet(Urls.URL_ORDER_QUERY + URL_DATA_HEAD + data, new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    if (xmbApiCallback != null) xmbApiCallback.onFailure(call, e);
                }
                public void onResponse(Call call, Response response) throws IOException {
                    try {
                        ResultBean resultBean = XMBSign.deSignByResult4Obj(response);
                        parseOrderInfo(resultBean);
                        if (xmbApiCallback != null) xmbApiCallback.onResponse(resultBean, call, response);
                    } catch (Exception e) {
                        e.printStackTrace();
                        if (xmbApiCallback != null) xmbApiCallback.onFailure(call, e);
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void queryOrderByCombinedID(final PayWebApiCallback<ResultBean> xmbApiCallback) {
        queryOrderByCombinedID(xmbApiCallback, PayVipActivity.getCombinedIDV2());
    }

    /**
     * 同步查询支付宝验签<br>
     */
    public static void queryAlipayOrder(final PayWebApiCallback<ResultBean> xmbApiCallback, Map<String, String> map) {
        String data = XMBSign.buildUrlData(map);

        try {
            if(Spu.isSucK(TAG)) LogUtils.iTag(TAG,"同步查询支付宝支付验签");
            XMBOkHttp.doGet(Urls.URL_QUERY_ALIPAY_ORDER + URL_DATA_HEAD + data, new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    if (xmbApiCallback != null) xmbApiCallback.onFailure(call, e);
                }
                public void onResponse(Call call, Response response) throws IOException {
                    ResultBean resultBean = XMBSign.deSignByResult4Obj(response);
                    if (xmbApiCallback != null) xmbApiCallback.onResponse(resultBean, call, response);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 同步查询微信验签<br>
     */
    public static void queryWeixinPayOrder(final PayWebApiCallback<ResultBean> xmbApiCallback, Map<String, String> map) {
        String data = XMBSign.buildUrlData(map);

        try {
            if(Spu.isSucK(TAG)) LogUtils.iTag(TAG,"同步查询微信支付验签");
            XMBOkHttp.doGet(Urls.URL_QUERY_WEIXIN_ORDER + URL_DATA_HEAD + data, new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    if (xmbApiCallback != null) xmbApiCallback.onFailure(call, e);
                }
                public void onResponse(Call call, Response response) throws IOException {
                    ResultBean resultBean = XMBSign.deSignByResult4Obj(response);
                    if (xmbApiCallback != null) xmbApiCallback.onResponse(resultBean, call, response);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 用户注册：post<br>
     */
    public static void userRegister(final PayWebApiCallback<ResultBean> xmbApiCallback, String app_pkg, String user_name, String user_psk, String remark) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("app_pkg", app_pkg);
        map.put("user_name", user_name);
        map.put("user_psk", user_psk);
        map.put("remark", remark);

        String data = XMBSign.buildUrlData(map);
        try {
            Map<String, String> postMap = new HashMap<>();
            postMap.put("data", URLDecoder.decode(data, "UTF-8"));  //POS请求不需要进行Url编码
            XMBOkHttp.doPost(Urls.URL_USER_REGISTER, postMap, new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    if (xmbApiCallback != null) xmbApiCallback.onFailure(call, e);
                }
                public void onResponse(Call call, Response response) throws IOException {
                    ResultBean resultBean = XMBSign.deSignByResult4Obj(response);
                    if (xmbApiCallback != null) xmbApiCallback.onResponse(resultBean, call, response);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 用户登录：get<br>
     */
    public static void userLogin(final PayWebApiCallback<ResultBean> xmbApiCallback, String app_pkg, String user_name, String user_psk) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("user_name", user_name);
        map.put("user_psk", user_psk);
        map.put("app_pkg", app_pkg);

        String data = XMBSign.buildUrlData(map);

        try {
            XMBOkHttp.doGet(Urls.URL_USER_LOGIN + URL_DATA_HEAD + data, new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    if (xmbApiCallback != null) xmbApiCallback.onFailure(call, e);
                }
                public void onResponse(Call call, Response response) throws IOException {
                    ResultBean resultBean = XMBSign.deSignByResult4Obj(response);
                    if (xmbApiCallback != null) xmbApiCallback.onResponse(resultBean, call, response);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * 用户注销：post<br>
     */
    public static void userDestroy(final PayWebApiCallback<ResultBean> xmbApiCallback, String app_pkg, String user_name, String user_psk, String reason) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("user_name", user_name);
        map.put("user_psk", user_psk);
        map.put("app_pkg", app_pkg);
        map.put("reason", reason);

        String data = XMBSign.buildUrlData(map);

        try {
            Map<String, String> postMap = new HashMap<>();
            postMap.put("data", URLDecoder.decode(data, "UTF-8"));  //POS请求不需要进行Url编码
            XMBOkHttp.doPost(Urls.URL_USER_DESTROY, postMap, new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    if (xmbApiCallback != null) xmbApiCallback.onFailure(call, e);
                }
                public void onResponse(Call call, Response response) throws IOException {
                    ResultBean resultBean = XMBSign.deSignByResult4Obj(response);
                    if (xmbApiCallback != null) xmbApiCallback.onResponse(resultBean, call, response);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 用户修改密码：post<br>
     */
    public static void userUpdatePsk(final PayWebApiCallback<ResultBean> xmbApiCallback, String user_id, String old_psk, String new_psk) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("id", user_id);
        map.put("old_psk", old_psk);
        map.put("new_psk", new_psk);

        String data = XMBSign.buildUrlData(map);

        try {
            Map<String, String> postMap = new HashMap<>();
            postMap.put("data", URLDecoder.decode(data, "UTF-8"));  //POS请求不需要进行Url编码
            XMBOkHttp.doPost(Urls.URL_USER_UPDATE_PASSWORD, postMap, new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    if (xmbApiCallback != null) xmbApiCallback.onFailure(call, e);
                }
                public void onResponse(Call call, Response response) throws IOException {
                    ResultBean resultBean = XMBSign.deSignByResult4Obj(response);
                    if (xmbApiCallback != null) xmbApiCallback.onResponse(resultBean, call, response);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 通过UserID查询用户订单：get<br>
     */
    public static void queryOrderByUserID(final PayWebApiCallback<ResultBean> xmbApiCallback, String user_id, String token) {
        Map<String, String> map = new HashMap<>();
        map.put("user_id", user_id);
        map.put("token", token);

        String data = XMBSign.buildUrlData(map);

        try {
            XMBOkHttp.doGet(Urls.URL_QUERY_VIP_BY_USER_ID + URL_DATA_HEAD + data, new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    if (xmbApiCallback != null) xmbApiCallback.onFailure(call, e);
                }
                public void onResponse(Call call, Response response){
                    try {
                        ResultBean resultBean = XMBSign.deSignByResult4Obj(response);
                        parseOrderInfo(resultBean);
                        if (xmbApiCallback != null) xmbApiCallback.onResponse(resultBean, call, response);
                    } catch (Exception e) {
                        e.printStackTrace();
                        if (xmbApiCallback != null) xmbApiCallback.onFailure(call, e);
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 会员用户注册并绑定支付的订单：post<br>
     */
    public static void userRegisterAndBindViP(final PayWebApiCallback<ResultBean> xmbApiCallback, String app_pkg, String user_name, String user_psk){
        userRegisterAndBindViP(xmbApiCallback, app_pkg, user_name, user_psk, PayVipActivity.getCombinedIDV2());
    }
    public static void userRegisterAndBindViP(final PayWebApiCallback<ResultBean> xmbApiCallback, String app_pkg, String user_name, String user_psk, String cid){
        userRegisterAndBindViP(xmbApiCallback, app_pkg, user_name, user_psk, cid, null);
    }
    public static void userRegisterAndBindViP(final PayWebApiCallback<ResultBean> xmbApiCallback, String app_pkg, String user_name, String user_psk, String cid, String remark){
        Map<String, String> map = new HashMap<>();
        map.put("app_pkg", app_pkg);
        map.put("user_name", user_name);
        map.put("user_psk", user_psk);
        map.put("combined_id", cid);
        map.put("remark", remark);

        String data = XMBSign.buildUrlData(map);

        try {
            Map<String, String> postMap = new HashMap<>();
            postMap.put("data", URLDecoder.decode(data, "UTF-8"));  //POS请求不需要进行Url编码
            XMBOkHttp.doPost(Urls.URL_USER_REGISTER_AND_BIND_VIP, postMap, new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    if (xmbApiCallback != null) xmbApiCallback.onFailure(call, e);
                }
                public void onResponse(Call call, Response response) throws IOException {
                    ResultBean resultBean = XMBSign.deSignByResult4Obj(response);
                    if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "绑定：ResultBean="+resultBean+",OrderBeanV2="+ OrderBeanV2.getOrderBean());
                    if (xmbApiCallback != null) xmbApiCallback.onResponse(resultBean, call, response);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 普通用户注册或会员用户注册并绑定支付的订单<br>
     */
    public static void registerOrBindViP(final PayWebApiCallback<ResultBean> xmbApiCallback, String app_pkg, String user_name, String user_psk, String remark){
        // 判断是ViP才进行（注册并绑定），否则只进行注册操作
        if (OrderBeanV2.hasViP()) {
            userRegisterAndBindViP(xmbApiCallback, app_pkg, user_name, user_psk, PayVipActivity.getCombinedIDV2(), remark);
        } else {
            userRegister(xmbApiCallback, app_pkg, user_name, user_psk, remark);
        }
    }

    /**
     * 登录后仅调用User_id方式查询订单，否则调用Cid查询订单<br>
     */
    public static void queryOrderByCidOrUserID(final PayWebApiCallback<ResultBean> xmbApiCallback){
        if(UserLoginDb.isLogin()) {
            // 用户登录过，刚调用User_id方式查询订单
            String user_id = UserLoginDb.get().getId()+"";
            String token = UserLoginDb.get().getToken();
            queryOrderByUserID(xmbApiCallback, user_id, token);
        }else{
            queryOrderByCombinedID(xmbApiCallback);
        }
    }
    public static void queryOrderByUserID(final PayWebApiCallback<ResultBean> xmbApiCallback){
        if(UserLoginDb.isLogin()) {
            // 用户登录过，刚调用User_id方式查询订单
            String user_id = UserLoginDb.get().getId()+"";
            String token = UserLoginDb.get().getToken();
            queryOrderByUserID(xmbApiCallback, user_id, token);
        }
    }

    /**
     * 查询订单信息并缓存<br>
     */
    public static void queryOrder(final PayWebApiCallback<ResultBean> xmbApiCallback){
        String type = ACacheUtils.getCacheValue(OrderBeanV2.CHECK_VIP_TYPE_KEY);
        if(OrderBeanV2.CHECK_VIP_TYPE_CID.equalsIgnoreCase(type)){
            queryOrderByCombinedID(xmbApiCallback);
        }else if(OrderBeanV2.CHECK_VIP_TYPE_USER.equalsIgnoreCase(type)){
            queryOrderByUserID(xmbApiCallback);
        }else if(OrderBeanV2.CHECK_VIP_TYPE_CID_OR_USER.equalsIgnoreCase(type)){
            queryOrderByCidOrUserID(xmbApiCallback);
        }else if(OrderBeanV2.CHECK_VIP_TYPE_AUTO.equalsIgnoreCase(type)){
            // 老APP需要支持Cid或用户查询ViP
            if (PayUtils.isOldApp()) {
                queryOrderByCidOrUserID(xmbApiCallback);
            }else{
                //CatAndDog-AEdit-IPScan
                queryOrderByUserID(xmbApiCallback);
            }
        }else {
            //2021.2.20：过渡期已过，已去除Cid会员方式，之后全部通过用户判断是否为会员
            queryOrderByUserID(xmbApiCallback);
        }
    }

    /**
     * 更新网络时间<br>
     */
    public static Date getNetTime(String webUrl){
        // 每日更新网络时间
        Date d1 = (Date) ACacheUtils.getAppFileCacheObject(PayUtils.XPAY_NET_TIME_KEY);
        try {
            if (StringUtils.noNullStr(webUrl) && (d1 == null || BaseUtils.getIsDebug())) {
                long correctTime = 0;
                if (webUrl.contains("https://")) {
                    URL url = new URL(webUrl);
                    HttpsURLConnection uc = (HttpsURLConnection) url.openConnection();
                    uc.setHostnameVerifier(new HostnameVerifier() {
                        @Override
                        public boolean verify(String s, SSLSession sslSession) {
                            //将所有验证的结果都设为true
                            return true;
                        }
                    });
                    uc.setReadTimeout(5000);
                    uc.setConnectTimeout(5000);
                    uc.connect();
                    correctTime = uc.getDate();
                } else {
                    URL url = new URL(webUrl);
                    URLConnection uc = url.openConnection();
                    uc.setReadTimeout(5000);
                    uc.setConnectTimeout(5000);
                    uc.connect();
                    correctTime = uc.getDate();
                }

                if (correctTime > 0) {
                    d1 = new Date(correctTime);
                    // 网络时间缓存一天
                    ACacheUtils.setAppFileCacheObject(PayUtils.XPAY_NET_TIME_KEY, d1, ACache.TIME_DAY);
                }
            }
        }catch (Exception e){
            //e.printStackTrace();
            LogUtils.wTag(TAG, "getUpdateNetTime::"+webUrl+"-->"+ StringUtils.getExcetionInfo(e));
        }
        return d1;
    }
    public static void updateNetTime(){
        CrashApp.networkThread(new Runnable() {
            public void run() {
                Date date = getNetTime(AdSwitchUtils.Vs.date_url.value);
                if(date == null){
                    getNetTime(XSEUtils.decode(Mtas.defReserveDateUrl));
                }
            }
        });
    }

    /**
     * 【增删改查】用户的云端数据<br>
     * @param xmbApiCallback 回调接口
     * @param handleType update:存储/更新用户数据；get:获取用户数据；delete:删除用户数据
     * @param data_type 数据类型(如：play_history、install_date等)
     * @param userdata 数据Json
     * @param req_mode 请求方式（查询采用get方式，增删改采用post方式）
     */
    public static void userDataHandle(final XMBApiCallback<ResultBean> xmbApiCallback, String handleType, String data_type, String userdata, String req_mode){
        Map<String, String> map = new HashMap<>();
        map.put("user_id", UserLoginDb.getUserID());//用户的唯一ID
        map.put("token", UserLoginDb.getToken());//用户登录后得到的token
        map.put("type", handleType);//update:存储/更新用户数据；get:获取用户数据；delete:删除用户数据

        map.put("data_type", data_type);
        //只有update时，才需要填此数据：
        if(!"[Not Provided]".equalsIgnoreCase(userdata)) {
            map.put("userdata", userdata);
        }

        String data = XMBSign.buildUrlData(map);
        try {
            if(XMBOkHttp.REQ_MODE_POST.equals(req_mode)) {//pos请求
                Map<String, String> postMap = new HashMap<>();
                postMap.put("data", URLDecoder.decode(data, "UTF-8"));  //POS请求不需要进行Url编码
                XMBOkHttp.doPost(Urls.URL_USER_DATA, postMap, new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        if (xmbApiCallback != null) xmbApiCallback.onFailure(call, e);
                    }
                    public void onResponse(Call call, Response response) throws IOException {
                        ResultBean resultBean = XMBSign.deSignByResult4Obj(response);
                        if (xmbApiCallback != null) xmbApiCallback.onResponse(resultBean, call, response);
                    }
                });
            } else {//默认：get请求
                String _url = Urls.URL_USER_DATA + URL_DATA_HEAD + data;
                System.err.println("userDataHandle-->"+_url);
                XMBOkHttp.doGet(_url, new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        if (xmbApiCallback != null) xmbApiCallback.onFailure(call, e);
                    }
                    public void onResponse(Call call, Response response) throws IOException {
                        ResultBean resultBean = XMBSign.deSignByResult4Obj(response);
                        if (xmbApiCallback != null) xmbApiCallback.onResponse(resultBean, call, response);
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void getUserData(final XMBApiCallback<ResultBean> xmbApiCallback, String data_type){
        getUserData(xmbApiCallback, data_type, XMBOkHttp.REQ_MODE_GET);
    }
    public static void updateUserData(final XMBApiCallback<ResultBean> xmbApiCallback, String data_type, String userdata){
        updateUserData(xmbApiCallback, data_type, userdata, XMBOkHttp.REQ_MODE_POST);
    }
    public static void deleteUserData(final XMBApiCallback<ResultBean> xmbApiCallback, String data_type){
        deleteUserData(xmbApiCallback, data_type, XMBOkHttp.REQ_MODE_POST);
    }
    public static void getUserData(final XMBApiCallback<ResultBean> xmbApiCallback, String data_type, String req_mode){
        userDataHandle(xmbApiCallback, "get", data_type, "[Not Provided]", req_mode);//Not Provided：不提供
    }
    public static void updateUserData(final XMBApiCallback<ResultBean> xmbApiCallback, String data_type, String userdata, String req_mode){
        userDataHandle(xmbApiCallback, "update", data_type, userdata, req_mode);
    }
    public static void deleteUserData(final XMBApiCallback<ResultBean> xmbApiCallback, String data_type, String req_mode){
        userDataHandle(xmbApiCallback, "delete", data_type, "[Not Provided]", req_mode);//Not Provided：不提供
    }

    /**
     * 用户登录：get<br>
     */
    public static void getSmsCode(final PayWebApiCallback<ResultBean> xmbApiCallback, String tel, String sms_config_key, String code) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("tel", tel);
        map.put("sms_config_key", sms_config_key);
        map.put("code", code);

        String data = XMBSign.buildUrlData(map);

        try {
            Map<String, String> postMap = new HashMap<>();
            postMap.put("data", URLDecoder.decode(data, "UTF-8"));  //POS请求不需要进行Url编码
            XMBOkHttp.doPost(Urls.URL_SMS_CODE + URL_DATA_HEAD + data, postMap, new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    if (xmbApiCallback != null) xmbApiCallback.onFailure(call, e);
                }
                public void onResponse(Call call, Response response) throws IOException {
                    ResultBean resultBean = XMBSign.deSignByResult4Obj(response);
                    if (xmbApiCallback != null) xmbApiCallback.onResponse(resultBean, call, response);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
