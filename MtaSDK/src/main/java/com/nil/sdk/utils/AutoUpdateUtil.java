package com.nil.sdk.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.annotation.Keep;
import android.view.View;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.EncryptUtils;
import com.blankj.utilcode.util.FileUtils;
import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.PathUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.nil.sdk.nb.utils.NbFileUtils;
import com.nil.vvv.utils.AdSwitchUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * APP自动更新升级<br>
 * 手动更新：AutoUpdateUtil.checkUpdate(getActivity(), false);
 * 自动检测更新：AutoUpdateUtil.checkUpdate(getActivity(), true);
 */
public class AutoUpdateUtil {
    /**
     * 用户手动检查更新
     *
     * @param act
     * @param isAutoCheck true:后台自动检测；false:用户手动点击检测
     */
    public static void checkUpdate(final Activity act, boolean isAutoCheck) {
        String update_config = AdSwitchUtils.getInstance().getOnlineValue(AdSwitchUtils.Vs.update_config);
        final UpdateConfigBean configBean = GsonUtils.fromJson(update_config, UpdateConfigBean.class);
        if (configBean != null && AppUtils.getAppVersionCode() < configBean.versionCode) {
            if (!isAutoCheck || configBean.isAutoTip()) {
                /**
                 * 强制更新：只有“立即更新”按钮，点击按钮后不关闭对话框<br>
                 * 非强制更新：有“取消、立即更新”按钮，点击按钮后关闭对话框<br>
                 */
                AlertDialog.Builder bd = new AlertDialog.Builder(act);
                final AlertDialog dialog = bd.setTitle("发现新版本")
                        .setMessage(configBean.getDes())
                        .setPositiveButton("立即更新",null)
                        .setCancelable(false)
                        .create();
                if(!configBean.isForceUpdate()) {
                    dialog.setButton(AlertDialog.BUTTON_NEGATIVE, "取消", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                }
                dialog.show(); //先调用show才能设置下面的配置

                //为了避免点击 positive 按钮后直接关闭 dialog,把点击事件拿出来设置：
                dialog.getButton(AlertDialog.BUTTON_POSITIVE)
                        .setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                downloadAndInstall(configBean.getApkUrl());

                                //非强制更新时，点“立即更新”关闭对话框
                                if(!configBean.isForceUpdate) dialog.dismiss();
                            }
                        });
            }
        } else {
            //用户检测才提示：
            if (!isAutoCheck)
                new AlertDialog.Builder(act)
                        .setMessage("当前已经是最新版本")
                        .setPositiveButton("确定", null).show();
        }
    }

    private static void downloadAndInstall(final String url) {
        if (!NbFileUtils.isUrl(url)) return;

        //文件名：url的md5做为文件名称
        final String fileName = EncryptUtils.encryptMD5ToString(url) + ".apk";
        //修改升级的安装包存储在手机SD卡的内部存储目录（此目录不需要存储权限）：
        final String path = PathUtils.getExternalAppFilesPath() + File.separator + fileName;
        if(FileUtils.isFileExists(path)){
            ToastUtils.showLong("安装包已下载，开始安装...");
            //安装
            AppUtils.installApp(path);
            XMBEventUtils.send(XMBEventUtils.EventType.update_apk, url);
            return;
        }

        OkHttpClient okHttpClient = new OkHttpClient();
        Request request = new Request.Builder().url(url).build();
        okHttpClient.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                ToastUtils.showLong("下载失败，请重试！");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                ToastUtils.showLong("下载中，请稍后...");

                // 下载 outputStream inputStream
                InputStream inputStream = response.body().byteStream();
                //未下载完成时，添加个bak后缀；下载完成后再重命名回来：
                String incompletePath = path + ".bak";
                File file = new File(incompletePath);
                //当文件不存在，创建出来
                FileUtils.createOrExistsFile(file);
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                byte[] bytes = new byte[1024];
                int readLength = 0;
                while ((readLength = inputStream.read(bytes)) != -1) {
                    fileOutputStream.write(bytes, 0, readLength);
                }
                inputStream.close();
                fileOutputStream.close();

                //下载完成后，进行文件重命名：
                FileUtils.rename(file, fileName);

                //下载完成后，开始安装：
                ToastUtils.showLong("下载完成，开始安装...");
                AppUtils.installApp(path);
                XMBEventUtils.send(XMBEventUtils.EventType.update_apk, url);
            }
        });
    }

    @Keep
    public class UpdateConfigBean implements Serializable {
        //        {
//            "versionCode":2010211,
//                "apkUrl":"www.a.com/xxx.apk",
//                "isAutoTip":true,
//                "isForceUpdate":false,
//                "des":"发现新版本，我们更新了更多实用的功能，赶紧更新吧"
//        }
        private int versionCode;
        private String apkUrl;
        private boolean isAutoTip;
        private boolean isForceUpdate;
        private String des;

        public int getVersionCode() {
            return versionCode;
        }

        public void setVersionCode(int versionCode) {
            this.versionCode = versionCode;
        }

        public String getApkUrl() {
            return apkUrl;
        }

        public void setApkUrl(String apkUrl) {
            this.apkUrl = apkUrl;
        }

        public boolean isAutoTip() {
            return isAutoTip;
        }

        public void setAutoTip(boolean autoTip) {
            isAutoTip = autoTip;
        }

        public boolean isForceUpdate() {
            return isForceUpdate;
        }

        public void setForceUpdate(boolean forceUpdate) {
            isForceUpdate = forceUpdate;
        }

        public String getDes() {
            return des;
        }

        public void setDes(String des) {
            this.des = des;
        }
    }
}
