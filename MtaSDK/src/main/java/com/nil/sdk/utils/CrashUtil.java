package com.nil.sdk.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.os.Looper;
import android.widget.Toast;

import com.blankj.utilcode.util.LogUtils;

import java.lang.Thread.UncaughtExceptionHandler;
import java.util.ArrayList;
import java.util.List;

/**
 * @declaration 程序意外Crash后自动重启<br>
 */
public class CrashUtil implements UncaughtExceptionHandler {
	public static void main(String[] args) {
//		CatchExcApplication app = (CatchExcApplication) getApplication();
//		app.init("");  
//		app.addActivity(this); 
	}
	
	public class CatchExcApplication extends Application {
		ArrayList<Activity> list = new ArrayList<Activity>();  
	    public void init(String packagename){  
	        //设置该CrashHandler为程序的默认处理器    
	        CrashUtil catchExcep = new CrashUtil(this, packagename);  
	        Thread.setDefaultUncaughtExceptionHandler(catchExcep);   
	    }  
	      
	    /** 
	     * Activity关闭时，删除Activity列表中的Activity对象*/  
	    public void removeActivity(Activity a){  
	        list.remove(a);  
	    }  
	      
	    /** 
	     * 向Activity列表中添加Activity对象*/  
	    public void addActivity(Activity a){  
	        list.add(a);   
	    }  
	      
	    /** 
	     * 关闭Activity列表中的所有Activity*/  
	    public void finishActivity(){  
	        for (Activity activity : list) {    
	            if (null != activity) {    
	                activity.finish();    
	            }    
	        }  
	        //杀死该应用进程  
	       android.os.Process.killProcess(android.os.Process.myPid());    
	    }
	}
    
   private Thread.UncaughtExceptionHandler mDefaultHandler;
   public final String TAG = "CatchExcep";
   CatchExcApplication application;
   String packagename;

   public CrashUtil(CatchExcApplication application, String packagename) {
       // 获取系统默认的UncaughtException处理器
       mDefaultHandler = Thread.getDefaultUncaughtExceptionHandler();
       this.application = application;
       this.packagename = packagename;
   }

   @Override
   public void uncaughtException(Thread thread, Throwable ex) {
       if (!handleException(ex) && mDefaultHandler != null) {
           // 如果用户没有处理则让系统默认的异常处理器来处理
           mDefaultHandler.uncaughtException(thread, ex);
       } else {
           try {
               Thread.sleep(2000);
           } catch (InterruptedException e) {
               LogUtils.eTag(TAG, "error : ", e);
           }
           Intent intent = getMainIntent(application.getApplicationContext(), packagename);
           @SuppressLint("WrongConstant")
		   PendingIntent restartIntent = PendingIntent.getActivity(
                   application.getApplicationContext(), 0, intent,
                   Intent.FLAG_ACTIVITY_NEW_TASK);
           // 退出程序
           AlarmManager mgr = (AlarmManager) application.getSystemService(Context.ALARM_SERVICE);
           mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 1000,restartIntent); // 1秒钟后重启应用
           application.finishActivity();
       }
   }

   /**
    * 自定义错误处理,收集错误信息 发送错误报告等操作均在此完成.
    * 
    * @param ex
    * @return true:如果处理了该异常信息;否则返回false.
    */
   private boolean handleException(Throwable ex) {
       if (ex == null) {
           return false;
       }
       // 使用Toast来显示异常信息
       new Thread() {
           @Override
           public void run() {
               Looper.prepare();
               Toast.makeText(application.getApplicationContext(),"很抱歉,程序出现异常,即将退出.", Toast.LENGTH_SHORT).show();
               Looper.loop();
           }
       }.start();
       return true;
   }
   
   private Intent getMainIntent(Context ctx,String packagename) {
	   Intent it = null;
		// 通过包名获取此APP详细信息，包括Activities、services、versioncode、name等等
		PackageInfo pi = null;
		try {
			pi = ctx.getPackageManager().getPackageInfo(packagename, 0);
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}finally{
			if (pi == null) {
				return it;
			}
		}

		// 创建一个类别为CATEGORY_LAUNCHER的该包名的Intent
		Intent rIt = new Intent(Intent.ACTION_MAIN, null);
		rIt.addCategory(Intent.CATEGORY_LAUNCHER);
		rIt.setPackage(pi.packageName);

		// 通过getPackageManager()的queryIntentActivities方法遍历
		List<ResolveInfo> resolveinfoList = ctx.getPackageManager().queryIntentActivities(rIt, 0);

		ResolveInfo ri = resolveinfoList.iterator().next();
		if (ri != null) {
			// packagename = 参数packname
			String packageName = ri.activityInfo.packageName;
			// 这个就是我们要找的该APP的LAUNCHER的Activity[组织形式：packagename.mainActivityname]
			String className = ri.activityInfo.name;
			// LAUNCHER Intent
			it = new Intent(Intent.ACTION_MAIN);
			it.addCategory(Intent.CATEGORY_LAUNCHER);

			// 设置ComponentName参数1:packagename参数2:MainActivity路径
			ComponentName cn = new ComponentName(packageName, className);
			it.setComponent(cn);
		}
		
		return it;
	}
}
