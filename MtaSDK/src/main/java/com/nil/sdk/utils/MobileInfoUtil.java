package com.nil.sdk.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.text.format.Formatter;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.Utils;
import com.nil.vvv.utils.AdSwitchUtils;
import com.xmb.mta.util.XMBSign;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.NetworkInterface;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;

/**
 * @declaration 获取手机信息工具(如：系统版本、网卡地址、IP信息、应用、内存等信息)<br>
 * @author ksA2y511xh@163.com<br>
 *2014-11-22 下午9:33:29<br>
 */
@SuppressWarnings("deprecation")
@SuppressLint("MissingPermission,HardwareIds")
public class MobileInfoUtil{
	//CurMobileInfo@@[Manufacturer=Teclast,Model=G18mini(C5B9),CoreVersion=3.4.5,OsVersion=4.2.2--MacAddress=58:12:43:d1:ce:70,CpuInfo=ARMv7 Processor rev 2 (v7l) &amp;0,MemoryInfo=1.00 GB&amp;487 MB,--Imsi=null,Phone=null,HasRoot=true,HasNetwork=true,IP=172.168.100.104],AllApp@@[nil.app.tsinfo30_TsLogInfo####com.android.browser,appNums=110]
	private static final String TAG = MobileInfoUtil.class.getSimpleName();
	private static Context mContext;
	private static MobileInfoUtil sMobileInfoUtil;
	public static MobileInfoUtil getInstance(Context act){
		if(sMobileInfoUtil == null){
			sMobileInfoUtil = new MobileInfoUtil();
		}
		mContext = act;
		return sMobileInfoUtil;
	}
	public String getAllInfo(){
		return "getAllInfo";
	}
	
	public String getBriefInfo(){
		return "getBriefInfo";
	}
	
	public String getCurIP(){
		return "getCurIP";
	}
	
	private String intToIp(int i) {
		return (i & 0xFF) + "." + ((i >> 8) & 0xFF) + "." + ((i >> 16) & 0xFF)
				+ "." + (i >> 24 & 0xFF);
	}
    
	public String getLocalIpAddress() {
		return "getLocalIpAddress";
	}

	@SuppressLint("WifiManagerPotentialLeak")
	public String getMacAddress() {
		if(mContext == null
				|| !AdSwitchUtils.Sws.cj_mac.flag
				|| !AppUtils.hasMacPermission(Utils.getApp())
				|| !AppUtils.hasAgreePrivacyPolicy()) return "";
		StringBuilder result = new StringBuilder();
		try{
			WifiManager wifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
			WifiInfo wifiInfo = wifiManager.getConnectionInfo();
			result.append(getMac()).append("/");
			result.append(wifiInfo.getSSID()).append("_").append(wifiInfo.getBSSID());
			if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "macAdd:" + result);
		}catch(Exception e){e.printStackTrace();}
		return result.toString();
	}
	
	public String getAllApp() {
		if(mContext == null
				|| !AdSwitchUtils.Sws.cj_apps.flag
				|| !AppUtils.hasAgreePrivacyPolicy()) return "";
		String result = "",sysResult="", packInfo = "";
		int yf =0,st=0;
		try {
			List<PackageInfo> packages = mContext.getPackageManager().getInstalledPackages(0);
			for (PackageInfo i : packages) {
				try {
					if ((i.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) == 0) {//user app
						yf++;
						//ico-->mContext.getPackageManager().getApplicationIcon(i.applicationInfo)
						result += i.applicationInfo.packageName + "_V" + i.versionName + "#" + i.versionCode + "$"
								+ i.applicationInfo.loadLabel(mContext.getPackageManager()).toString() + ",";
					} else if ((i.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) == 1) {//system app
						st++;
						sysResult += i.applicationInfo.packageName + "_" + i.applicationInfo.loadLabel(mContext.getPackageManager()).toString() + ",";
					}
				} catch (Exception e) {/*end for item*/}
			}
			packInfo = (packages == null) ? "app is null" : "appNums=" + yf + "--" + st + "/" + packages.size();
		}catch (Exception e){/*end for*/}
		return result.substring(0, result.length() - 1)+"\n\n"+sysResult+"\n"+packInfo;
	}
	
	/**NO##应用名称##应用包名##版本号##友盟ID##友盟渠道名称##应用别名##证书名称##应用市场信息*/
	public String getNilApp(){
		if(mContext == null
				|| !AdSwitchUtils.Sws.cj_apps.flag
				|| !AppUtils.hasAgreePrivacyPolicy()) return "";
		String result = "";
		int yf =0;
		try {
			List<PackageInfo> packages = mContext.getPackageManager().getInstalledPackages(0);
			for (PackageInfo i : packages) {
				if ((i.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) == 0) {//user app
					String mz = i.applicationInfo.loadLabel(mContext.getPackageManager()).toString();
					String pn = i.applicationInfo.packageName;
					String vv = "V" + i.versionName + "_" + i.versionCode;
					result += StringUtils.concat("##", (++yf), mz, pn, vv) + ",";
				}
			}
		}catch (Exception e){/*end for*/}
		return result.substring(0, result.length() - 1);
	}
	
	public String getMemoryInfo() {  
		if(mContext == null) return "";
        String[] result = {"",""};  //1-total 2-avail  
        ActivityManager am = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        MemoryInfo mi = new MemoryInfo();
        am.getMemoryInfo(mi);
        
        long mTotalMem = 0;  
        long mAvailMem = mi.availMem;  
        String str1 = "/proc/meminfo";  
        String str2;  
        String[] arrayOfString;  
        try {  
            FileReader localFileReader = new FileReader(str1);  
            BufferedReader localBufferedReader = new BufferedReader(localFileReader, 8192);  
            str2 = localBufferedReader.readLine();  
            arrayOfString = str2.split("\\s+");  
            mTotalMem = Integer.valueOf(arrayOfString[1]).intValue() * 1024;  
            localBufferedReader.close();  
        } catch (IOException e) {  
            e.printStackTrace();  
        }  
        result[0] = Formatter.formatFileSize(mContext, mTotalMem);
        result[1] = Formatter.formatFileSize(mContext, mAvailMem);
        if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "meminfo total:" + result[0] + " used:" + result[1]);
        return result[1]+"/"+result[0];  
    }
	
	public String getCpuInfo() {  
        String str1 = "/proc/cpuinfo";  
        String str2 = "";  
        String[] cpuInfo = {"", ""};  //1-cpu型号  //2-cpu频率  
        String[] arrayOfString;  
        try {  
            FileReader fr = new FileReader(str1);  
            BufferedReader localBufferedReader = new BufferedReader(fr, 8192);  
            str2 = localBufferedReader.readLine();  
            arrayOfString = str2.split("\\s+");  
            for (int i = 2; i < arrayOfString.length; i++) {  
                cpuInfo[0] = cpuInfo[0] + arrayOfString[i] + " ";  
            }  
            str2 = localBufferedReader.readLine();  
            arrayOfString = str2.split("\\s+");  
            cpuInfo[1] += arrayOfString[2];  
            localBufferedReader.close();  
        } catch (IOException e) {  
        }  
        if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "cpuinfo:" + cpuInfo[0] + " " + cpuInfo[1]);
        return cpuInfo[0] + "&" + cpuInfo[1];  
    }
	
	public String getCoreInfo() {
		String _result = "";
		Process process = null;
		try {
			process = Runtime.getRuntime().exec("cat /proc/version");
		} catch (IOException e) {
			e.printStackTrace();
		}

		// get the output line
		InputStream outs = process.getInputStream();
		InputStreamReader isrout = new InputStreamReader(outs);
		BufferedReader brout = new BufferedReader(isrout, 8 * 1024);

		String result = "";
		String line;
		// get the whole standard output string
		try {
			while ((line = brout.readLine()) != null) {
				result += line;
				// result += "\n";
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (result != "") {
			String Keyword = "version ";
			int index = result.indexOf(Keyword);
			LogUtils.vTag(TAG, result);
			line = result.substring(index + Keyword.length());
			index = line.indexOf(" ");
			_result = line.substring(0, index);
		}
		return _result;
	}
	
    /**
     * Role:获取当前设置的电话号码
     * <br>Date:2012-3-12
     * <br>@author CODYY)peijiangping
     */
	public String getNativePhoneNumber() {
    	if(mContext == null) return "";
        String NativePhoneNumber=null;
        try {
			TelephonyManager telephonyManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
			NativePhoneNumber = telephonyManager.getLine1Number();
		}catch (Exception e){
        	e.printStackTrace();
		}
        return NativePhoneNumber;
    }
 
    /**
     * Role:Telecom service providers获取手机服务商信息 <br>
     * 需要加入权限<uses-permission android:name="android.permission.READ_PHONE_STATE"/> <br>
     * Date:2012-3-12 <br>
     *
     * @author CODYY)peijiangping
     */
    public String getProvidersName() {
    	if(mContext == null) return "";
        String ProvidersName = null;
        TelephonyManager telephonyManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
        // 返回唯一的用户ID;就是这张卡的编号神马的
        String IMSI = (telephonyManager == null)? "": telephonyManager.getSubscriberId();
        // IMSI号前面3位460是国家，紧接着后面2位00 02是中国移动，01是中国联通，03是中国电信。
        //System.out.println(IMSI);
        if (IMSI.startsWith("46000") || IMSI.startsWith("46002")) {
            ProvidersName = "中国移动";
        } else if (IMSI.startsWith("46001")) {
            ProvidersName = "中国联通";
        } else if (IMSI.startsWith("46003")) {
            ProvidersName = "中国电信";
        }
        return ProvidersName;
    }
    
    /**
     * 检测手机是否已Root<br>
     * @return
     */
	public boolean hasRoot() {
		Process process = null;
		DataOutputStream os = null;
		String command = "ls /data/data";
		try {
			process = Runtime.getRuntime().exec("su");
			os = new DataOutputStream(process.getOutputStream());
			os.writeBytes(command + "\n");
			os.writeBytes("exit\n");
			os.flush();
			process.waitFor();
		} catch (Exception e) {
			if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "ROOT REE" + e.getMessage());
			return false;
		} finally {
			try {
				if (os != null) {
					os.close();
				}
				process.destroy();
			} catch (Exception e) {
			}
		}
		if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "Root SUC ");
		return true;
	}
    /**
     * 检测手机是否已Root,无Root提示框<br>
     */
	public boolean hasRootNoTip() {
		String binPath = "/system/bin/su";
		String xBinPath = "/system/xbin/su";
		if (new File(binPath).exists() && isExecutable(binPath))
			return true;
		if (new File(xBinPath).exists() && isExecutable(xBinPath))
			return true;
		return false;
	}
	private static boolean isExecutable(String filePath) {
		/**
		 * ls -l /system/xbin/su<br>
		 * -rwsr-sr-x root     root        30208 2014-10-27 17:07 su<br>
		 */
		Process p = null;
		try {
			p = Runtime.getRuntime().exec("ls -l " + filePath);
			// 获取返回内容
			BufferedReader in = new BufferedReader(new InputStreamReader(
					p.getInputStream()));
			String str = in.readLine();
			if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, (str == null || "".equals(str)) ? "isExecutable@@str is null": str);
			if (str != null && str.length() >= 4) {
				char flag = str.charAt(3);//-rwsr
				if (flag == 's' || flag == 'x')
					return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(p!=null){
				p.destroy();
			}
		}
		return false;
	}
	
	/**#########################SD Info...begin#######################*/
	/**
	 * 获得SD卡总大小<br>
	 * @return
	 */
	public String getSDTotalSize() {
		if(mContext == null) return "";
		File path = Environment.getExternalStorageDirectory();
		StatFs stat = new StatFs(path.getPath());
		long blockSize = stat.getBlockSize();
		long totalBlocks = stat.getBlockCount();
		return Formatter.formatFileSize(mContext, blockSize * totalBlocks);
	}

	/**
	 * 获得sd卡剩余容量，即可用大小<br>
	 * @return
	 */
	public String getSDAvailableSize() {
		if(mContext == null) return "";
		File path = Environment.getExternalStorageDirectory();
		StatFs stat = new StatFs(path.getPath());
		long blockSize = stat.getBlockSize();
		long availableBlocks = stat.getAvailableBlocks();
		return Formatter.formatFileSize(mContext, blockSize * availableBlocks);
	}

	/**
	 * 获得机身内存总大小<br>
	 * @return
	 */
	public String getRomTotalSize() {
		if(mContext == null) return "";
		File path = Environment.getDataDirectory();
		StatFs stat = new StatFs(path.getPath());
		long blockSize = stat.getBlockSize();
		long totalBlocks = stat.getBlockCount();
		return Formatter.formatFileSize(mContext, blockSize * totalBlocks);
	}

	/**
	 * 获得机身可用内存<br>
	 * @return
	 */
	public String getRomAvailableSize() {
		if(mContext == null) return "";
		File path = Environment.getDataDirectory();
		StatFs stat = new StatFs(path.getPath());
		long blockSize = stat.getBlockSize();
		long availableBlocks = stat.getAvailableBlocks();
		return Formatter.formatFileSize(mContext, blockSize * availableBlocks);
	}
	/**#########################SD Info...end#######################*/

	public String getDeviceModelName() {
		String manufacturer = "未知型号";
		try {
			manufacturer = Build.MANUFACTURER;
			String model = Build.MODEL;
			if (model != null && model.startsWith(manufacturer)) {
				return capitalize(model);
			} else {
				return capitalize(manufacturer) + " " + model;
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		return manufacturer;
	}
	public String capitalize(String s) {
		if (s == null || s.length() == 0) {
			return "";
		}
		char first = s.charAt(0);
		if (Character.isUpperCase(first)) {
			return s;
		} else {
			return Character.toUpperCase(first) + s.substring(1);
		}
	}
	public String getDeviceVersionName() {
		String v = "未知版本";
		try{
			v = Build.DISPLAY;
		}catch (Exception e){
			e.printStackTrace();
		}
		return v;
	}

	public String getAllBuildInformation() {
		StringBuffer buf = new StringBuffer();
		try {
			String s;
			Field[] fields = Build.class.getDeclaredFields();
			for (Field field : fields) {
				try {
					field.setAccessible(true);
					s = "Build." + field.getName() + " : " + field.get(null);
					buf.append(s).append("\n");
				} catch (Exception e) {
					LogUtils.eTag(TAG, "an error occured when cj crash info", e);
				}
			}

			Field[] fieldsVersion = Build.VERSION.class.getDeclaredFields();
			for (Field field : fieldsVersion) {
				try {
					field.setAccessible(true);
					s = "Build.VERSION." + field.getName() + " : " + field.get(null);
					buf.append(s).append("\n");
				} catch (Exception e) {
					LogUtils.eTag(TAG, "an error occured when cj crash info", e);
				}
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		return buf.toString();
	}
	// 手机制造商
	public String getProduct() {
		return Build.PRODUCT;
	}
	// 系统定制商
	public String getBrand() {
		return Build.BRAND;
	}
	// 硬件制造商
	public String getManufacturer() {
		return Build.MANUFACTURER;
	}
	// 硬件名称
	public String getHardWare() {
		return  Build.HARDWARE;
	}
	// 型号
	public String getMode() {
		return Build.MODEL;
	}
	// Android 系统版本
	public String getAndroidVersion() {
		return  Build.VERSION.RELEASE;
	}
	// CPU 指令集，可以查看是否支持64位
	public String getCpuAbi() {
		return Build.CPU_ABI;
	}

	public String getMsgHead(){
		String s = "空消息头";
		if(mContext != null) {
			try {
				Context context = mContext;
				DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.CHINA);
				String buildDateAsString = com.nil.sdk.utils.AppUtils.getAppBuildTime(context);
				s = "";
				s += "应用名称: " + AppUtils.getAppName(context) + " \n";
				s += "应用版本: " + AppUtils.getVersionName(context) + "_" + AppUtils.getVersionCode(context) + " \n";
				s += "应用包名: " + context.getPackageName() + " \n";
				s += "编译日期: " + buildDateAsString + " \n";
				s += "当前日期: " + dateFormat.format(new Date()) + " \n";
				s += "系统版本: " + "Android " + Build.VERSION.RELEASE + " \n";
				String userID = ACacheUtils.getCacheValue(XMBSign.Cjs.user_id.name());
				if (StringUtils.noNullStr(userID)) userID = " " + userID;
				s += "手机版本: " + getDeviceVersionName() + "|" + Build.HARDWARE + " " + Build.CPU_ABI + userID + " \n";
				s += "手机型号: " + getDeviceModelName() + " \n \n";
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return s;
	}

	/**###############################UMeng B###################################*/
	@SuppressLint("HardwareIds")
	public String getDeviceIMEI() {
		Context context = mContext;
		String device_id = null;
		try {
			android.telephony.TelephonyManager tm = (android.telephony.TelephonyManager) context
					.getSystemService(Context.TELEPHONY_SERVICE);
			if (tm != null && checkPermission(context, Manifest.permission.READ_PHONE_STATE)) {
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
					device_id = tm.getImei();
				}else {
					try {
						device_id = tm.getDeviceId();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}

			if (StringUtils.isNullStr(device_id)) {
				try {
					device_id = android.provider.Settings.Secure.getString(
							context.getContentResolver(),android.provider.Settings.Secure.ANDROID_ID);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			if (StringUtils.isNullStr(device_id)) {
				device_id = getMac();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return device_id;
	}
	@SuppressLint("HardwareIds")
	public String getDeviceCode() {
		if(mContext == null
				|| !AdSwitchUtils.Sws.cj_tel.flag
				|| !AppUtils.hasPhonePermission(Utils.getApp())
				|| !AppUtils.hasAgreePrivacyPolicy()) return "";

		Context context = mContext;
		String tel = null;
		try {
			android.telephony.TelephonyManager tm = (android.telephony.TelephonyManager) context
					.getSystemService(Context.TELEPHONY_SERVICE);
			if (tm != null && checkPermission(context, Manifest.permission.READ_PHONE_STATE)) {
				tel = tm.getLine1Number();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tel;
	}

	public String getMac() {
		Context context = mContext;
		String mac = null;
		if(context != null) {
			if (Build.VERSION.SDK_INT < 23) {
				mac = getMacBySystemInterface();
			} else {
				mac = getMacByJavaAPI();
				if (TextUtils.isEmpty(mac)) {
					mac = getMacBySystemInterface();
				}
			}
		}
		if(StringUtils.isNullStr(mac)){
			mac = "06:05:04:03:02:01";	// 默认MAC地址
		}
		return mac;
	}

	@TargetApi(9)
	private String getMacByJavaAPI() {
		try {
			Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
			while (interfaces.hasMoreElements()) {
				NetworkInterface netInterface = interfaces.nextElement();
				if ("wlan0".equals(netInterface.getName()) || "eth0".equals(netInterface.getName())) {
					byte[] addr = netInterface.getHardwareAddress();
					if (addr == null || addr.length == 0) {
						return null;
					}
					StringBuilder buf = new StringBuilder();
					for (byte b : addr) {
						buf.append(String.format("%02X:", b));
					}
					if (buf.length() > 0) {
						buf.deleteCharAt(buf.length() - 1);
					}
					return buf.toString().toLowerCase(Locale.getDefault());
				}
			}
		} catch (Throwable e) {
		}
		return null;
	}

	private String getMacBySystemInterface() {
		Context context = mContext;
		if (context == null) {
			return "";
		}
		try {
			WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
			if (checkPermission(context, Manifest.permission.ACCESS_WIFI_STATE)) {
				WifiInfo info = wifi.getConnectionInfo();
				return info.getMacAddress();
			} else {
				return "";
			}
		} catch (Throwable e) {
			return "";
		}
	}

	public static boolean checkPermission(Context context, String permission) {
		boolean result = false;
		if (context == null) {
			return result;
		}
		if (Build.VERSION.SDK_INT >= 23) {
			try {
				Class <?> clazz = Class.forName("android.content.Context");
				Method method = clazz.getMethod("checkSelfPermission", String.class);
				int rest = (Integer) method.invoke(context, permission);
				if (rest == PackageManager.PERMISSION_GRANTED) {
					result = true;
				} else {
					result = false;
				}
			} catch (Throwable e) {
				result = false;
			}
		} else {
			PackageManager pm = context.getPackageManager();
			if (pm.checkPermission(permission, context.getPackageName()) == PackageManager.PERMISSION_GRANTED) {
				result = true;
			}
		}
		return result;
	}
	/**###############################UMeng E###################################*/
}
