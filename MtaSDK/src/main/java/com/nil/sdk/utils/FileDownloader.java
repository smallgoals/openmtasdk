package com.nil.sdk.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * 文件下载器<br>
 */
public class FileDownloader {
	public static void download(final String fileUrl, final File desFile, final IDownload lsn) {
		new Thread() {
			public void run() {
				// 先下载到临时文件
				File tempFile = new File(desFile.getAbsoluteFile() + ".temp");
				InputStream in = null;
				FileOutputStream fos = null;
				try {
					URLConnection conn = new URL(fileUrl).openConnection();
					in = conn.getInputStream();
					int fileLen = conn.getContentLength();
					System.out.println(">>>文件大小：" + fileLen);
					// fos = new FileOutputStream(desFile);
					fos = new FileOutputStream(tempFile);
					byte[] buffer = new byte[2048];
					int len = -1;
					int dowloadedLen = 0;
					while ((len = in.read(buffer)) != -1) {
						fos.write(buffer, 0, len);
						dowloadedLen += len;
						if (lsn != null) {
							lsn.downloadProgress((int) Math.round(dowloadedLen
									* 100.0 / fileLen));
						}
					}
					// 下载完成，再把临时文件更名成正式文件：
					tempFile.renameTo(desFile);
					if (lsn != null) {
						lsn.downloadSuced(desFile);
						lsn.downloadFinished();
					}
				} catch (MalformedURLException e) {
					e.printStackTrace();
					if (lsn != null) {
						lsn.downloadFaild(e, "请求的URL地址失效");
						lsn.downloadFinished();
					}
				} catch (ConnectException e) {
					e.printStackTrace();
					if (lsn != null) {
						lsn.downloadFaild(e, "连接服务器失败");
						lsn.downloadFinished();
					}
				} catch (IOException e) {
					e.printStackTrace();
					if (lsn != null) {
						lsn.downloadFaild(e, "读取失败");
						lsn.downloadFinished();
					}
				} finally {
					try {
						fos.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
					try {
						in.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			};
		}.start();
	}
	public interface IDownload {
		void downloadSuced(final File desFile);
		void downloadFinished();
		void downloadFaild(Exception e, String eStr);
		void downloadProgress(int progress);
	}
	public static abstract class DefDownload implements IDownload{
		public abstract void downloadSuced(final File desFile);
		public void downloadFinished(){}
		public void downloadFaild(Exception e, String eStr){}
		public void downloadProgress(int progress){}
	}
}