package com.nil.sdk.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.widget.Toast;

import com.blankj.utilcode.util.LogUtils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Locale;

/**
 * 功能性工具类
 * 
 * @author ksA2y511xhs
 * 
 */
public class FunUtils {
	private static final String TAG = "FunUtils";
	public static final int MAK = 1;
	
	/**
	 *  密钥解密
	 * @param strMak
	 * @return
	 */
	public static String decodeSerial(final String strMak){
		if (strMak == null || "".equals(strMak)){
			return null;
		}
		int len = strMak.length();
		ByteBuffer buf = ByteBuffer.allocate(strMak.length());
		buf.order(ByteOrder.LITTLE_ENDIAN);
		buf.put(strMak.getBytes());
		for (int i = 0; i < len; i++){
			buf.array()[i] -= MAK;
		}
		return new String(buf.array());
	}
	
	/**
	 *  密钥加密
	 * @param strMak
	 * @return
	 */
	public static String encodeSerial(final String strMak){
		if (strMak == null || "".equals(strMak)){
			return null;
		}
		int len = strMak.length();
		ByteBuffer buf = ByteBuffer.allocate(strMak.length());
		buf.order(ByteOrder.LITTLE_ENDIAN);
		buf.put(strMak.getBytes());
		for (int i = 0; i < len; i++){
			buf.array()[i] += MAK;
		}
		return new String(buf.array());
	}

	/**
	 * 查找序列号是否存?
	 * @param spu
	 * @param strMak<br>false:不存?<br> true:存在
	 * @return
	 */
	public static boolean findSerial(Spu spu, final String strMak){
		if (strMak == null || "".equals(strMak)){
			return false;
		}
//		String keyName = FunUtils.encodeSerial(new String(ComUtil.getBufByLen(strMak, ConstantInfo.MAK_LENGTH).array()));
//		String mak = spu.loadStringSharedPreference(keyName/*1+""*/);
//		int serialNums = spu.loadIntSharedPreference(ConstantInfo.SERIAL_NUMS);
//		LogUtils.iTag("MSG", "MAK value:"+ mak+",serialNums:"+serialNums);
		
		String mak = spu.loadStringSharedPreference(strMak.trim());
		if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "MAK value:"+ mak);
		return (mak == null) ? false : true;
	}
	
	public static String concatString(String split, Object... intAry) {
		if (intAry == null) {
			return null;
		}

		int len = intAry.length;
		StringBuffer strBuf = new StringBuffer();
		for (int i = 0; i < len - 1; i++) {
			strBuf.append(intAry[i]);
			strBuf.append(split);
		}
		strBuf.append(intAry[len - 1]);
		return strBuf.toString();
	}

	public static void exportFile(String fileName, List<String> nodeData)
			throws IOException {
		File expFile = new File(fileName);
		BufferedWriter bufWriter = new BufferedWriter(new FileWriter(expFile));

		// 写入节点信息
		int nodeSize = nodeData.size();
		bufWriter.write("AllNodeInfo:\n");
		bufWriter.write("NodeSize:" + nodeSize + "\n");
		bufWriter.write("NodeFlags:" + concatString(AppConstantInfo.SPLIT_CHAR, "device_types","device_name", "ip", "port","stream_types", "gallery", "username", "user_password") + "\n");
		for (int i = 0; i < nodeSize; i++) {
			bufWriter.write("Node" + i + ":" + nodeData.get(i) + "\n");
		}
		bufWriter.flush();
		bufWriter.close();
	}

	public static void importFile(String fileName, List<String> nodeData)
			throws FileNotFoundException, IOException {
		nodeData.clear();

		File impFile = new File(fileName);
		BufferedReader bufReader = new BufferedReader(new FileReader(impFile));
		String strLine = null;
		String strData = null;

		// 读取节点信息
		strLine = bufReader.readLine();
		if (!"AllNodeInfo:".equals(strLine)){
			if (bufReader != null) bufReader.close();
			throw new IOException();
		}
		strLine = bufReader.readLine();
		int nodeSize = Integer
				.parseInt(strLine.substring(strLine.indexOf(":") + 1));
		bufReader.readLine();
		for (int i = 0; i < nodeSize; i++) {
			strLine = bufReader.readLine();
			strData = strLine.substring(strLine.indexOf(":") + 1);
			nodeData.add(strData);
		}
		if (bufReader != null) bufReader.close();
	}
	
	public static String getExternalStoragePath() {
		boolean exists = Environment.getExternalStorageState().equals(
				android.os.Environment.MEDIA_MOUNTED);
		if (exists) {
			return Environment.getExternalStorageDirectory().getAbsolutePath();
		} else {
			return "/";
		}
	}
	
	public static int dip2px(Context context, float dipValue) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (dipValue * scale + 0.5f);
	}

	public static int px2dip(Context context, float pxValue) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (pxValue / scale + 0.5f);
	}
	
	/**
	 * 获取屏幕分辨?
	 * 
	 * @param activity
	 * @return int[0]: width,int[1]:height
	 */
	public static int [] getScreenSize(Activity activity){
		DisplayMetrics dm = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
		int [] screenSize = new int [2];
		screenSize[0] = dm.widthPixels;
		screenSize[1] = dm.heightPixels;
		return screenSize;
	}
	
	/**
	 * 得到指定长度的byte数组，不足时补空
	 * @param value
	 * @param len
	 * @return
	 */
	public static ByteBuffer getBufByLen(String value, int len){
		return getBufByLen(value.getBytes(), len);
	}
	
	/**
	 * 得到指定长度的byte数组，不足时补空
	 * @param value
	 * @param len
	 * @return
	 */
	public static ByteBuffer getBufByLen(byte[] value, int len){
		ByteBuffer buf = ByteBuffer.allocate(len);
		buf.order(ByteOrder.LITTLE_ENDIAN);
		buf.put(value);
		buf.rewind();
		return buf;
	}
	
	/**
	 * 得到语言类型
	 * @return 0 ?体中?,1 繁体中文, 2 英文
	 */
	public static int getLanguageTypes(){
		int flags = 0;
		String str = Locale.getDefault().getLanguage();
		if ("zh".equals(str)){
			str = Locale.getDefault().getCountry();
			if ("CN".equals(str)){
				flags = 0;
			}else if("TW".equals(str)){
				flags = 1;
			}
		}else {
			flags = 2;
		}
		return flags;
	}
	
	public static Bitmap loadOrigPic(String filePath, float w, float h) {
		Bitmap bm = null;
		if (bm == null) {
			Options op = new Options();
			op.inJustDecodeBounds = true;
			BitmapFactory.decodeFile(filePath, op);
			op.inJustDecodeBounds = false;
			if (op.outHeight < h && op.outWidth < w) {
				op.inSampleSize = 1;
			} else {
				float heightscale = op.outHeight / h;
				float widthscale = op.outWidth / w;
				float maxscale = Math.max(heightscale, widthscale);
				int scale = 1;
				for (; (maxscale / 2) > 1;) {
					scale *= 2;
					maxscale = maxscale / 2;
				}
				op.inSampleSize = scale; 
			}
			try {
				bm = BitmapFactory.decodeFile(filePath, op);
				if (bm.getWidth() > bm.getHeight()
						&& (op.inSampleSize > 1 || bm.getWidth() > w || bm
								.getHeight() > h)) {
					float ht = h * bm.getHeight() / bm.getWidth();
					if (ht - (int) ht > 0.5) {
						ht = (int) ht + 1;
					}
					bm = Bitmap.createScaledBitmap(bm, (int) w, (int) ht, false);
				} else if (bm.getWidth() <= bm.getHeight()
						&& (op.inSampleSize > 1 || bm.getWidth() > w || bm
								.getHeight() > h)) {
					float wt = w * bm.getWidth() / bm.getHeight();
					if (wt - (int) wt > 0.5) {
						wt = (int) wt + 1;
					}
					bm = Bitmap.createScaledBitmap(bm, (int) wt, (int) h, false);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "原图读取完毕");
		return bm;
	}
	
	public static Bitmap loadOrigPic(Resources res, int resID, float w, float h) {
		Bitmap bm = null;
		if (bm == null) {
			Options op = new Options();
			op.inJustDecodeBounds = true;
			BitmapFactory.decodeResource(res, resID, op);
			op.inJustDecodeBounds = false;
			if (op.outHeight < h && op.outWidth < w) {
				op.inSampleSize = 1;
			} else {
				float heightscale = op.outHeight / h;
				float widthscale = op.outWidth / w;
				float maxscale = Math.max(heightscale, widthscale);
				int scale = 1;
				for (; (maxscale / 2) > 1;) {
					scale *= 2;
					maxscale = maxscale / 2;
				}
				op.inSampleSize = scale; 
			}
			try {
				bm = BitmapFactory.decodeResource(res, resID, op);
				if (bm.getWidth() > bm.getHeight()
						&& (op.inSampleSize > 1 || bm.getWidth() > w || bm
								.getHeight() > h)) {
					float ht = h * bm.getHeight() / bm.getWidth();
					if (ht - (int) ht > 0.5) {
						ht = (int) ht + 1;
					}
					bm = Bitmap.createScaledBitmap(bm, (int) w, (int) ht, false);
				} else if (bm.getWidth() <= bm.getHeight()
						&& (op.inSampleSize > 1 || bm.getWidth() > w || bm
								.getHeight() > h)) {
					float wt = w * bm.getWidth() / bm.getHeight();
					if (wt - (int) wt > 0.5) {
						wt = (int) wt + 1;
					}
					bm = Bitmap.createScaledBitmap(bm, (int) wt, (int) h, false);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "原图读取完毕");
		return bm;
	}
	
	/**
	 * 从资源中读取得到位图对象
	 * 
	 * @param context
	 * @param resId
	 *            资源ID
	 * @return 位图对象
	 */
	public static Bitmap readBitMapByRes(Context context, int resId){ 
		BitmapFactory.Options opt = new BitmapFactory.Options(); 
		opt.inPreferredConfig = Bitmap.Config.RGB_565; 
		opt.inPurgeable = true; 
		opt.inInputShareable = true; 
		//获取资源图片 
		InputStream is = context.getResources().openRawResource(resId); 
		return BitmapFactory.decodeStream(is,null,opt); 
	} 
	
	public static String getStringByLength(long fileLength){
		float perLength = 1024.0f;
		String str = null;
		double _len = fileLength;
		if (_len < perLength) {
			str = String.format("%.0fBytes", _len);
			return str;
		}
		
		_len /= perLength;
		if (_len < perLength) {
			str = String.format("%.2fKB", _len);
			return str;
		}

		_len /= perLength;
		if (_len < perLength) {
			str = String.format("%.2fMB", _len);
			return str;
		}
		
		_len /= perLength;
		if (_len < perLength) {
			str = String.format("%.2fGB", _len);
			return str;
		}

		_len /= perLength;
		if (_len < perLength) {
			str = String.format("%.2fTB", _len);
			return str;
		}
		
		_len /= perLength;
		if (_len < perLength) {
			str = String.format("%.2fPB", _len);
			return str;
		}
		
		_len /= perLength;
		if (_len < perLength) {
			str = String.format("%.2fEB", _len);
			return str;
		}

		_len /= perLength;
		if (_len < perLength) {
			str = String.format("%.2fZB", _len);
			return str;
		}
		
		_len /= perLength;
		if (_len < perLength) {
			str = String.format("%.2fYB", _len);
			return str;
		}
		return str;
	}
	
	public static FilenameFilter getFilenameFilter(final String extNames){
		FilenameFilter filter = new FilenameFilter() {

			@Override
			public boolean accept(File dir, String filename) {
				File file = new File(dir.getAbsolutePath() + "/" + filename);
				if (file.isDirectory()) {
					return true;
				}

				String name = filename.toLowerCase();
				String exts = extNames;

				String[] temp = exts.split(AppConstantInfo.SPLIT_CHAR);
				for (String s : temp) {
					if (name.endsWith(s)) {
						return true;
					}
				}
				return false;
			}
		};
		return filter;
	}
	
	/**
	 * 显示一提示消息
	 * @param msg
	 * @param context
	 */
	public static void sendToast(String msg, Context context){
		Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
	}
	
	/**
	 * 得到手机唯一ID，为IMEI、Device ID、Android ID、Wifi Mac、Bluetooth Mac字符组合的MD5码前?12?<br>
	 * 参考链接：http://cloudstack.blog.163.com/blog/static/1876981172012710823152/<br>
	 * @param context
	 * @return 12位字符ID
	 */
	@SuppressLint("DefaultLocale")
	public static String getDeviceUID(Context context){
		 //1 compute IMEI
        TelephonyManager TelephonyMgr = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
    	String m_szImei = TelephonyMgr.getDeviceId(); // Requires READ_PHONE_STATE
    	
        //2 compute DEVICE ID
        String m_szDevIDShort = "35" + //we make this look like a valid IMEI
        	Build.BOARD.length()%10+ Build.BRAND.length()%10 + 
        	Build.CPU_ABI.length()%10 + Build.DEVICE.length()%10 + 
        	Build.DISPLAY.length()%10 + Build.HOST.length()%10 + 
        	Build.ID.length()%10 + Build.MANUFACTURER.length()%10 + 
        	Build.MODEL.length()%10 + Build.PRODUCT.length()%10 + 
        	Build.TAGS.length()%10 + Build.TYPE.length()%10 + 
        	Build.USER.length()%10 ; //13 digits
        
        //3 android ID - unreliable
        String m_szAndroidID = Secure.getString(context.getContentResolver(), Secure.ANDROID_ID); 
        
        //4 wifi manager, read MAC address - requires  android.permission.ACCESS_WIFI_STATE or comes as null
        WifiManager wm = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);
        String m_szWLANMAC = wm.getConnectionInfo().getMacAddress();

//        //5 Bluetooth MAC address  android.permission.BLUETOOTH required
//        BluetoothAdapter m_BluetoothAdapter	= null; // Local Bluetooth adapter
//    	m_BluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
//    	String m_szBTMAC = m_BluetoothAdapter.getAddress();
    	
    	//6 SUM THE IDs
    	String m_szLongID = m_szImei + m_szDevIDShort + m_szAndroidID+ m_szWLANMAC/* + m_szBTMAC*/;
    	MessageDigest m = null;
		try {
			m = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} 
		m.update(m_szLongID.getBytes(),0,m_szLongID.length());
		byte p_md5Data[] = m.digest();
		
		String m_szUniqueID = new String();
		for (int i=0;i<p_md5Data.length;i++) {
			int b =  (0xFF & p_md5Data[i]);
			// if it is a single digit, make sure it have 0 in front (proper padding)
			if (b <= 0xF) m_szUniqueID+="0";
			// add number to string
			m_szUniqueID+=Integer.toHexString(b); 
		}
		m_szUniqueID = m_szUniqueID.toUpperCase();
		m_szUniqueID = (m_szUniqueID == null || m_szUniqueID.length() < 12)?"0000000000000000":m_szUniqueID.substring(0, 12);
		return m_szUniqueID;
	}
	
	private static final float ROUND_RATE = (float) 0.1;// 圆角比例
	/**
	 * 处理成圆角（如果程序内存溢出了，可能就是这里?
	 * 
	 * @param bitmap
	 * @return
	 */
	public static Bitmap getRoundCornerImage(Bitmap bitmap) {
		if (bitmap == null) {
			return null;
		}
		// 创建一个和原始图片一样大小位图
		Bitmap roundConcerImage = Bitmap.createBitmap(bitmap.getWidth(),
				bitmap.getHeight(), Config.ARGB_8888);
		// 创建带有位图roundConcerImage的画布
		Canvas canvas = new Canvas(roundConcerImage);
		// 创建画笔
		Paint paint = new Paint();
		// 创建一个和原始图片一样大小的矩形
		int bmW = bitmap.getWidth();
		int bmH = bitmap.getHeight();
		Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
		RectF rectF = new RectF(rect);
		// 去锯齿
		paint.setAntiAlias(true);
		// 画一个和原始图片一样大小的圆角矩形
		canvas.drawRoundRect(rectF, bmW * ROUND_RATE, bmH * ROUND_RATE, paint);
		// 设置相交模式
		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		// 把图片画到矩形去
		canvas.drawBitmap(bitmap, null, rect, paint);
		bitmap.recycle();
		bitmap = null;
		System.gc();
		return roundConcerImage;
	}
}
