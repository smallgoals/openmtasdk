package com.nil.sdk.utils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class StringUtils {
	public static boolean noNullStr(String src){return !isNullStr(src);}
	public static boolean isNullStr(String src){
		return isNullStr(src, true);
	}
	public static boolean isNullStr(String src, boolean isTrim){
		return (src == null) || ("".equals(isTrim ? src.trim() : src));
	}
	public static boolean isNullStr(Object[] obj){
		return (obj == null) || (obj.length < 1);
	}
	public static boolean isNullStr(List<?> list){
		return (list == null) || (list.isEmpty());
	}
	public static boolean isNullStr(Map<?,?> map){
		return (map == null) || (map.isEmpty());
	}
	public static String concat(String split, Object... src){
		String r = null;
		if(src != null && src.length > 0){
			if(split == null) split = "";
			StringBuilder buf = new StringBuilder();
			for(Object str : src){
				buf.append(str).append(split);
			}
			buf.append("end");
			r = buf.toString().replace(split+"end", "");
		}
		return r;
	}
	public static String concatObjectDef(Object... src){
		return concat("\n",src);
	}
	public static boolean containsIgnoreCase(String src, String kw){
		boolean isOK = false;
		if(!isNullStr(src) && !isNullStr(kw)){
			isOK = src.toLowerCase().contains(kw.toLowerCase());
		}
		return isOK;
	}
	public static boolean containsIgnoreCaseHead(String src, String kw){
		boolean isOK = false;
		if(!isNullStr(src) && !isNullStr(kw) && src.length() >= kw.length()){
			int len = kw.length();
			isOK = src.substring(0, len).toLowerCase().contains(kw.toLowerCase());
		}
		return isOK;
	}
	/*
	public static boolean containsIgnoreCasePy(String src, String kw){
		boolean isOK = false;
		if(!isNullStr(src) && !isNullStr(kw)){
			isOK = src.toLowerCase().contains(kw.toLowerCase());
			try{
				if(!isOK) {
					isOK = PinyinUtils.getPinYin(src.toLowerCase()).contains(PinyinUtils.getPinYin(kw.toLowerCase()));
				}
			}catch(Exception e){}
		}
		return isOK;
	}
	public static boolean containsIgnoreCaseAllPy(String src, String kw){
		boolean isOK = false;
		if(!isNullStr(src) && !isNullStr(kw)){
			isOK = src.toLowerCase().contains(kw.toLowerCase());
			try{
				if(!isOK) {
					isOK = PinyinUtils.getAllPinYin(src.toLowerCase()).contains(PinyinUtils.getAllPinYin(kw.toLowerCase()));
				}
			}catch(Exception e){}
		}
		return isOK;
	}
	*/
	public static boolean equalsIgnoreCase(String src, String kw){
		boolean isOK = false;
		if(!isNullStr(src) && !isNullStr(kw)){
			isOK = src.equalsIgnoreCase(kw);
		}
		return isOK;
	}
	public static String getValue(String src, String def){
		String r = null;
		if(!isNullStr(src)){
			r = src;
		}else{
			r =  def;
		}
		return r;
	}
	public static String[] getValues(String src, String def, int vNum){
		String[] r = null;
		if(!isNullStr(src) && (vNum > 0) && src.split("##").length>=vNum){
			r = src.split("##");
		}else{
			r =  def.split("##");
		}
		return r;
	}
	public static String getValue2Cs(String src, String cs){
		return getValue2Cs(src,cs,"##");
	}
	public static String getValue2Cs(String src, String cs, String split){
		String r = null;
		if(!isNullStr(src)){
			r = src.replace(split, cs);
		}
		return r;
	}
	public static String getPrintInfo(String connector, String keys, Object...values){
		String r = null;
		if(!isNullStr(keys) && values != null){
			StringBuffer buf = new StringBuffer();
			String[] ary = keys.split("##");
			int len = (ary.length > values.length) ? values.length : ary.length;
			for(int i = 0; i < len; i++){
				Object obj = values[i];
				buf.append(ary[i]).append("：").append(obj).append(connector);
			}
			buf.append("end");
			r = buf.toString().replace(connector+"end", "");
		}
		return r;
	}
	public static String getObjectName(Object obj){
		String name = "null";
		if(obj != null){
			name = obj.getClass().getName();
		}
		return name;
	}
	public static String getObjectSimpleName(Object obj){
		String name = "null";
		if(obj != null){
			name = obj.getClass().getSimpleName();
		}
		return name;
	}
	
	// 将字母转换成数字_1
	public static String letterToNum(String input) {
		String reg = "[a-zA-Z]";
		StringBuffer strBuf = new StringBuffer();
		input = input.toLowerCase();
		if (!isNullStr(input)) {
			for (char c : input.toCharArray()) {
				if (String.valueOf(c).matches(reg)) {
					strBuf.append(c - 96);
				} else {
					strBuf.append(c);
				}
			}
			return strBuf.toString();
		} else {
			return input;
		}
	}

	// 将数字转换成字母
	public static String numToLetter(String input) {
		StringBuffer buf = new StringBuffer();
		if(input != null && !"".equals(input.trim())){
			for (byte b : input.getBytes()) {
				buf.append((char) (b + 48));
			}
		}
		return buf.toString();
	}
	
	public static String[] getReverse(String... input) {
		int size = (input == null)? 0: input.length;
		String[] buf = null;
		if(size > 0){
			buf = new String[size];
			for(int i = 0; i < size; i++){
				if(!isNullStr(input[i])){
					buf[i] = "";
					char[] ary = input[i].toCharArray();
					int len = ary.length;
					for(int ii = len; ii > 0; ii--){
						buf[i] += ary[ii-1];
					}
				}
			}
		}
		return buf;
	}
	
	public static String getAdPosID(String ids){
		return getAdPosID(ids, "\\$");
	}
	public static String getAdPosID(String ids, String split){
		String posID = "id";
		if(!isNullStr(ids) && !isNullStr(split)){
			String[] ss = ids.split(split);
			posID = ss[new Random().nextInt(ss.length)];
		}
		return posID;
	}
	public static String getAdPosID(String ids, String split, int pos){
		String posID = "id";
		if(!isNullStr(ids) && !isNullStr(split)){
			String[] ss = ids.split(split);
			posID = ss[pos%ss.length];
		}
		return posID;
	}

	public static String  getKeyValue(String ids){
		return getKeyValue(ids, "##", 0);
	}
	public static String getKeyValue(String ids, String split, int pos){
		String posID = "key";
		if(!isNullStr(ids) && !isNullStr(split)){
			String[] ss = ids.split(split);
			posID = ss[pos%ss.length];
		}
		return posID;
	}

	public static String replace(String src, String obj, String... targets){
		String r = src;
		if(noNullStr(src) && noNullStr(obj) && targets != null){
			for(String s : targets){
				if(isNullStr(s)) continue;
				r = r.replace(s, obj);
			}
		}
		return r;
	}
	public static String replaceAll(String src, String obj, String... targets){
		String r = src;
		if(noNullStr(src) && noNullStr(obj) && targets != null){
			for(String s : targets){
				if(isNullStr(s)) continue;
				r = r.replaceAll(s, obj);
			}
		}
		return r;
	}

	/**
	 * LogUtils.eTag(TAG, StringUtils.getExcetionInfo(e));<br>
	 */
	public static String getExcetionInfo(Exception e){
		String info = "e is null";
		if(e != null){
			info = e.toString() + "\n" +
					Arrays.toString(e.getStackTrace());
		}
		return info;
	}
}