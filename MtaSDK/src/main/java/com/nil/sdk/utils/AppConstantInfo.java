package com.nil.sdk.utils;

/**
 * 配置名常量
 * 
 * @author ksA2y511xhs
 * 
 */
public class AppConstantInfo {
	/** 默认存储根目录，一般为：/sdcard/PackageName */
	public static final String RootDirPath = (FunUtils.getExternalStoragePath()+"/").replaceAll("//", "/")+"MainFrameLib";
	/** 默认存储图片目录，一般为：/sdcard/PackageName/images */
	public static final String ImageDirPath = RootDirPath +"/images";
	/** 默认存储录像目录，一般为：/sdcard/PackageName/videos */
	public static final String VideoDirPath = RootDirPath +"/videos";
	public static final String AudioDirPath = RootDirPath +"/audios";

	public static final String SPLIT_CHAR = "@@";
	public static final String VideoExts = FunUtils.concatString(AppConstantInfo.SPLIT_CHAR, ".h264", ".mp4");
	public static final String ImageExts = FunUtils.concatString(AppConstantInfo.SPLIT_CHAR, ".jpg", ".jpeg",".png",".gif");
	public static final String ENCRYPT_TYPE = "encrypt_type"; //0:none 1:aes 2:base
}
