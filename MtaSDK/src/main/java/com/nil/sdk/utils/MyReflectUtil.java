package com.nil.sdk.utils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @declaration 反射工具类<br>
 * 				类使用说明如下：<br>
 *              1.MyReflectUtil mru = new MyReflectUtil(类名);<br>
 *              2.mru.callMethod(方法名); /mru.setFiled(属性名,属性值);/...<br>
 * 2014-1-13 下午2:00:59<br>
 */
public class MyReflectUtil {
	/**
	 * 1.className:<br>
	 * a.普通类：com.temp.test.TestReflect<br>
	 * b.类中类：com.temp.test.TestReflect$PrivateClass<br>
	 * */
	/** 类实例*/
	private Class<?> mClass = null;
	/** 类对象*/
	private Object mObj = null;
	
	public MyReflectUtil(String className){
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		try {
			mClass = loader.loadClass(className);
			if(mClass != null) mObj = mClass.newInstance();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 配置私有变量<br>
	 * @param fieldName 变量名
	 * @param value 变量值
	 * @return 是否调用成功
	 */
	public boolean setFiled(String fieldName, Object value){
		boolean isOK = false;
		if(mClass != null && mObj != null){
			//配置私有变量
			try {
				Field colorFid = mClass.getDeclaredField(fieldName);
				colorFid.setAccessible(true);// 取消java语言访问检查以访问private变量
				colorFid.set(mObj, value);
				isOK = true;
			} catch (NoSuchFieldException e) {
				e.printStackTrace();
			} catch (SecurityException e) {//1
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}	
		}
		return isOK;
	}
	/**
	 * 获取私有变量<br>
	 * @param fieldName 变量名
	 * @return 返回私有变量的值
	 */
	public Object getFiled(String fieldName){
		Object value = null; 
		if(mClass != null && mObj != null){
			//配置私有变量
			try {
				Field colorFid = mClass.getDeclaredField(fieldName);
				colorFid.setAccessible(true);// 取消java语言访问检查以访问private变量
				value = colorFid.get(mObj);
			} catch (NoSuchFieldException e) {
				e.printStackTrace();
			} catch (SecurityException e) {//1
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}	
		}
		return value;
	}
	
	/**
	 * 调用私有方法<br>
	 * @param methodName 方法名
	 * @return 是否调用成功
	 */
	public boolean callMethod(String methodName){
		return callMethod(methodName, null);
	}
	
	public static class MethodInfo{
		/** 参数类型数组*/
		public Class<?>[] parameterTypes;
		/** 参数值数组*/
		public Object[] args;
	}
	/**
	 * 调用私用方法<br>
	 * @param methodName 方法名
	 * @param methodInfo 参数类型及参数值结构体
	 * @return 是否调用成功
	 */
	public boolean callMethod(String methodName, MethodInfo methodInfo){
		boolean isOK = false;
		if(mClass != null && mObj != null){
			//调用私用方法
			try {
				Method driveMtd = mClass.getDeclaredMethod(methodName, (Class[])((methodInfo == null) ? null : methodInfo.parameterTypes));
				if(driveMtd != null){
					driveMtd.setAccessible(true);
					driveMtd.invoke(mObj, (Object[])((methodInfo == null) ? null : methodInfo.args));
					isOK = true;
				}
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		}
		return isOK;
	}
}
