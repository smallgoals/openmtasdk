package com.nil.sdk.utils;

import com.blankj.utilcode.util.LogUtils;
import com.nil.sdk.ui.BaseUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * 实体类操作工具类<br>
 */
public class ComUtil {
	public static final String TAG = "ComUtil";

	/**
	 * 得到指定长度的byte数组，不足时补空<br>
	 * 
	 * @param value
	 * @param len
	 * @return
	 */
	public static ByteBuffer getBufByLen(String value, int len) {
		return getBufByLen(value.getBytes(), len);
	}

	/**
	 * 得到指定长度的byte数组，不足时补空<br>
	 * 
	 * @param value
	 * @param len
	 * @return
	 */
	public static ByteBuffer getBufByLen(byte[] value, int len) {
		ByteBuffer buf = ByteBuffer.allocate(len);
		buf.order(ByteOrder.LITTLE_ENDIAN);
		buf.put(value);
		buf.rewind();
		return buf;
	}

	/**
	 * 连接字符<br>
	 * 
	 * @param split
	 *            间隔符
	 * @param intAry
	 *            字符
	 * @return
	 */
	public static String concatString(String split, Object... intAry) {
		if (intAry == null) {
			return null;
		}

		int len = intAry.length;
		StringBuffer strBuf = new StringBuffer();
		for (int i = 0; i < len - 1; i++) {
			strBuf.append(intAry[i]);
			strBuf.append(split);
		}
		strBuf.append(intAry[len - 1]);
		return strBuf.toString();
	}

	/**
	 * 读取指定长度数据<br>
	 * 
	 * @param socket
	 *            数据连接
	 * @param length
	 *            数据长度
	 * @param maxErrNums
	 *            最大错误次数
	 * @return
	 * @throws IOException
	 */
	public static ByteBuffer readDataByLength(Socket socket, int length,
			int maxErrNums) throws IOException {
		ByteBuffer result = ByteBuffer.allocate(length);
		InputStream is = socket.getInputStream();
		int totalLen = 0;
		int lenRec = 0;
		int errCount = 0;
		while (totalLen < length && errCount < maxErrNums) {
			// 为防止读取数据返?-1而丢失数据，采用读取指定消息长度的数?
			lenRec = is.read(result.array(), result.position(),
					result.remaining());
			if (lenRec == -1) {
				if(Spu.isSucK(TAG)) LogUtils.iTag(TAG, "lenRec == -1..." + lenRec);
				errCount++;
				continue;
			}
			totalLen += lenRec;
			// 设置已读取数据的长度对应缓冲区中的长?
			result.position(totalLen);
		}
		return result;
	}
	
    private static long lastClickTime;
    public static boolean isFastDoubleClick(long msec) {
        long time = System.currentTimeMillis();
        long timeD = time - lastClickTime;
        lastClickTime = time;
        if ( 0 < timeD && timeD < msec) {
			return true; 
		} 
		return false; 
	}
    private static long preTime=-1;
    public static boolean isSafetyInterval(long msec) {
        long time = System.currentTimeMillis();
        if(preTime < 0) preTime = time;
        long timeD = time - preTime;
        BaseUtils.addMap("timeD_Nil", timeD);
        if(timeD >= msec){
        	preTime = -1;
        	return true;
        }
		return false; 
	}
}
