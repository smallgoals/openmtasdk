package com.nil.sdk.utils;

import android.os.Environment;
import android.os.StatFs;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.Utils;
import com.nil.sdk.ui.aid.MyTipDialog;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 此类的代码完全抽取自apache源项目commons中的commons-io包，并作适当修改，使其更适合在手机上执行<br>
 * @author Geek_Soledad (66704238@51uc.com)
 */
public class FileUtils {
	private final static String TAG = FileUtils.class.getSimpleName();
	/**
	 * The number of bytes in a kilobyte.
	 */
	public static final long ONE_KB = 1024;
	/**
	 * The number of bytes in a megabyte.
	 */
	public static final long ONE_MB = ONE_KB * ONE_KB;
	/**
	 * The file copy buffer size (10 MB) （原来30MB，为更适合在手机上使用，将其改为10MB，by
	 * Geek_Soledad)
	 */
	private static final long FILE_COPY_BUFFER_SIZE = ONE_MB * 10;
	/**
	 * <p>
	 * 将一个目录下的文件全部拷贝到另一个目录里面，并且保留文件日期<br>
	 * </p>
	 * <p>
	 * 如果目标目录不存在，则被创建 如果目标目录已经存在，则将会合并两个文件夹的内容，若有冲突则替换掉目标目录中的文件<br>
	 * </p>
	 * 
	 * @param srcDir
	 *      源目录，不能为null且必须存在<br>
	 * @param destDir
	 *      目标目录，不能为null<br>
	 * @throws NullPointerException
	 *       如果源目录或目标目录为null<br>
	 * @throws IOException
	 *       如果源目录或目标目录无效<br>
	 * @throws IOException
	 *       如果拷贝中出现IO错误<br>
	 */
	public static void copyDirectoryToDirectory(File srcDir, File destDir)
			throws IOException {
		if (srcDir == null) {
			throw new NullPointerException("Source must not be null");
		}
		if (srcDir.exists() && srcDir.isDirectory() == false) {
			throw new IllegalArgumentException("Source '" + destDir
					+ "' is not a directory");
		}
		if (destDir == null) {
			throw new NullPointerException("Destination must not be null");
		}
		if (destDir.exists() && destDir.isDirectory() == false) {
			throw new IllegalArgumentException("Destination '" + destDir
					+ "' is not a directory");
		}
		copyDirectory(srcDir, new File(destDir, srcDir.getName()), true);
	}
	/**
	 * <p>
	 * 将目录及其以下子目录拷贝到一个新的位置，并且保留文件日期<br>
	 * <p>
	 * 如果目标目录不存在，则被创建;如果目标目录已经存在，则将会合并两个文件夹的内容，若有冲突则替换掉目标目录中的文件<br>
	 * <p>
	 * 
	 * @param srcDir
	 *      一个存在的源目录，不能为null<br>
	 * @param destDir
	 *      新的目录，不能为null<br>
	 * 
	 * @throws NullPointerException
	 *       如果源目录或目标目录为null<br>
	 * @throws IOException
	 *       如果源目录或目标目录无效<br>
	 * @throws IOException
	 *       如果拷贝中出现IO错误<br>
	 */
	public static void copyDirectory(File srcDir, File destDir)
			throws IOException {
		copyDirectory(srcDir, destDir, true);
	}
	/**
	 * 拷贝目录到一个新的位置<br>
	 * <p>
	 * 该方法将拷贝指定的源目录的所有内容到一个新的目录中<br>
	 * </p>
	 * <p>
	 * 如果目标目录不存在，则被创建;如果目标目录已经存在，则将会合并两个文件夹的内容，若有冲突则替换掉目标目录中的文件<br>
	 * </p>
	 * 
	 * @param srcDir
	 *      一个存在的源目录，不能为null<br>
	 * @param destDir
	 *      新的目录，不能为null<br>
	 * 
	 * @throws NullPointerException
	 *       如果源目录或目标目录为null<br>
	 * @throws IOException
	 *       如果源目录或目标目录无效<br>
	 * @throws IOException
	 *       如果拷贝中出现IO错误<br>
	 */
	public static void copyDirectory(File srcDir, File destDir,
			boolean preserveFileDate) throws IOException {
		if (srcDir == null) {
			throw new NullPointerException("Source must not be null");
		}
		if (srcDir.exists() && srcDir.isDirectory() == false) {
			throw new IllegalArgumentException("Source '" + destDir
					+ "' is not a directory");
		}
		if (destDir == null) {
			throw new NullPointerException("Destination must not be null");
		}
		if (destDir.exists() && destDir.isDirectory() == false) {
			throw new IllegalArgumentException("Destination '" + destDir
					+ "' is not a directory");
		}
		if (srcDir.getCanonicalPath().equals(destDir.getCanonicalPath())) {
			throw new IOException("Source '" + srcDir + "' and destination '"
					+ destDir + "' are the same");
		}
		// 为满足当目标目录在源目录里面的情况??
		List<String> exclusionList = null;
		if (destDir.getCanonicalPath().startsWith(srcDir.getCanonicalPath())) {
			File[] srcFiles = srcDir.listFiles();
			if (srcFiles != null && srcFiles.length > 0) {
				exclusionList = new ArrayList<String>(srcFiles.length);
				for (File srcFile : srcFiles) {
					File copiedFile = new File(destDir, srcFile.getName());
					exclusionList.add(copiedFile.getCanonicalPath());
				}
			}
		}
		doCopyDirectory(srcDir, destDir, preserveFileDate, exclusionList);
	}
	/**
	 * Internal copy directory method.
	 * 
	 * @param srcDir
	 *      the validated source directory, must not be <code>null</code>
	 * @param destDir
	 *      the validated destination directory, must not be
	 *      <code>null</code>
	 * @param preserveFileDate
	 *      whether to preserve the file date
	 * @param exclusionList
	 *      List of files and directories to exclude from the copy, may be
	 *      null
	 * @throws IOException
	 *       if an error occurs
	 * @since Commons IO 1.1
	 */
	private static void doCopyDirectory(File srcDir, File destDir,
			boolean preserveFileDate, List<String> exclusionList)
					throws IOException {
		// recurse
		File[] srcFiles = srcDir.listFiles();
		if (srcFiles == null) { // null if abstract pathname does not denote a
			// directory, or if an I/O error occurs
			throw new IOException("Failed to list contents of " + srcDir);
		}
		if (destDir.exists()) {
			if (destDir.isDirectory() == false) {
				throw new IOException("Destination '" + destDir
						+ "' exists but is not a directory");
			}
		} else {
			if (!destDir.mkdirs() && !destDir.isDirectory()) {
				throw new IOException("Destination '" + destDir
						+ "' directory cannot be created");
			}
		}
		if (destDir.canWrite() == false) {
			throw new IOException("Destination '" + destDir
					+ "' cannot be written to");
		}
		for (File srcFile : srcFiles) {
			File dstFile = new File(destDir, srcFile.getName());
			if (exclusionList == null
					|| !exclusionList.contains(srcFile.getCanonicalPath())) {
				if (srcFile.isDirectory()) {
					doCopyDirectory(srcFile, dstFile, preserveFileDate,
							exclusionList);
				} else {
					doCopyFile(srcFile, dstFile, preserveFileDate);
				}
			}
		}
		// Do this last, as the above has probably affected directory metadata
		if (preserveFileDate) {
			destDir.setLastModified(srcDir.lastModified());
		}
	}
	/**
	 * Internal copy file method.
	 * 
	 * @param srcFile
	 *      the validated source file, must not be <code>null</code>
	 * @param destFile
	 *      the validated destination file, must not be <code>null</code>
	 * @param preserveFileDate
	 *      whether to preserve the file date
	 * @throws IOException
	 *       if an error occurs
	 */
	private static void doCopyFile(File srcFile, File destFile,
			boolean preserveFileDate) throws IOException {
		if (destFile.exists() && destFile.isDirectory()) {
			throw new IOException("Destination '" + destFile
					+ "' exists but is a directory");
		}
		FileInputStream fis = null;
		FileOutputStream fos = null;
		FileChannel input = null;
		FileChannel output = null;
		try {
			fis = new FileInputStream(srcFile);
			fos = new FileOutputStream(destFile);
			input = fis.getChannel();
			output = fos.getChannel();
			long size = input.size();
			long pos = 0;
			long count = 0;
			while (pos < size) {
				count = (size - pos) > FILE_COPY_BUFFER_SIZE ? FILE_COPY_BUFFER_SIZE
						: (size - pos);
				pos += output.transferFrom(input, pos, count);
			}
		} finally {
			closeQuietly(output);
			closeQuietly(fos);
			closeQuietly(input);
			closeQuietly(fis);
		}
		if (srcFile.length() != destFile.length()) {
			throw new IOException("Failed to copy full contents from '"
					+ srcFile + "' to '" + destFile + "'");
		}
		if (preserveFileDate) {
			destFile.setLastModified(srcFile.lastModified());
		}
	}

	public static void closeQuietly(Closeable closeable) {
		try {
			if (closeable != null) {
				closeable.close();
			}
		} catch (IOException ioe) {
			// ignore
		}
	}
	
	/** 获取文件后缀名 */
	public static String getFileExtension(File f) {
		return getFileExtension(f, null);
	}
	/** 获取文件后缀名 */
	public static String getFileExtension(File f, String defSuffix) {
		if (f != null) {
			String filename = f.getName();
			int i = filename.lastIndexOf('.');
			if (i > 0 && i < filename.length() - 1) {
				return filename.substring(i + 1).toLowerCase();
			}
		}
		return defSuffix;
	}
	/** 获取文件后缀名 */
	public static String getFileExtension(String path) {
		return getFileExtension(new File(path));
	}

	/** 获取文件后缀名 */
	public static String getNilFileSuffix(String path) {
		return getNilFileSuffix(new File(path));
	}
	public static String getNilFileSuffix(File file) {
		String defSuffix = ".apk";
		if (file != null) {
			String filename = file.getName();
			int i = filename.lastIndexOf('.');
			if (i > 0 && i < filename.length() - 1) {
				String suffix = filename.substring(i).toLowerCase();
				if(suffix.length() > 0 && suffix.length() <= 5) {
					return suffix;
				}else{
					return defSuffix;
				}
			}
		}
		return defSuffix;
	}

	public static String getUrlFileName(String url) {
		int slashIndex = url.lastIndexOf('/');
		int dotIndex = url.lastIndexOf('.');
		String filenameWithoutExtension;
		if (dotIndex == -1) {
			filenameWithoutExtension = url.substring(slashIndex + 1);
		} else {
			filenameWithoutExtension = url.substring(slashIndex + 1, dotIndex);
		}
		return filenameWithoutExtension;
	}

	public static String getUrlExtension(String url) {
		if (url != null && !"".equals(url.trim())) {
			int i = url.lastIndexOf('.');
			if (i > 0 && i < url.length() - 1) {
				return url.substring(i + 1).toLowerCase();
			}
		}
		return "";
	}

	public static String getFileNameNoEx(String filename) {
		if ((filename != null) && (filename.length() > 0)) {
			int dot = filename.lastIndexOf('.');
			if ((dot > -1) && (dot < (filename.length()))) {
				return filename.substring(0, dot);
			}
		}
		return filename;
	}
	
	private static final double KB = 1024.0;
	private static final double MB = KB * KB;
	private static final double GB = KB * KB * KB;
	public static String getFileSize(long size) {
		String fileSize;
		if (size < KB)
			fileSize = String.format("%.02f", size / 1.0) + "B";
		else if (size < MB)
			fileSize = String.format("%.02f", size / KB) + "KB";
		else if (size < GB)
			fileSize = String.format("%.02f", size / MB) + "MB";
		else
			fileSize = String.format("%.02f", size / GB) + "GB";
		return fileSize/*fileSize.replace(".0", "")*/;
	}
	
	private static final double WC = 10000;
	private static final double YC = WC * WC;
	public static String showViewTime(long size, String unit) {
		String fileSize = "∞";
		if (size < WC)
			fileSize = size+"";
		else if (size < YC)
			fileSize = String.format("%.1f", size / WC) + "万";
		else if (size < YC)
			fileSize = String.format("%.1f", size / YC) + "亿";
		return (fileSize+unit).replace(".0", "");
	}

	/** 显示SD卡剩余空间 */
	@SuppressWarnings("deprecation")
	public static String showFileAvailable() {
		String result = "";
		if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
			StatFs sf = new StatFs(Environment.getExternalStorageDirectory().getPath());
			long blockSize = sf.getBlockSize();
			long blockCount = sf.getBlockCount();
			long availCount = sf.getAvailableBlocks();
			return "可用："+getFileSize(availCount * blockSize) + "/总共：" + getFileSize(blockSize * blockCount);
		}
		return result;
	}

	/** 如果不存在就创建 */
	public static boolean createIfNoExists(String path) {
		File file = new File(path);
		boolean mk = false;
		if (!file.exists()) {
			mk = file.mkdirs();
		}
		return mk;
	}

	private static HashMap<String, String> mMimeType = new HashMap<String, String>();
	/** 获取MIME */
	public static String getMimeType(String path) {
		int lastDot = path.lastIndexOf(".");
		if (lastDot < 0)
			return null;

		return mMimeType.get(path.substring(lastDot + 1).toUpperCase());
	}

	/** 多个SD卡时 取外置SD卡 */
	public static String getExternalStorageDirectory() {
		//参考文章
		//http://blog.csdn.net/bbmiku/article/details/7937745
		Map<String, String> map = System.getenv();
		String[] values = new String[map.values().size()];
		map.values().toArray(values);
		String path = values[values.length - 1];
		LogUtils.eTag(TAG, "FileUtils.getExternalStorageDirectory : " + path);
		if (path.startsWith("/mnt/") && !Environment.getExternalStorageDirectory().getAbsolutePath().equals(path))
			return path;
		else
			return null;
	}
	
	public static String getFileAttribute(String path){
		StringBuffer buf = new StringBuffer();
		if(path != null && !"".equals(path.trim())){
			File file = new File(path);
			buf.append("文件名称：").append(file.getName()).append("\r\n");
			buf.append("文件大小：").append((file.isDirectory())? file.list().length+"个文件":getFileSize(file.length())).append("\r\n");
			buf.append("修改时间：").append(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(file.lastModified()))).append("\r\n");
			buf.append("路径MD5：").append(MyMD5Util.getMD5ByString(file.getAbsolutePath())).append("\r\n");
			buf.append("完整路径：").append(file.getAbsolutePath()).append("\r\n");
			MyTipDialog.popDialog(Utils.getApp(), "文件属性信息",buf.toString(), "关闭查看");
		}else{
			buf.append("文件路径不合法");
		}
		return buf.toString();
	}
	
	/**
	 * 删除指定文件夹下所有文件
	 * 
	 * @param path 文件夹完整绝对路径
	 * @return
	 */
	public static boolean delAllFile(String path) {
		boolean flag = false;
		File file = new File(path);
		if (!file.exists() || !file.isDirectory()) {
			return flag;
		}
		String[] tempList = file.list();
		File temp = null;
		for (int i = 0; i < tempList.length; i++) {
			if (path.endsWith(File.separator)) {
				temp = new File(path + tempList[i]);
			} else {
				temp = new File(path + File.separator + tempList[i]);
			}
			if (temp.isFile()) {
				temp.delete();
			}
			if (temp.isDirectory()) {
				delAllFile(path + "/" + tempList[i]);		// 先删除文件夹里面的文件
				delFolder(path + "/" + tempList[i]);		// 再删除空文件夹
				flag = true;
			}
		}
		return flag;
	}
	
	/**
	 * 删除文件夹
	 * 
	 * @param folderPath  文件夹完整绝对路径
	 */
	public static boolean delFolder(String folderPath) {
		boolean isOK = false;
		try {
			delAllFile(folderPath); 		// 删除完里面所有内容
			isOK = deleteFileSafely(new File(folderPath)); 		// 删除空文件夹
		} catch (Exception e) {
			e.printStackTrace();
			isOK = false;
		}
		return isOK;
	}
	
    /**
     * 安全删除文件.
     * @param file
     * @return
     */
    public static boolean deleteFileSafely(File file) {
        if (file != null && file.exists()) {
        	if(file.isDirectory()) delAllFile(file.getAbsolutePath()); 
            String tmpPath = file.getParent() + File.separator + System.currentTimeMillis();
            File tmp = new File(tmpPath);
            file.renameTo(tmp);
            return tmp.delete();
        }
        return false;
    }
}