package com.nil.sdk.utils;

import android.content.Context;

import com.blankj.utilcode.util.Utils;
import com.nil.sdk.ui.BaseUtils;
import com.nil.vvv.utils.AdSwitchUtils;

/**
 * 把APP分享给好友
 * 显示隐藏：itemVip.setVisibility(ShareAPP2FriendUtil.isSetUpOnlineConfig(getActivity()) ? View.VISIBLE : View.GONE);
 * 点击事件：ShareAPP2FriendUtil.onClickShare(getActivity());
 */
public class ShareAPP2FriendUtil {
    public static final String SHARE_CONFIG_KEY = "share_config";

    /**
     * @return 是否在后台配置了分享参数
     */
    public static boolean isSetUpOnlineConfig(Context ctx) {
        if(ctx == null) ctx = Utils.getApp();
        String share_config = AdSwitchUtils.Vs.share_config.value;
        if (share_config == null || share_config.length() == 0) {
            return false;
        }
        return true;
    }

    /**
     * 分享
     */
    public static void onClickShare(Context ctx) {
        if(ctx == null) ctx = Utils.getApp();
        String share_config = AdSwitchUtils.Vs.share_config.value;
        BaseUtils.openShare(ctx, share_config);
        XMBEventUtils.send(XMBEventUtils.EventType.share_apk, share_config);
    }

}
