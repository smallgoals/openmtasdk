package com.nil.sdk.ui.aid;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class BNAdapter extends FragmentPagerAdapter {
    private List<BNItem> bnItems = new ArrayList<>();
    public BNAdapter(FragmentManager fragmentManager, List<BNItem> bnItems) {
        super(fragmentManager);
        if(bnItems != null) {
            this.bnItems = bnItems;
        }
    }

    @Override
    public int getCount() {
        return bnItems.size();
    }
    public CharSequence getPageTitle(int position) {
        return bnItems.get(position).title;
    }
    public Fragment getItem(int position) {
        return bnItems.get(position).fragment;
    }
}
