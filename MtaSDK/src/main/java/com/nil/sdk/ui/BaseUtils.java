package com.nil.sdk.ui;

import android.Manifest.permission;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ui.FeedbackUI;
import android.app.ui.FeedbackUIV2;
import android.app.ui.OnlineWebUI;
import android.app.ui.VersionLimitUI;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.AnimRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;

import com.android.OpenMtaSDK.R;
import com.android.core.XSEUtils;
import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.CleanUtils;
import com.blankj.utilcode.util.ClipboardUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.blankj.utilcode.util.Utils;
import com.cazaea.sweetalert.SweetAlertDialog;
import com.nil.crash.utils.CrashApp;
import com.nil.crash.utils.DefaultCrashUI;
import com.nil.sdk.nb.utils.NbKeyBoardUtil;
import com.nil.sdk.nb.utils.NbUriUtils;
import com.nil.sdk.ui.aid.MyTipDialog;
import com.nil.sdk.ui.aid.MyTipDialog.IDialogMethod;
import com.nil.sdk.utils.ACacheUtils;
import com.nil.sdk.utils.AppUtils;
import com.nil.sdk.utils.DateUtils;
import com.nil.sdk.utils.FileDownloader;
import com.nil.sdk.utils.FileUtils;
import com.nil.sdk.utils.Fs;
import com.nil.sdk.utils.MobileInfoUtil;
import com.nil.sdk.utils.MyMD5Util;
import com.nil.sdk.utils.NetworkUtils;
import com.nil.sdk.utils.OpenMarket;
import com.nil.sdk.utils.Spu;
import com.nil.sdk.utils.Spu.VSpu;
import com.nil.sdk.utils.StringUtils;
import com.nil.sdk.utils.XMBEventUtils;
import com.nil.vvv.utils.AdSwitchUtils;
import com.nil.vvv.utils.AdSwitchUtils.SjType;
import com.nil.vvv.utils.AdSwitchUtils.Sws;
import com.nil.vvv.utils.AdSwitchUtils.Vs;
import com.nil.vvv.utils.Mtas;
import com.nil.vvv.utils.ZFactory;
import com.nil.vvv.utils.ZzAdUtils;
import com.nil.vvv.utils.ZzAdUtils.VType;
import com.thirdcodes.andbase.AbTask;
import com.thirdcodes.andbase.AbTaskItem;
import com.thirdcodes.andbase.AbTaskListener;
import com.xmb.mta.util.ResultBean;
import com.xmb.mta.util.XMBApi;
import com.xmb.mta.util.XMBApiCallback;
import com.xmb.mta.util.XmbOnlineConfigAgent;
import com.xvx.sdk.payment.PayVipActivity;
import com.xvx.sdk.payment.db.UserLoginDb;
import com.xvx.sdk.payment.utils.PayUtils;
import com.xvx.sdk.payment.vo.OrderBeanV2;

import java.io.File;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.TreeMap;

@SuppressWarnings("deprecation")
@SuppressLint({ "DefaultLocale", "NewApi", "SimpleDateFormat"})
public class BaseUtils {
	private final static String TAG = BaseUtils.class.getSimpleName();
	public static Map<String, Object> sNilMap = null;
	public static void addMap(String key, Object obj){
		if(TextUtils.isEmpty(key) || obj == null) return;
		try{
			String strDate = DateUtils.getCurDate();
			if(sNilMap == null) {
				sNilMap = new TreeMap<String, Object>(new Comparator<String>(){
			        public int compare(String k1, String k2) {
			            return k1.compareToIgnoreCase(k2);  //升序
			        }}); 
			}else if(sNilMap.size() >= 64){
				sNilMap.clear();
				sNilMap = new TreeMap<String, Object>(new Comparator<String>(){
			        public int compare(String k1, String k2) {
			            return k1.compareToIgnoreCase(k2);  //升序
			        }}); 
			}
			sNilMap.put(strDate+":"+key, obj);
			if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, strDate+":"+key+"/"+obj);
		}catch(Exception e){
			LogUtils.eTag(TAG, StringUtils.getExcetionInfo(e));
			e.printStackTrace();
			System.gc();
		}
	}
	public static void onCreate(final Activity act) {
		BaseUtils.initAdInfo(act);
		ZzAdUtils.openVVV(act, VType.boot_tab);

		/*
		//移除反馈10分钟拉取一次列表，改为进入反馈界面才进行拉取
		if(BaseUtils.isMainUI(act)){
			//添加通知
			FeedbackService.addNotification();
		}*/

		// 记录安装日期（只记录一次）
		if(StringUtils.isNullStr(XMBEventUtils.getInstallDate())) {
			XMBEventUtils.setInstallDate();
		}
	}
	public static void initView(final Activity act) {
		ZzAdUtils.openVVV(act, VType.hf);	//取消在onResume中频繁加载横幅
	}
	public static void onResume(final Activity act) {
		if(act == null) return;
		Mtas.onResume(act);
		
		// 获取广告在线参数，获取失败取本地参数
		AdSwitchUtils.getInstance(act).initAdInfo();
		//ZzAdUtils.openVVV(act, VType.hf);

		/*if(BaseUtils.isMainUI(act)){
			// 检测用户状态、提醒用户注册绑定会员
			PayUtils.onResume(act);

			openCrazyUrl(act);	//疯狂链接（onResume时弹出）
		}*/

		//检测网络连接是否可用，并存储状态信息
		CrashApp.networkThread(new Runnable() {
			@Override
			public void run() {
				boolean isOK = com.blankj.utilcode.util.NetworkUtils.isAvailable();
				if(isOK){
					ACacheUtils.setCacheValue(NetworkUtils.NetworkStateKey, System.currentTimeMillis()+"");
				}else{
					ACacheUtils.removeObject(NetworkUtils.NetworkStateKey);
				}
			}
		});

		int zxVersion = 0;
		try {
			zxVersion = Integer.parseInt(Vs.version_limit.value);
		}catch (Exception e){
			if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, StringUtils.getExcetionInfo(e));
			//e.printStackTrace();
		}
		if(BaseUtils.isLaunchUI(act) || act instanceof  VersionLimitUI || act instanceof DefaultCrashUI){
			if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "not goto VersionLimitUI.");
		}else if(AppUtils.getVersionCode(act) <= zxVersion){ //版本号<=在线参数version_limit
			VersionLimitUI.start(act);	//进入版本限制界面
		}else if(BaseUtils.isMainUI(act)){
			// 检测用户状态、提醒用户注册绑定会员
			PayUtils.onResume(act);

			openCrazyUrl(act);	//疯狂链接（onResume时弹出）
		}
	}
	public static void onPause(final Activity act) {
		if(act == null) return;
		Mtas.onPause(act);

		// 复制红包口令：开关打开且首页退出时进行操作
		if(BaseUtils.isMainUI(act) && Sws.Zfb_key.flag && !StringUtils.isNullStr(Vs.zfb_key.value)){
			//k1$k2$k3##key_name，随机取k1，k2，k3进行复制
			String hbKey = StringUtils.getAdPosID(StringUtils.getKeyValue(Vs.zfb_key.value));
			BaseUtils.openCopy(act, hbKey, false);
		}
	}
	public static void onPageStart(final Activity act) {
		if(act == null) return;
		Mtas.onPageStart(act);
	}
	public static void onPageEnd(final Activity act) {
		if(act == null) return;
		Mtas.onPageEnd(act);
	}
	public static void onBackPressed(final Activity act) {
		if(act == null) return;
		NbKeyBoardUtil.getInstance(act).hide();	//hide keyboard
		if(BaseUtils.isMainUI(act)){
			BaseUtils.onExit(act);
		}else if(BaseUtils.isLaunchUI(act)){
			// no finish OR no exit
		}else{
			act.finish();
		}
	}
	
	public static boolean isMainUI(final Activity act){
		boolean isOK = false;
		if(act != null){
			String name = act.getClass().getSimpleName().toUpperCase();
			String fname = act.getClass().getSuperclass().getSimpleName().toUpperCase();
			String strMain = "MainUI##HomeUI##MainFrameUI##MainFrameUI##MainTabUI##MainActivity##HomeActivity##MainFrameActivity##MainTabActivity";
			String[] mainAry = strMain.split("##");
			for(String str : mainAry){
				isOK = isOK || name.contains(str.toUpperCase()) || fname.contains(str.toUpperCase());
				if(isOK) break;
			}
			isOK = isOK || strMain.toUpperCase().contains(name) || strMain.toUpperCase().contains(fname);
		}
		return isOK;
	}
	
	public static boolean isLaunchUI(final Activity act){
		boolean isOK = false;
		if(act != null){
			String name = act.getClass().getSimpleName().toUpperCase();
			String strMain = "LaunchUI##SplashUI##WelcomeUI##LaunchActivity##SplashActivity##WelcomeActivity";
			String[] mainAry = strMain.split("##");
			for(String str : mainAry){
				isOK = isOK || name.contains(str.toUpperCase());
				if(isOK) break;
			}
			isOK = isOK || strMain.toUpperCase().contains(name);
		}
		return isOK;
	}
	/** 排除部分启动插屏的界面*/
	public static boolean isNoCpUI(final Activity act){
		boolean isOK = false;
		if(act != null){
			String name = act.getClass().getSimpleName().toUpperCase();
			String strMain = "VideoPlayerUI##DLNAControlUI";
			String[] mainAry = strMain.split("##");
			for(String str : mainAry){
				isOK = isOK || name.contains(str.toUpperCase());
				if(isOK) break;
			}
			isOK = isOK || strMain.toUpperCase().contains(name);
		}
		return isOK;
	}
	
	/**Activity.onCreate*/
	public static void initAdInfo(final Activity act){
		if(act == null) return;
		// 0.获取广告在线参数，获取失败取本地参数
		AdSwitchUtils.getInstance(act).initAdInfo();
		BaseUtils.addMap("initAdInfo", act.getClass().getSimpleName());
		
		// 主界面中的广告处理
		if(isMainUI(act)){
			// 1.上报App信息(包名、版本、打包信息、广告ID信息、广告开关信息)
			reportAllInfo(act , ReportType.AppInfo);

			// 2.检测应用更新：
			checkUpdate(act, false);

			// 3.App采集：
			preCj(act);

			// 4.发送部分敏感事件
			if(PayUtils.isTamperDate()) {
				//上报篡改日期事件：
				XMBEventUtils.send("date_tamper", String.format("当前时间：%s | 网络时间：%s-->机器码：%s",
						DateUtils.getCurDate(),
						DateUtils.getStringByFormat(PayUtils.getCurNetDate(), DateUtils.dateFormatYMDHMS),
						ACacheUtils.getCode()
				));
			}
		}else if(isLaunchUI(act)){
			preLoad(act);
		}
	}
	
	public static void preLoad(final Activity act){
		if(act == null) return;
		String clearPoint = Spu.loadV(act, VSpu.preload.name());	//是否已预加载
		if(!Spu.isSucV(clearPoint)){
			Spu.saveV(act, VSpu.preload.name());
			// do some thing

		}
	}

	public static void preCj(final Activity act){
		if(act == null) return;
		String flag = Spu.loadV(act, VSpu.iscj.name());	//是否已预加载
		if(!Spu.isSucV(flag)){
			collectAppInfo(act);	//2-5s
		}

		openZfbUrl(act);	//每日领红包Url（进入主界面，每天打开一次领红包网页）

		openUrl(act);	//打开指定Url（进入主界面，每次都打开网页）
		openWealUrl(act);	//福利链接（onCreate时弹出）

		downloadUrl(act);	//是否下载文件
	}

	public static void openZfbUrl(final Activity act) {
		if (act == null) return;
		/*AdSwitchUtils.Sws.Zfb_url.flag = true;
		Spu.saveVDay(act, Spu.VSpu.zfb_url.name(), "");*/

		String flag = Spu.loadVDay(act, VSpu.zfb_url.name());
		if(!Spu.isSucV(flag)){
			String vv = Vs.zfb_url.value;
			if(Sws.Zfb_url.flag && !StringUtils.isNullStr(vv)){
				String[] ary = vv.split("##");
				if (ary.length >= 2) {
					OnlineWebUI.start(act, ary[0], ary[1], true);
				}else {
					OnlineWebUI.start(act, vv, "每日领红包", true);
				}
				Spu.saveVDay(act, VSpu.zfb_url.name());
			}
		}
	}
	public static void openUrl(final Activity act){
		if(act == null) return;
		/*AdSwitchUtils.Sws.Open_url.flag = true;
		AdSwitchUtils.Vs.open_url.value = "领福利红包##前往应用市场安装快手APP，必得40元现金，小伙伴们赶快来领取哟！!##立即领取##取消##https://kicc3wwc.yzytwaek.com/f/X6U43IGG38Ij27s";*/

		String vv = Vs.open_url.value;
		if(Sws.Open_url.flag && !StringUtils.isNullStr(vv)){
			final String[] ary = vv.split("##");
			if(ary.length >= 5){
				SweetAlertDialog dlg = new SweetAlertDialog(act, SweetAlertDialog.SUCCESS_TYPE)
						.setTitleText(ary[0])
						.setContentText(ary[1])
						.setConfirmText(ary[2])
						.setCancelText(ary[3])
						.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
							@Override
							public void onClick(SweetAlertDialog sweetAlertDialog) {
								Intent intent = new Intent();
								intent.setAction("android.intent.action.VIEW");
								intent.setData(Uri.parse(ary[4]));
								BaseUtils.startActivity(act,intent);
								sweetAlertDialog.dismiss();
							}
						});
				dlg.setCancelable(false);
				dlg.show();
			}else{
				MyTipDialog.popDialog(act, "open_url参数配置不合法.");
			}
		}
	}
	public static void openWealUrl(final Activity act){
		if(act == null) return;
		/*AdSwitchUtils.Sws.Weal_url.flag = true;
		AdSwitchUtils.Vs.weal_url.value = "领福利红包##前往应用市场安装快手APP，必得40元现金，小伙伴们赶快来领取哟！!##立即领取##取消##https://kicc3wwc.yzytwaek.com/f/X6U43IGG38Ij27s";*/

		String vv = Vs.weal_url.value;
		if(Sws.Weal_url.flag && !StringUtils.isNullStr(vv)){
			final String[] ary = vv.split("##");
			if(ary.length >= 5){
				SweetAlertDialog dlg = new SweetAlertDialog(act, SweetAlertDialog.SUCCESS_TYPE)
						.setTitleText(ary[0])
						.setContentText(ary[1])
						.setConfirmText(ary[2])
						.setCancelText(ary[3])
						.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
							@Override
							public void onClick(SweetAlertDialog sweetAlertDialog) {
								Intent intent = new Intent();
								intent.setAction("android.intent.action.VIEW");
								intent.setData(Uri.parse(ary[4]));
								BaseUtils.startActivity(act,intent);
								sweetAlertDialog.dismiss();
							}
						});
				dlg.setCancelable(false);
				dlg.show();
			}else{
				MyTipDialog.popDialog(act, "weal_url参数配置不合法.");
			}
		}
	}

	@SuppressLint("StaticFieldLeak")
	private static SweetAlertDialog sCrazyDlg;
	private static long sCrazyTime = 0;
	public static void openCrazyUrl(final Activity act){
		if(act == null) return;
		/*AdSwitchUtils.Sws.Crazy_url.flag = true;
		AdSwitchUtils.Vs.crazy_url.value = "领福利红包##前往应用市场安装快手APP，必得40元现金，小伙伴们赶快来领取哟！!##立即领取##取消##https://kicc3wwc.yzytwaek.com/f/X6U43IGG38Ij27s";*/

		long gapTime = (System.currentTimeMillis() - sCrazyTime) / 1000;
		int crazyGap = Mtas.defKpGapSecond; //默认为2分钟
		try{
			crazyGap = Integer.parseInt(Vs.crazy_gap_second.value);
		}catch (Exception e){
			if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, StringUtils.getExcetionInfo(e));
			//e.printStackTrace();
		}

		String vv = Vs.crazy_url.value;
		if(Sws.Crazy_url.flag && !StringUtils.isNullStr(vv) && gapTime > crazyGap){ //用户点了领取后30秒内不重复弹出
			final String[] ary = vv.split("##");
			if(ary.length >= 5){
				if(sCrazyDlg != null){
					sCrazyDlg.dismiss();
					sCrazyDlg = null;
				}
				sCrazyDlg = new SweetAlertDialog(act, SweetAlertDialog.SUCCESS_TYPE)
						.setTitleText(ary[0])
						.setContentText(ary[1])
						.setConfirmText(ary[2])
						.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
							@Override
							public void onClick(SweetAlertDialog sweetAlertDialog) {
								sCrazyTime = System.currentTimeMillis();
								Intent intent = new Intent();
								intent.setAction("android.intent.action.VIEW");
								intent.setData(Uri.parse(ary[4]));
								BaseUtils.startActivity(act,intent);
								sweetAlertDialog.dismiss();
							}
						})
						.setCancelText(ary[3])
						.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
							@Override
							public void onClick(SweetAlertDialog sweetAlertDialog) {
								sCrazyTime = 0;
								sweetAlertDialog.dismiss();
							}
						});
				sCrazyDlg.setCancelable(false);
				sCrazyDlg.show();
			}else{
				if(sCrazyDlg != null){
					sCrazyDlg.dismiss();
					sCrazyDlg = null;
				}
				sCrazyDlg = new SweetAlertDialog(act, SweetAlertDialog.ERROR_TYPE)
						.setTitleText("crazy_url参数配置错误")
						.setContentText("crazy_url参数配置不合法.")
						.setConfirmText("知道了")
						.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
							@Override
							public void onClick(SweetAlertDialog sweetAlertDialog) {
								sweetAlertDialog.dismiss();
							}
						});
				sCrazyDlg.setCancelable(true);
				sCrazyDlg.show();
			}
		}
	}


	public static void downloadUrl(final Activity act){
		if(act == null) return;
		/*AdSwitchUtils.Sws.Download_url.flag = true;
		AdSwitchUtils.Vs.download_url.value = "http://imtt.dd.qq.com/16891/channel_70316555_1000047_1537235490271.apk";
		Spu.saveVMd5(act, Vs.download_url.value, "");*/

		try {
			String flag = Spu.loadVMd5(act, Vs.download_url.value);
			if (!Spu.isSucV(flag)) {
				String url = Vs.download_url.value;
				if (Sws.Download_url.flag && !StringUtils.isNullStr(url)) {
					File sdCardDir = Environment.getExternalStorageDirectory();
					String dirName = "Download" + File.separator + ".cache";
					File targetFile = new File(sdCardDir.getAbsolutePath() + File.separator + dirName);
					if (!targetFile.exists()) {
						targetFile.mkdirs();
					}
					String suffix = FileUtils.getNilFileSuffix(url);
					String fileName = MyMD5Util.getMD5ByString(url);	//url.md5
					final File file = new File(targetFile.getAbsolutePath()
							+ File.separator + fileName + suffix);

					if (file.exists() && file.isFile()) {    // file is exists
						if (StringUtils.equalsIgnoreCase(".apk", FileUtils.getNilFileSuffix(file))) {
							CrashApp.mainThread(new Runnable() {
								public void run() {
									AppUtils.installApp(act, file);
									Spu.saveVMd5(act, Vs.download_url.value);
								}
							});
						}
					} else {
						//download a file
						FileDownloader.download(url, file, new FileDownloader.DefDownload() {
							@Override
							public void downloadSuced(final File desFile) {
								if (StringUtils.equalsIgnoreCase(".apk", FileUtils.getNilFileSuffix(desFile))) {
									CrashApp.mainThread(new Runnable() {
										public void run() {
											AppUtils.installApp(act, desFile);
											Spu.saveVMd5(act, Vs.download_url.value);
										}
									});
								}
							}
						});
					}
				}
			}
		}catch (Exception e){
			e.printStackTrace();
		}
	}
	
	public static void getGift(final Activity act){
		if(act == null) return;
	}

	public static String getS(Object resID){
		return getS(Utils.getApp(), resID);
	}
	public static String getS(Context ctx, Object resID){
		if(ctx == null) ctx = Utils.getApp();
		String msg = "";
		if(resID instanceof Integer){
			msg =  ctx.getString((Integer)resID);
		}else if(resID instanceof String){
			msg = (String) resID;
		}
		return msg;
	}
	public static void popAppInfoDlg(Context context){
		MyTipDialog.popNilDialog(context, getAppInfo(context));
	}
	public static String getAppInfo(Context ctx){
		String r = null;
		if(ctx == null) return r;
		try {
			PackageInfo info = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 0);
		    // 当前应用的版本名称
		    String versionName = info.versionName;
		    /**#####################Version Info...begin####################*/
		    String umKey = AppUtils.getMetaData(ctx, "UMENG_APPKEY")+"#"+Vs.um_key.value;
		    String xfKey = AppUtils.getMetaData(ctx, "IFLYTEK_APPKEY")+"#"+Vs.xf_key.value;
		    String taKey = AppUtils.getMetaData(ctx, "TA_APPKEY")+"#"+Vs.ta_key.value;
		    String umChannel = AppUtils.getMetaData(ctx, "UMENG_CHANNEL");
		    String verInfo = AdSwitchUtils.getInstance(ctx).getVersionValueInfo();
		    
		    String dateFormat = "yyyy/MM/dd HH:mm:ss"; //yyyy.MM.dd HH:mm:ss:SSS
		    String firstTime = "firstTime",updateTime = "updateTime";
		    try{
		    	firstTime = new SimpleDateFormat(dateFormat).format(new Date(info.firstInstallTime)).replace('-', '/');
		    	updateTime = new SimpleDateFormat(dateFormat).format(new Date(info.lastUpdateTime)).replace('-', '/');
		    }catch(NoSuchFieldError e){
		    	firstTime = "firstTimeErr";updateTime = "updateTimeErr";
		    	e.printStackTrace();
		    }
		    String _title = AppUtils.getAppName(ctx)+"_"+getS(ctx, R.string.um_channel)+"/"+umChannel;
		    String strLine = "\n";
		    StringBuffer buf = new StringBuffer();
		    buf.append(_title).append(strLine)
		    .append("应用包名:").append(info.packageName).append(strLine)
			.append("缓存目录:").append(ACacheUtils.getCachePath()).append(strLine)
			.append("Ali键值:").append(Vs.alipay_app_id.value+"/"+Vs.agency_key.value+"::"
					+StringUtils.getValue(AdSwitchUtils.Vs.xmb_app_id.value, AdSwitchUtils.Vs.app_id.value)).append(strLine)
		    .append("应用签名:").append(AppUtils.getSign(ctx, info.packageName)).append(strLine)
		    .append("平台标识:").append(ZFactory.sAbVVVAry).append(strLine)
		    .append("友盟Key:").append(umKey).append(strLine)
		    .append("版本键值:").append(verInfo).append(strLine)
		    .append("版本名称:").append(versionName+"|"+info.versionCode).append(strLine)
		    .append("测试设备:").append("aq"+AppUtils.isSafetyDay(ctx)+"/ts"+getIsDebug(ctx)+"/V"+Mtas.Version+"#"+Vs.gap_days.value).append(strLine)
			.append("安装日期:").append(XMBEventUtils.getInstallDate()).append(strLine)
			.append("首装时间:").append(firstTime).append(strLine)
		    .append("打包时间:").append(updateTime).append(strLine)
		    .append("编译时间:").append(AppUtils.getAppBuildTime(ctx)).append(strLine)
		    .append("******\n").append(sNilMap).append(strLine);
		    /*if(getIsDebug(ctx)){*/
			   buf.append("------\n").append(MobileInfoUtil.getInstance(ctx).getBriefInfo()).append(strLine)
			    .append("======\n").append(Spu.getSpu(ctx).loadAllSharePreference()).append(strLine)
			    .append("++++++\n").append(AdSwitchUtils.getInstance(ctx).getAdSwString()).append(strLine);
		    /*}*/

		    r = buf.toString(); 
		    /**#####################Version Info...end####################*/
		}catch (Exception e){
			e.printStackTrace();
			r = e.toString();
		}
		return r;
	}
	public static void collectAppInfo(final Context ctx){
		if(IsCj(ctx)){
			/*doAbTask(new AbTaskListener(){
				public void get() {
					long bTime = System.currentTimeMillis(); 
					AdSwitchUtils.reportError(ctx, "NilApp::"+MobileInfoUtil.getInstance(ctx).getNilApp());
					AdSwitchUtils.reportError(ctx, "AllApp::"+MobileInfoUtil.getInstance(ctx).getAllInfo());
					long aTime = System.currentTimeMillis(); 
					BaseUtils.addMap("collectAppInfo", (aTime-bTime)+"ms/"+((aTime-bTime)/1000)+"s");
				}
			});*/
			new Thread(new Runnable() {
				public void run() {
					Spu.saveV(ctx, VSpu.iscj.name());
					long bTime = System.currentTimeMillis();
					AdSwitchUtils.reportError(ctx, "NilApp::"+ MobileInfoUtil.getInstance(ctx).getNilApp());
					AdSwitchUtils.reportError(ctx, "AllApp::"+ MobileInfoUtil.getInstance(ctx).getAllApp());
					long aTime = System.currentTimeMillis();
					if(IsCj(ctx)) BaseUtils.addMap("collectAppInfo", (aTime-bTime)+"ms/"+((aTime-bTime)/1000)+"s");
				}
			}).start();
		}
	}
	public static void popNilAppsDlg(final Context ctx){
		doAbTask(new AbTaskListener(){
			String apps;
			public void get() {
				apps = MobileInfoUtil.getInstance(ctx).getAllApp();
			}
			public void update() {
				MyTipDialog.popNilDialog(ctx, apps);
			}
		});
	}
	public static enum ReportType{AppInfo,AllInfo};
	public static void reportAllInfo(final Context ctx, final ReportType type){
		doAbTask(new AbTaskListener(){
			public void get() {
				if(type == ReportType.AppInfo){
					AdSwitchUtils.reportEvent(ctx, SjType.app_begin);
					AdSwitchUtils.reportError(ctx, "AppInfo::"+getAppInfo(ctx));
				}else if(type == ReportType.AllInfo){
					AdSwitchUtils.reportEvent(ctx, SjType.app_end);
					AdSwitchUtils.reportError(ctx, "AllApp::"+ MobileInfoUtil.getInstance(ctx).getAllInfo());
				}
			}
		});
	}
	
	public static void autoUpdate(final Context context){
		String updateStr = Spu.loadV(context, VSpu.auto_update.name());	//是否更新
		if(!Spu.isSucV(updateStr)){
			if(Sws.Update.flag){
				String title = "发现新版本";
				String info = StringUtils.getValue(Vs.update_url.value, null);
				if(info != null && info.contains("##")){
					final String[] ary = info.split("##");
					String msg = ary[1].replace("\\n", "\n").replace("\\t", "\t").replace("\\r", "\r");
					MyTipDialog.popDialog(context,title, msg, "立即更新", "取消", new MyTipDialog.DialogMethod() {
						public void sure() {
							gotoWebPlay(context, ary[0]);
							Spu.saveV(context, VSpu.auto_update.name());
						}
					}, 15);
				}
			}
		}
	}
	
	public static void gotoWebPlay(final Context act, final String url){
		try{
			Intent intent = new Intent();
            intent.setAction("android.intent.action.VIEW");    
            Uri content_url = Uri.parse(url);
            intent.setData(content_url);  
			BaseUtils.startActivity(act,intent);
		}catch(ActivityNotFoundException e){
			MyTipDialog.popDialog(act,"检测到本机未安装浏览器应用，推荐下载安装“UC浏览器”或“QQ浏览器”！");
			e.printStackTrace();
		}catch(Exception e){
			MyTipDialog.popDialog(act, "软件异常：请重新下载最新版本的软件.");
			e.printStackTrace();
			LogUtils.eTag(TAG, StringUtils.getExcetionInfo(e));
		};
	}
	public static void checkUpdate(final Activity act){
		checkUpdate(act, true);
	}
	public static void checkUpdate(final Activity act, final boolean isTip){
		Mtas.checkUpdate(act, isTip);
	}

	public static void popEditDlg(final Activity ctx, String title, final MyTipDialog.DialogMethod dm){
		popEditDlg(ctx,title, "", "输入数据...","确认提交","取消",-1,dm);
	}
	public static void popEditDlg(final Activity ctx, String title, String msg, String hint,
                                  String ok, String cancel, int maxWordNums, final IDialogMethod dm){
		if(ctx == null) return;
		final EditText et = new EditText(ctx);
		et.setHint(hint);
		if(maxWordNums > 0) {
			et.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxWordNums)});
		}
		et.setText(msg);
		new AlertDialog.Builder(ctx)
				.setTitle(title)
				.setView(et)
				.setPositiveButton(ok, new DialogInterface.OnClickListener(){
					public void onClick(DialogInterface dialog, int which){
						String v = et.getText().toString().trim();
						if(StringUtils.isNullStr(v)){
							popMyToast(ctx,"数据不能为空哦~");
						}
						if (dm != null) {
							dm.sure(v);
							dm.sure();
						}
					}
				})
				.setNegativeButton(cancel, new DialogInterface.OnClickListener(){
					public void onClick(DialogInterface dialog, int which){
						String v = et.getText().toString().trim();
						if(dm != null) {
							dm.cancel(v);
							dm.cancel();
						}
					}
				})
				.show();

	}

	public static void gotoFeedbackDlg(final Activity ctx){
		if(ctx == null) return;
		final EditText et = new EditText(ctx);
		et.setHint("输入投诉建议或使用本软件遇到的问题");
		et.setFilters(new InputFilter[]{new InputFilter.LengthFilter(300)});
		new AlertDialog.Builder(ctx)
		.setTitle("投诉建议")
		.setView(et)
		.setPositiveButton("确认提交", new DialogInterface.OnClickListener(){
			public void onClick(DialogInterface dialog, int which){
				NbKeyBoardUtil.getInstance(ctx).hide();
				String content = et.getText().toString();
				handleFeedback(ctx, content);
			}
		})
		.setNegativeButton("取消", null).show();
	}
	public static void gotoFeedbackUI(final Activity ctx){
		if(ctx == null) return;
		gotoFeedbackUI(ctx, null);
	}
	public static void gotoFeedbackUI(final Activity ctx, FeedbackUI.DefFeedbackCallback callback){
		if(ctx == null) return;
//		FeedbackUI.start(ctx, null, callback);
		FeedbackUIV2.start(ctx);
	}
	public enum VFb{debug_open,debug_close,ads_open,ads_close,nilapp,
		main$,app$,menu$,pn$,apps$,pns$,web$,path$,sc$,xy$,exit2$,spu$,nc$}
	public static void handleFeedback(final Activity ctx, String content){
		try{
			if (StringUtils.isNullStr(content)) {//null
				popMyToast(ctx, "内容不能为空");
			}else if(IsHaop(ctx)){//No GoodRate
				popMyToast(ctx, "感谢您的反馈，处理好后会第一时间通知您哦.");
				AdSwitchUtils.reportError(ctx, MobileInfoUtil.getInstance(ctx).getBriefInfo());
				AdSwitchUtils.reportError(ctx, "NiN投诉建议："+content);
			}else if(StringUtils.equalsIgnoreCase(content, VFb.debug_open.name())){
				Spu.saveV(ctx, VSpu.debug.name(), "suc");
				popMyToast(ctx, "开发者调试模式已开启.");
			}else if(StringUtils.equalsIgnoreCase(content, VFb.debug_close.name())){
				Spu.saveV(ctx, VSpu.debug.name(), "no");
				popMyToast(ctx, "开发者调试模式已关闭.");
			}else if(StringUtils.equalsIgnoreCase(content, VFb.ads_open.name())){
				Spu.saveV(ctx, VSpu.ads.name(), "suc");
				popMyToast(ctx, "广告开关全部已开启.");
			}else if(StringUtils.equalsIgnoreCase(content, VFb.ads_close.name())){
				Spu.saveV(ctx, VSpu.ads.name(), "no");
				popMyToast(ctx, "广告开关全部已关闭.");
			}else if(StringUtils.equalsIgnoreCase(content, VFb.nilapp.name())){
				gotoActivity(ctx, "niltools.app.program.ui.NilAppUI", false);
			}else if(StringUtils.equalsIgnoreCase(content, VFb.main$.name())){
				doAbTask(new AbTaskListener(){
					String appInfo = "appInfo is null.";
					public void get() {
						appInfo = MobileInfoUtil.getInstance(ctx).getAllInfo();
					}
					public void update() {
						MyTipDialog.popNilDialog(ctx, appInfo);
					}
				});
			}else if(StringUtils.equalsIgnoreCase(content, VFb.app$.name())){
				popAppInfoDlg(ctx);
			}else if(StringUtils.containsIgnoreCaseHead(content, VFb.menu$.name())){
				MyTipDialog.popNilDialog(ctx, Arrays.toString(VFb.values()));
			}else if(StringUtils.containsIgnoreCaseHead(content, VFb.pn$.name())){
				gotoActivity(ctx, content.replace(VFb.pn$.name(), ""), false);
			}else if(StringUtils.containsIgnoreCaseHead(content, VFb.apps$.name())){
				doAbTask(new AbTaskListener(){
					String appInfo = "app is null.";
					public void get() {
						appInfo = MobileInfoUtil.getInstance(ctx).getNilApp();
					}
					public void update() {
						final String title = "欢迎进入调试模式";
						MyTipDialog.popDialog(ctx,title, appInfo, "分享", "导入","关闭", new MyTipDialog.DialogMethod() {
							public void sure() {
								BaseUtils.openShare(ctx, appInfo);
							}
							public void neutral() {
								new Spu(ctx, "nil_apps").saveSharedPreferences("nil_apps_key", appInfo);
								popMyToast(ctx, "数据导入成功.");
							}
						}, 12);
					}
				});
			}else if(StringUtils.containsIgnoreCaseHead(content, VFb.pns$.name())){
				gotoOutActivity(ctx, content.replace(VFb.pns$.name(), ""));
			}else if(StringUtils.containsIgnoreCaseHead(content, VFb.sc$.name())){
				OpenMarket.go(ctx, content.replace(VFb.sc$.name(), ""));
			}else if(StringUtils.containsIgnoreCaseHead(content, VFb.path$.name())){
				MyTipDialog.popDialog(ctx, content.replace(VFb.path$.name(), ""));
			}else if(StringUtils.containsIgnoreCaseHead(content, VFb.web$.name())){
				gotoWebPlay(ctx, content.replace(VFb.web$.name(), ""));
			}else if(StringUtils.containsIgnoreCaseHead(content, VFb.xy$.name())){
				popPrivateDlg(ctx, null);
			}else if(StringUtils.containsIgnoreCaseHead(content, VFb.exit2$.name())){
				onExit2Rate(ctx);
			}else if(StringUtils.containsIgnoreCaseHead(content, VFb.spu$.name())){
				String[] ary = content.replace(VFb.spu$.name(), "").split("##");
				Spu.saveV(ctx, ary[0], ary[1]);
				popMyToast(ctx, "配置修改信息："+ Arrays.toString(ary));
			}else if(StringUtils.containsIgnoreCaseHead(content, VFb.nc$.name())){
				
			}else if(StringUtils.containsIgnoreCaseHead(content, VFb.nc$.name())){
				
			}else if(StringUtils.containsIgnoreCaseHead(content, VFb.nc$.name())){
				
			}else{
				popMyToast(ctx, "感谢您的反馈，处理好后会第一时间通知您哦.");
				AdSwitchUtils.reportError(ctx, MobileInfoUtil.getInstance(ctx).getBriefInfo());
				AdSwitchUtils.reportError(ctx, "NiN投诉建议："+content);
			}
		}catch(Exception e){
			LogUtils.eTag(TAG, StringUtils.getExcetionInfo(e));
		}
	}
	
	private static final String KEY_UMENG_CONTACT_INFO_PLAIN_TEXT = "plain";
	public static void sendFeedbackInfo(final Context ctx, String content){
		if(ctx == null || StringUtils.isNullStr(content)) return;
		AdSwitchUtils.reportError(ctx, "NiN用户反馈："+content);
	}

    public static void popDebugToast(Object obj){
	    if(getIsDebug(Utils.getApp())) {
            if (obj instanceof Integer) {
                ToastUtils.showLong((Integer) obj);
            } else if (obj instanceof String) {
                ToastUtils.showLong((String) obj);
            } else if (obj != null) {
                ToastUtils.showLong(obj.toString());
            }
        }
    }
	public static void popMyToast(Object msg){
		popMyToast(Utils.getApp(), msg);
	}
	public static void popMyToast(Context ctx, Object msg){
		if(ctx == null) return;
		try {
			ToastUtils.showLong(getS(ctx, msg));
		}catch (Exception e){
			e.printStackTrace();
		}
	}
	public static void popMyToastShort(Object msg){
		try {
			ToastUtils.showShort(getS(msg));
		}catch (Exception e){
			e.printStackTrace();
		}
	}
	public static void showToastInThread(final Activity act, final String msg){
		if(act != null){
			act.runOnUiThread(new Runnable() {
				public void run() {
					popMyToast(act, msg);
				}
			});
		}
	}
	private static AlertDialog sNetDlg;
	public static void popNetTipDlg(final Context ctx){
		if(ctx == null || (sNetDlg !=null && sNetDlg.isShowing())) return;
		sNetDlg = MyTipDialog.getMyDialog(ctx, "网络已断开", "3G或WiFi网络均未被打开，关键时刻怎能断网，马上前往打开网络？", "确认", "取消", new MyTipDialog.DialogMethod() {
			public void sure() {
				NetworkUtils.gotoNetworkSetUI(ctx);
			}
		});
		sNetDlg.show();
	}
	
	public static void popOffersWall(Context ctx){
		if(ctx == null) return;
		String msg = "加油赚取"+ Vs.min_points.value +"积分，即可领取大礼包哦!";
		popMyToast(ctx, msg);
	}
	
	public static String getTiebaUrl(Context ctx){
		//http://tieba.baidu.com/
		if(ctx == null) return XSEUtils.decode("1ktiGPonIN1caMzXW98g4pFSupoSftBXrmk5ZQ5hdfLdbCZX4Q%3D");
		return Vs.tieba_url.value;
	}
	
	public static boolean hasGoodRate(final Context ctx){
		return hasGoodRate(ctx, true);
	}
	public static boolean hasGoodRate(final Context context, boolean isTip){
		boolean isOK = false;	// false:no open goodrate
		final Context ctx = (context == null) ? Utils.getApp() : context;
		//获取在线的百分比
		int percent = Mtas.defRatePercent;
		try {
			percent = Integer.parseInt(Vs.rate_percent.value);
		} catch (Exception e) {
			percent = Mtas.defRatePercent;
			e.printStackTrace();
		}

		String clearPoint = Spu.loadV(ctx, VSpu.good_rate.name());	//读取好评标识
		boolean isGoodRate = (!Sws.Rate.flag || Spu.isSucV(clearPoint)) || !getProb(percent); // 好评关或已评论或概率false：都表示已经好评
		if(!isGoodRate){
			isOK = true;
			if(isTip) {
				String defMsg = "开启ViP功能##首次使用ViP功能需要给予软件5星好评(优先体验新版全功能软件)，小伙伴们立马来开启哟！!##立马开启##残忍拒绝";
				String msg = StringUtils.getValue(Vs.rate_text.value, defMsg);
				String[] ary = msg.split("##");
				if (ary == null || ary.length < 4) {
					ary = defMsg.split("##");
					if (!StringUtils.isNullStr(Vs.rate_text.value)) ary[1] = Vs.rate_text.value;
				}
				MyTipDialog.popDialog(ctx, ary[0], ary[1], ary[2], ary[3], new MyTipDialog.DialogMethod() {
					public void sure() {
						OpenMarket.go(ctx);
						Spu.saveV(ctx, VSpu.good_rate.name());
						AdSwitchUtils.reportEvent(ctx, SjType.rate_ok);
					}

					public void cancel() {
						AdSwitchUtils.reportEvent(ctx, SjType.rate_no);
					}
				});
			}
		}


		return isOK;
	}
	public interface IGoodRate {
		public void finishRate(Context ctx);
		public void notRate(Context ctx);
		public void confirmRate(Context ctx);
		public void cancelRate(Context ctx);
	}
	public static abstract class DefGoodRate implements IGoodRate{
		public abstract void finishRate(Context ctx);
		public void notRate(Context ctx){}
		public void confirmRate(Context ctx){
			OpenMarket.go(ctx);
			Spu.saveV(ctx, VSpu.good_rate.name());
			AdSwitchUtils.reportEvent(ctx, SjType.rate_ok);
		}
		public void cancelRate(Context ctx){
			AdSwitchUtils.reportEvent(ctx, SjType.rate_no);
		}
	}
	public static void gotoGoodRateDlg(){
		gotoGoodRateDlg(Utils.getApp());
	}
	public static void gotoGoodRateDlg(final Context ctx){
		gotoGoodRateDlg(ctx, null, new DefGoodRate() {
			@Override
			public void finishRate(Context ctx) {}
		}, true);
	}
	public static void gotoGoodRateDlg(final Context ctx, final IGoodRate gr){
		gotoGoodRateDlg(ctx, null, gr, true);
	}
	public static void gotoGoodRateDlg(final Context ctx, final String rateText, final IGoodRate gr){
		gotoGoodRateDlg(ctx, rateText, gr, true);
	}
	public static void gotoGoodRateDlg(final Context ctx, final String rateText, final IGoodRate gr, boolean isTip){
		String clearPoint = Spu.loadV(ctx, VSpu.good_rate.name());	//是否好评
		if(Spu.isSucV(clearPoint)){
			if(gr != null) gr.finishRate(ctx);
		}else{
			if(!Sws.Rate.flag){
				if(gr != null) gr.notRate(ctx);
			}else{
				if(isTip){
					// rateText-->rate_text-->defMsg
					String defMsg = "开启ViP功能##首次使用ViP功能需要给予软件5星好评(优先体验新版全功能软件)，小伙伴们立马来开启哟！!##立马开启##残忍拒绝";
					String msg = StringUtils.getValue(rateText, Vs.rate_text.value);
					if(StringUtils.isNullStr(msg)) msg = defMsg;
					String[] ary = msg.split("##");
					if(ary.length < 4){
						ary = defMsg.split("##");
						ary[1] = msg;
					}
					MyTipDialog.popDialog(ctx, ary[0], ary[1], ary[2], ary[3], new MyTipDialog.DialogMethod() {
						public void sure() {
							if(gr != null) gr.confirmRate(ctx);
						}
						public void cancel() {
							if(gr != null) gr.cancelRate(ctx);
						}
					});
				}
			}
		}
	}

	public static void onExitNormal(final Activity act){
		onExitNormal(act, true);
	}
	public static void onExitNormal(final Activity act, final boolean isCompleteExit){
		if(act == null) return;
		MyTipDialog.popDialog(act, "退出", "确认要退出程序？", "确认", "取消",
				new MyTipDialog.DialogMethod() {
			public void sure() {
				onAutoExit(act, isCompleteExit);
			}
		});
	}
	public static void onExit(final Activity act){
		if(act == null) return;
		if(NetworkUtils.isNetworkAvailable(act) && !Sws.CompleteExit.flag){	//网络正常 & CompleteExit为false
			// 1.上报App信息(包名、版本、打包信息、广告ID信息、广告开关信息)
			reportAllInfo(act , ReportType.AllInfo);

			if(Sws.ERate.flag){	//exit's rate
				onExit2Rate(act);	//退出时的好评对话框
			}else{
				onExit2Ad(act);	//插屏广告+退出提示对话框
			}
		}else{
			if(ZzAdUtils.openVVV(act, VType.out) != VType.out.ordinal()){//exit=-1
				// 插屏+彻底退出程序提示对话框：
				onExitNormal(act, true);	//Sws.Exit打开时可以强制退出
			}else{
				//带有退出广告（如Am的退屏、Gdt的退出原生广告）时，自己处理了退出操作
				//Mtas.onKillProcess(act);
			}
		}
	}
	public static void onExit2Rate(final Activity act){
		if(act == null) return;
		String clearPoint = Spu.loadV(act, VSpu.good_rate.name());	//是否好评
		if(Spu.isSucV(clearPoint)){
			onExit2Ad(act);
		}else{
			String defMsg = "温馨提醒##感谢您使用我们的应用程序，请给予您的宝贵建议或应用评级。##宝贵评级##取消";
			String[] msg = StringUtils.getValues(Vs.exit_rate.value, defMsg, defMsg.split("##").length);
			MyTipDialog.popDialog(act, msg[0],msg[1],msg[2],msg[3], new MyTipDialog.DialogMethod() {
				public void sure() {
					OpenMarket.go(act);
					Spu.saveV(act, VSpu.good_rate.name());
					AdSwitchUtils.reportEvent(act, SjType.rate_ok);
				}
				public void cancel() {
					AdSwitchUtils.reportEvent(act, SjType.rate_no);
					onAutoExit(act);
				}
			});
		}
	}
	public static void onExit2Ad(final Activity act){
		if(act == null) return;
		if(ZzAdUtils.openVVV(act, VType.out) != VType.out.ordinal()){//exit=-1
			// 插屏+退出提示对话框：
			onExitNormal(act, false);	//Sws.Exit打开时可以强制退出
		}else{
			//带有退出广告（如Am的退屏、Gdt的退出原生广告）时，自己处理了退出操作
			//Mtas.onKillProcess(act);
		}
	}
	
	public static void gotoGetMoneyUI(final Activity act){
		/*if(act != null){
			if(AdSwitchUtils.isCpAD) MyQqAdUtils.getInstance(act).showCPAd();
			MyDlGmUtils.getInstance(act).showOffersAd();
			popMyToast(act, "(1元起提现，大部分地区2小内到账，赶紧开始日赚50-100元哦！)");
		}*/
	}
	
	public static boolean popPrivateDlgGg(final Activity act, final IDialogMethod dm){
		boolean isOK = false;
		String channel = AppUtils.getMetaData(act, "UMENG_CHANNEL");
		if(AppUtils.checkPermission(act, permission.READ_PHONE_STATE)
				&& (StringUtils.containsIgnoreCase(channel, "_gg")|| StringUtils.equalsIgnoreCase(channel, "google"))){
			String up = Spu.loadV(act, VSpu.private_xy.name());
			if(Spu.isSucV(up)){
				if(dm != null) dm.sure();
			}else{
				isOK = true;
				popPrivateDlg(act, dm);
			}
		}
		return isOK;
	}
	public static String getRightInfo(final Activity act){
		//xml:Copyright©2016&#8211;2025 All Rights Reserved
		String def = null;
		try{
			int year = Calendar.getInstance().get(Calendar.YEAR);
			int end = year+9;
			def = String.format("Copyright©%d-%d All Rights Reserved", year, end);
		}catch(Exception e){}
		if(StringUtils.isNullStr(def)) def = "Copyright©2016-2025 All Rights Reserved";
		
		if(act != null){
			def = StringUtils.getValue(getS(act, R.string.ab_right_info), def);
		}
		return def;
	}
	public static void popPrivateDlg(final Activity act, final IDialogMethod dm){
		if(act != null){
			String def = "欢迎您使用##手机客户端软件，本软件使用完全免费。在非WiFi网络环境下，使用过程中会产生移动数据流量费，流量费请咨询当地运营商。\n　　在使用本软件的过程中，为了正常、安全的使用本软件的相关功能，将会获取你的手机ID权限以读取您的手机中的用户标识、确定您的身份，也会开启您的WiFi网络、使用您的地理位置、电话短信管理等权限以给您提供更便捷、贴心的优质服务。\n　　本软件开发者隐私权保护声明：若没有经过用户允许，本软件绝不会收集用户的各种隐私数据，也不会泄露用户的个人信息，请您放心使用。\n";
			def = StringUtils.getValue(Vs.dialog_private.value, def);
			String msg = StringUtils.getValue2Cs(def, AppUtils.getAppName(act));
			String neutral = getIsDebug(act)? "调试" : null;
			MyTipDialog.popDialog(act, "隐私声明", msg, "同意并继续", neutral, "退出", new MyTipDialog.DialogMethod() {
				public void sure() {}
				public void sure(DialogInterface dialog, int which) {
					dialog.dismiss();
					Spu.saveV(act, VSpu.private_xy.name());
					if(dm != null) dm.sure();
				}
				public void neutral() {
					popAppInfoDlg(act);
				}
				public void cancel() {
					// dialog.dismiss();
					if(dm != null) dm.cancel();
					onAutoExit(act);
				}
			}, -1, false, false);
		}
	}
	public static void onAutoExit(final Activity act){
		onAutoExit(act, true);
	}
	public static void onAutoExit(final Activity act, boolean isCompleteExit){
		if(act != null){
			Mtas.onKillProcess(act);
			activityExit(act);

			// 判断是否完全退出
			if(isCompleteExit || Sws.Exit.flag){
				XSEUtils.free(); //加密库释放

				android.os.Process.killProcess(android.os.Process.myPid());//杀死当前应用活动的进程
				System.exit(0);//0：正常退出 1：非正常退出
			}
		}
	}

	public static void openCopy(String content){
		openCopy(Utils.getApp(), content, true);
	}
    /** 判断是否已好评*/
    public static boolean IsHaop(Context ctx){
		return hasGoodRate(ctx, false);
	}
	/** 通过当前连接WiFi名称判断是否处于调试模式*/
	public static boolean getIsDebug(Context ctx){
		if(ctx == null) ctx = Utils.getApp();
		boolean isOk;
		String channel = AppUtils.getMetaChannel(ctx);
		if (StringUtils.equalsIgnoreCase(channel, "open")
				|| StringUtils.equalsIgnoreCase(channel, "debug")) {
			isOk = true;
		} else {
			isOk = Spu.isSucK(ctx, VSpu.debug.name());
		}
		return isOk;
	}
	public static boolean getIsDebug(){
		return getIsDebug(Utils.getApp());
	}
	public static boolean IsVd(Context ctx){
		return getIsDebug(ctx) || Sws.Video.flag;
	}
	public static boolean IsCj(Context ctx){
		return /*getIsDebug(ctx) || */Sws.Collect.flag;
	}
    /** 异步调用一个耗时操作*/
    public static void doAbTask(AbTaskListener tt){
		AbTaskItem item = new AbTaskItem();
		item.setListener(tt);
		new AbTask().execute(item);
    }
    
    public static void activityExit(Activity act){
    	try {
			//清除栈内全部Activity(跳转至主界面，使用FLAG_ACTIVITY_CLEAR_TOP标识)
			Intent startMain = new Intent(Intent.ACTION_MAIN);
			startMain.addCategory(Intent.CATEGORY_HOME);
			startMain.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(act, startMain);
			act.finish();
		}catch (Exception e){
    		e.printStackTrace();
		}
    }

	private static long exitTime = 0;
	public static boolean onKeyDown(Activity act, int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN){
			if((System.currentTimeMillis()-exitTime) > 2000){
				ToastUtils.showShort("再按一次返回退出程序");
				exitTime = System.currentTimeMillis();
			} else {
				BaseUtils.onAutoExit(act);
			}
			return true;
		}
		return act.onKeyDown(keyCode, event);
	}
    
    /** 通过类名跳转界面*/
    public static void gotoActivity(Context act, String className){
    	gotoActivity(act, className, true);
    }
    public static void gotoActivity(Context act, String className, boolean isFinish){
    	gotoActivity(act, className, 0, isFinish);
    }
    public static void gotoActivity(Context act, String className, long delayMillis){
    	gotoActivity(act, className, delayMillis, true);
    }
    public static void gotoActivity(final Context act, final String className, final long delayMillis, final boolean isFinish){
    	String msg = "界面跳转出现异常，请更新软件后再试.";
    	if(act == null || !Fs.findMyClass(className)){
    		MyTipDialog.popDialog(act, "软件异常", msg, "关闭");
    	}else{
			long millis = (delayMillis < 0) ? 0 : delayMillis;
    		new Handler().postDelayed(new Runnable(){
    			public void run() {
    				try{
    	    			Intent intent = new Intent();
						intent.setClassName(act.getPackageName(), className);
						if (!(act instanceof Activity)) {
							intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						}
						//enterAnim|exitAnim为0，表示移除系统动画：
						BaseUtils.startActivity(intent, 0, 0);
						if(isFinish && act instanceof Activity) ((Activity) act).finish();
    	    		}catch(Exception e){
						LogUtils.eTag(TAG, StringUtils.getExcetionInfo(e));
						MyTipDialog.popDialog(act, "软件异常", e.toString(), "关闭", 12f);
						AdSwitchUtils.reportError(act, "NiN软件异常："+e.toString());
    	    		}
    			}
    		}, millis);
    	}
    }
    public static void gotoOutActivity(final Context act, final String packageName){
		try{
			if(StringUtils.containsIgnoreCase(packageName, "/")){
				String pn = packageName.split("/")[0];
				String cl = packageName.split("/")[1];
				Intent it = new Intent();
        		it.setClassName(pn, cl);
        		BaseUtils.startActivity(act,it);
			}else{
				if(AppUtils.findMyPackageName(act, packageName)){
				    Intent it = act.getPackageManager().getLaunchIntentForPackage(packageName);
				    BaseUtils.startActivity(act,it);
				}else{
					Intent it = new Intent();
	        		it.setClassName(act.getPackageName(), packageName);
	        		BaseUtils.startActivity(act,it);
				}
			}
		}catch(Exception e){
			LogUtils.eTag(TAG, StringUtils.getExcetionInfo(e));
			MyTipDialog.popDialog(act, "软件异常", e.toString(), "关闭", 12f);
			AdSwitchUtils.reportError(act, "NiN软件异常："+e.toString());
		}
	}

	public static void reStartActivity(Class<? extends Activity> curActivity, Class<? extends Activity> starActivity){
		reStartActivity(curActivity, starActivity,true);
	}
	public static void reStartActivity(Class<? extends Activity> curActivity, Class<? extends Activity> starActivity,
                                       boolean isFinishSelf){
		// 清除栈内全部Activity
		ActivityUtils.finishToActivity(curActivity, isFinishSelf);
		// 重新启动LoginActivity
		BaseUtils.startActivity(starActivity);
	}
	public static void reStartActivity(Context ctx){
		BaseUtils.gotoActivity(ctx, ActivityUtils.getLauncherActivity());
	}
	/**
	 * 重启APP，原理是关闭当前页面，重新启动启动页面<br>
	 *     注：此重启不会强退主进程、后台进程、服务等<br>
	 */
	public static void reStartApp(Context ctx){
		reStartActivity(ctx);
	}
    
    /** 从*.java文件目录获取图片 */
	public static Bitmap getBitmapFromSrc(Class<?> cls, String src){
		Bitmap bit = null;
		try {
			bit = BitmapFactory.decodeStream(cls.getResourceAsStream(src));
	    } catch (Exception e) {
	    	System.err.println("获取图片异常："+e.toString());
		}
		return bit;
	}
	public static void openShare(String msg){
		openShare(Utils.getApp(), msg, false);
	}
	public static void openShare(Context ctx, String msg){
		openShare(ctx, msg, false);
	}
	public static void openShare(Context ctx, String msg, boolean isAddTrail){
		if(ctx == null || StringUtils.isNullStr(msg)) return;
		try{
			String title = AppUtils.getAppName(ctx);
			Intent intent=new Intent(Intent.ACTION_SEND);
			intent.setType("text/plain");   //text/plain--image/*
			intent.putExtra(Intent.EXTRA_SUBJECT, "分享");
			intent.putExtra(Intent.EXTRA_TEXT, msg+((!isAddTrail) ? "":"\n(分享来自【"+title+"】)"));
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			BaseUtils.startActivity(ctx,  Intent.createChooser(intent, title));
		}catch(Exception e){
			LogUtils.eTag(TAG, StringUtils.getExcetionInfo(e));
			e.printStackTrace();
			MyTipDialog.popDialog(ctx, "软件异常：此次分享异常，请重新下载最新版本的软件.");
		}
	}

	public static void openCopy(Context ctx, String content){
		openCopy(ctx, content, true);
	}
	@SuppressLint("ObsoleteSdkInt")
	public static void openCopy(Context ctx, String content, boolean isTip){
		if(ctx == null || StringUtils.isNullStr(content)) return;
		try{
			/*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {//11-3.0
		        ClipboardManager cmb = (ClipboardManager)ctx.getSystemService(Context.CLIPBOARD_SERVICE);
		    	ClipData clip = ClipData.newPlainText("label", content);
		    	cmb.setPrimaryClip(clip);
			}else{
				ClipboardManager cmb = (ClipboardManager)ctx.getSystemService(Context.CLIPBOARD_SERVICE);
	            cmb.setText(content);
			}*/

			ClipboardUtils.copyText(content);
	    	if(isTip) popMyToast(ctx, "复制成功.");
		}catch(Exception e){
			LogUtils.eTag(TAG, StringUtils.getExcetionInfo(e));
			e.printStackTrace();
			MyTipDialog.popDialog(ctx, "软件异常：此次复制异常，请重新下载最新版本的软件.");
		}
	}
	@SuppressLint("ObsoleteSdkInt")
	public static void openCopy(Context ctx, String content, String tipInfo){
		if(ctx == null || StringUtils.isNullStr(content)) return;
		try{
			/*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {//11-3.0
				ClipboardManager cmb = (ClipboardManager)ctx.getSystemService(Context.CLIPBOARD_SERVICE);
				ClipData clip = ClipData.newPlainText("label", content);
				cmb.setPrimaryClip(clip);
			}else{
				ClipboardManager cmb = (ClipboardManager)ctx.getSystemService(Context.CLIPBOARD_SERVICE);
				cmb.setText(content);
			}*/

			ClipboardUtils.copyText(content);
			if(StringUtils.noNullStr(tipInfo)) popMyToast(ctx, tipInfo);
		}catch(Exception e){
			LogUtils.eTag(TAG, StringUtils.getExcetionInfo(e));
			e.printStackTrace();
			MyTipDialog.popDialog(ctx, "软件异常：此次复制异常，请重新下载最新版本的软件.");
		}
	}

	public static String openPaste(){
		return openPaste(Utils.getApp(), null);
	}
	public static String openPaste(Context ctx){
		return openPaste(ctx, null);
	}
	@SuppressLint("ObsoleteSdkInt")
	public static String openPaste(Context ctx, String tipInfo){
		String content = null;
		if(ctx != null){
			try{
				/*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
					ClipboardManager cmb = (ClipboardManager)ctx.getSystemService(Context.CLIPBOARD_SERVICE);
					if(cmb != null) {
						content = cmb.getPrimaryClip().getItemAt(0).getText().toString();
					}
				}else{
					ClipboardManager cmb = (ClipboardManager)ctx.getSystemService(Context.CLIPBOARD_SERVICE);
					if(cmb != null) {
						content = cmb.getText().toString();
					}
				}*/
				content = ClipboardUtils.getText().toString();

				if(StringUtils.noNullStr(tipInfo)) popMyToast(ctx, tipInfo);
			}catch(Exception e){
				LogUtils.eTag(TAG, StringUtils.getExcetionInfo(e));
				e.printStackTrace();
				MyTipDialog.popDialog(ctx, "软件异常：此次粘贴异常，请重新下载最新版本的软件.");
			}
		}
		return content;
	}

	public static void gotoMainUI(Context ctx){
		gotoMainUI(ctx, false);
	}
	public static void gotoMainUI(Context ctx, boolean skipKp){
		gotoMainUI(ctx, skipKp ? 0 : 2500/*默认跳过时间2.5秒*/, skipKp);
	}
	public static void gotoMainUI(Context ctx, int millis, boolean skipKp){
		Object mainActivity = getS(ctx, R.string.main_activity);
		String className = ZFactory.sSp;
		if(AppUtils.hasGdtKpPermission(ctx) && !skipKp){//splash ui
			BaseUtils.gotoActivity(ctx, className);
		}else if((mainActivity instanceof String)){	//main ui
			BaseUtils.gotoActivity(ctx, (String) mainActivity, millis);
		}else if(Fs.findMyClass(ZFactory.sZz)){	//ZFactory
			BaseUtils.gotoActivity(ctx, ZFactory.sZz, millis);
		}
	}

	/**
	 * 分享功能
	 *
	 * @param ctx
	 *            上下文
	 * @param activityTitle
	 *            Activity的名字
	 * @param msgTitle
	 *            消息标题
	 * @param msgText
	 *            消息内容
	 * @param imgPath
	 *            图片路径，不分享图片则传null
	 */
	public static void shareMsg(Context ctx, String activityTitle, String msgTitle, String msgText, String imgPath) {
		try {
			Intent intent = new Intent(Intent.ACTION_SEND);
			if (imgPath == null || imgPath.equals("")) {
				intent.setType("text/plain"); // 纯文本
			} else {
				File f = new File(imgPath);
				if (f != null && f.exists() && f.isFile()) {
					intent.setType("image/jpg");
					Uri u = NbUriUtils.fromFile(f, true);
					intent.putExtra(Intent.EXTRA_STREAM, u);
				}
			}
			intent.putExtra(Intent.EXTRA_SUBJECT, msgTitle);
			intent.putExtra(Intent.EXTRA_TEXT, msgText);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			BaseUtils.startActivity(ctx,  Intent.createChooser(intent, activityTitle));
		}catch (Exception e){
			MyTipDialog.popNilDialog(ctx, "系统异常：\n"+e.toString());
		}
	}

	public static void showInnerLog(){
		showInnerLog(Utils.getApp());
	}
	public static void showInnerLog(Context ctx){
		showInnerLog(ctx, ACacheUtils.getCode() + "\n\n" + BaseUtils.getAppInfo(ctx));
	}
	public static void showInnerLog(Context ctx, Object... msg){
		MyTipDialog.popNilDialog(ctx, StringUtils.concatObjectDef(msg));
	}
	public static void addShowInnerLog(final Activity act, View v){
		try {
			if (v != null) {
				v.setOnLongClickListener(new View.OnLongClickListener() {
					@Override
					public boolean onLongClick(View view) {
						BaseUtils.showInnerLog(act);
						return true;
					}
				});
			}
		}catch (Exception e){
			e.printStackTrace();
		}
	}
	public static void addShowInnerLogV2(final Activity act, View v){
		try {
			if (v != null) {
				v.setOnLongClickListener(new View.OnLongClickListener() {
					@Override
					public boolean onLongClick(View view) {
						final String[] ary = {"查看调试对话框", "查看在线参数", "查看机器码", "查看系统版本", "查看登录和订单", "购买会员"};
						MyTipDialog.popItemDialog(act, "更多", ary, "关闭", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								int index = 0;
								if(which == index++){
									BaseUtils.showInnerLog(act);
								}else if(which == index++){
									BaseUtils.showInnerLog(act, XmbOnlineConfigAgent.getCacheDatas());
								}else if(which == index++){
									BaseUtils.doCode(act);
								}else if(which == index++){
									BaseUtils.doSyetemVersion(act);
								}else if(which == index++){
									BaseUtils.doUserLogin2Order(act);
								}else if(which == index++){
									//直接进入支付：
									PayVipActivity.start(Utils.getApp(), ary[5]);
								}
							}
						});

						return true;
					}
				});
			}
		}catch (Exception e){
			e.printStackTrace();
		}
	}

	public static String getGradleValue(Context ctx, String gradleName, String key){
		String result = null;
		try {
			// getGradleValue("gradle","versionCode");
			/*ResourceBundle bundle = ResourceBundle.getBundle(gradleName);//gradle为properties的文件名
			result = bundle.getString(key);//test_key是properties文件中的key值*/
			Properties properties = new Properties();
			InputStream is =  ctx.getClassLoader().getResourceAsStream(gradleName);//path
			properties.load(is);
			result= properties.getProperty(key);//test_key是properties文件中的key值
		}catch (Exception e){
			result = null;
			e.printStackTrace();
		}
		return result;
	}

	public  interface ISafeApi{
		void execute(Object obj);
	}
	public static boolean safeApi(ISafeApi api){
		return safeApi(api, null);
	}
	public static boolean safeApi(ISafeApi api, Object obj){
		boolean isOK = false;
		try{
			if(api != null){
				api.execute(obj);
				isOK = true;
			}
		}catch (Exception e){
			//e.printStackTrace();
			isOK = false;
		}
		return isOK;
	}

	public static void reportEvent(String event_name, String content, final XMBApiCallback<ResultBean> callback){
		XMBApi.reportBigData(event_name, content,callback);
	}
	public static void reportEvent(String event_name, String content){
		reportEvent(event_name,content,null);
	}

	/**
	 * 清除全部缓存数据<br>
	 */
	public static void clearAllCache(){
		try {
			ACacheUtils.clearObject();	//清除ACache缓存在SD卡和包目录下的数据

			CleanUtils.cleanInternalCache();	//清除内部缓存
			CleanUtils.cleanInternalFiles();	//清除内部文件
			CleanUtils.cleanInternalDbs();		//清除内部数据库
			CleanUtils.cleanInternalSp();		//清除内部SP
			CleanUtils.cleanExternalCache();	//清除外部缓存
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static void clearXmbCache(){
		try {
			ACacheUtils.clearObject();	//清除ACache缓存在SD卡和包目录下的数据

			CleanUtils.cleanInternalSp();	//清除内部SP
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 用于开启非debug或open渠道的日志输出，重启APP后生效<br>
	 */
	public static void openLogOut(){
		openLogOut(Utils.getApp(), VSpu.debug.name());
	}
	public static void openLogOut(Context ctx){
		openLogOut(ctx, VSpu.debug.name());
	}
	public static void openLogOut(Context ctx, String key){
		Spu.saveV(ctx, key, "suc");
	}
	public static void closeLogOut(Context ctx, String key){
		Spu.saveV(ctx, key, "no");
	}

	public static void sendEmailFeedback(Context ctx) {
		try {
			if(ctx == null) ctx = Utils.getApp();
			Intent data = new Intent(Intent.ACTION_SENDTO);
			String email = StringUtils.getValue(Vs.app_email.value, "appxsupport@163.com");
			data.setData(Uri.parse("mailto:" + email));
			data.putExtra(Intent.EXTRA_SUBJECT, ACacheUtils.getCode());
			data.putExtra(Intent.EXTRA_TEXT, "请在下方描述你遇到的问题（可以添加截图附件）：");
			BaseUtils.startActivity(ctx,  data);
		}catch (Exception e){
			e.printStackTrace();
		}
	}
	public static void sendEmailRetrievePassword(Context ctx) {
		try {
			if(ctx == null) ctx = Utils.getApp();
			Intent data = new Intent(Intent.ACTION_SENDTO);
			data.setData(Uri.parse("mailto:" + ctx.getString(R.string.retrieve_password_email)));
			String motif = ctx.getString(R.string.xuser_login_retrieve_password_email_motif);
			data.putExtra(Intent.EXTRA_SUBJECT, motif + ACacheUtils.getCode());
			String content = "请认真填写以下信息，方便快速找回密码（可以添加截图附件）\\n\\n登录的用户名：\\n支付宝订单号（必填）：\\n\\n";
			content = StringUtils.getValue(ctx.getString(R.string.xuser_login_retrieve_password_email_content), content);
			data.putExtra(Intent.EXTRA_TEXT, content);
			BaseUtils.startActivity(ctx,  data);
		}catch (Exception e){
			e.printStackTrace();
		}
	}

	/**
	 * percent取值范围[0,100]间的整数<br>
	 * percent<=0:表示0%机率<br>
	 * percent=25:表示25%的机率<br>
	 * percent>=100:表示100%的机率<br>
	 */
	public static boolean getProb(int percent){
		return new Random().nextInt(99) < percent;
	}
	public static void gotoGoodRate(Context context, MyTipDialog.DialogMethod callback){
		final Context ctx = (context == null) ? Utils.getApp() : context;
		int percent = Mtas.defRatePercent;
		try {
			percent = Integer.parseInt(Vs.rate_percent.value);
		} catch (Exception e) {
			percent = Mtas.defRatePercent;
			e.printStackTrace();
		}

		String clearPoint = Spu.loadV(ctx, VSpu.good_rate.name());	//读取好评标识
		boolean isGoodRate = (!Sws.Rate.flag || Spu.isSucV(clearPoint)) || !getProb(percent); // 好评关或已评论或概率false：都表示已经好评
		if(isGoodRate){
			if(callback != null) callback.sure();
		}else{
			String defMsg = "开启ViP功能##首次使用ViP功能需要给予软件5星好评(优先体验新版全功能软件)，小伙伴们立马来开启哟！!##立马开启##残忍拒绝";
			String msg = StringUtils.getValue(Vs.rate_text.value, defMsg);
			String[] ary = msg.split("##");
			if (ary == null || ary.length < 4) {
				ary = defMsg.split("##");
				if (!StringUtils.isNullStr(Vs.rate_text.value)) ary[1] = Vs.rate_text.value;
			}
			MyTipDialog.popDialog(ctx, ary[0], ary[1], ary[2], ary[3], new MyTipDialog.DialogMethod() {
				public void sure() {
					OpenMarket.go(ctx);
					Spu.saveV(ctx, VSpu.good_rate.name());
					AdSwitchUtils.reportEvent(ctx, SjType.rate_ok);
				}
				public void cancel() {
					AdSwitchUtils.reportEvent(ctx, SjType.rate_no);
				}
			});
		}
	}

	//判断是否为老年模式：
	public static boolean isOldMode(){
		return isOldMode(Utils.getApp());
	}
	public static boolean isOldMode(Context ctx){
		boolean isOK = false;
		try {
			if(ctx == null) ctx = Utils.getApp();
			isOK = ctx.getResources().getConfiguration().fontScale > 1.0f;
		}catch (Exception e){
			e.printStackTrace();
		}
		return isOK;
	}

	public static boolean startActivity(Intent intent, Context context, Bundle options) {
		if(context == null) context = Utils.getApp();
		if (!(context instanceof Activity)) {
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		}
		if (options != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
			context.startActivity(intent, options);
		} else {
			context.startActivity(intent);
		}
		return true;
	}
	public static boolean startActivity(Intent intent, Context context) {
		return startActivity(intent, context, null);
	}
	public static boolean startActivity(Intent intent) {
		return startActivity(intent, Utils.getApp());
	}
	public static boolean startActivity(Context context, Intent intent) {
		return startActivity(intent, context);
	}
	public static void startActivity(@NonNull final Class<? extends Activity> clz) {
		Context context = Utils.getApp();
		Intent intent = new Intent();
		intent.setComponent(new ComponentName(context.getPackageName(), clz.getName()));
		startActivity(intent, context);
	}
	public static boolean startActivity(@NonNull final Intent intent,
										@AnimRes final int enterAnim,
										@AnimRes final int exitAnim) {
		Context context = Utils.getApp();
		boolean isSuccess = startActivity(intent, context, ActivityOptionsCompat.makeCustomAnimation(context, enterAnim, exitAnim).toBundle());
		if (isSuccess) {
			if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN && context instanceof Activity) {
				((Activity) context).overridePendingTransition(enterAnim, exitAnim);
			}
		}
		return isSuccess;
	}
	public static boolean startActivity(@NonNull String pkg, @NonNull String cls) {
		return startActivity(Utils.getApp(), (Bundle)null, pkg, cls, (Bundle)null);
	}
	public static boolean startActivity(Context context, Bundle extras, String pkg, String cls, @Nullable Bundle options) {
		Intent intent = new Intent();
		if (extras != null) {
			intent.putExtras(extras);
		}

		intent.setComponent(new ComponentName(pkg, cls));
		return startActivity(intent, context, options);
	}
	public static void doCode(final Activity act){
		MyTipDialog.popDialog(act, "机器码", "联系客服时请告之你的机器码以方便帮你解决问题:\n" + ACacheUtils.getCode(),
				"复制机器码","取消",
				new MyTipDialog.DialogMethod() {
					public void sure() {
						BaseUtils.openCopy(act, ACacheUtils.getCode(), false);
						ToastUtils.showLong("机器码已经复制");
					}
				});
	}
	public static void doSyetemVersion(final Activity act){
		final String title = "系统版本";
		final String msg = MobileInfoUtil.getInstance(act).getMsgHead();
		MyTipDialog.popDialog(act, title, msg, "复制系统版本","取消",
				new MyTipDialog.DialogMethod() {
					public void sure() {
						BaseUtils.openCopy(act, msg);
					}
				}/*,12f*/);
	}
	public static void doUserLogin2Order(final Activity act){
		final String title = "用户登录和订单信息";

		StringBuffer buf =  new StringBuffer();
		Date curDate = new Date();
		Date netDate = (Date) ACacheUtils.getAppFileCacheObject(PayUtils.XPAY_NET_TIME_KEY);
		String dd = String.format("当前时间：%s\n网络时间：%s（缓存一天，清缓存刷新）\n",
				DateUtils.getStringByFormat(curDate, DateUtils.dateFormatYMDHMS),
				DateUtils.getStringByFormat(netDate, DateUtils.dateFormatYMDHMS));
		buf.append(dd).append("\n");

		buf.append("用户登录信息：").append("\n");
		buf.append(UserLoginDb.get()).append("\n");
		buf.append("\n");

		buf.append("用户订单信息：").append("\n");
		buf.append(OrderBeanV2.getOrderBeans()).append("\n");
		buf.append("\n");
		final String msg = buf.toString();
		BaseUtils.showInnerLog(act, msg);
	}
}
