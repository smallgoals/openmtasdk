package com.nil.sdk.ui.aid;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.DrawableCompat;

public class XMBSelectorViewUtil {
    public static int TINT_COLOR = 0x33000000;

    public static Drawable createPressImage(Drawable normalDrawable) {
        Drawable wrappedDrawable = DrawableCompat.wrap(normalDrawable.getConstantState()
                .newDrawable());
        DrawableCompat.setTintMode(wrappedDrawable, PorterDuff.Mode.DARKEN);//测试了下，这个DARKEN MODE最好
        DrawableCompat.setTint(wrappedDrawable, TINT_COLOR);//用半透明的颜色Tint上去
        return wrappedDrawable;
    }

}
