package com.nil.sdk.ui.aid;

import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.webkit.WebView;

import com.nil.sdk.nb.utils.NbWebviewUtils;

/**
 * 安全的WebView：移除有风险的Webview系统隐藏接口<br>
 */
public class MyWebView extends WebView {
    public MyWebView(@NonNull Context context) {
        super(context);
        NbWebviewUtils.removeRiskCode(this);
    }
    public MyWebView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        NbWebviewUtils.removeRiskCode(this);
    }
    public MyWebView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        NbWebviewUtils.removeRiskCode(this);
    }
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public MyWebView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        NbWebviewUtils.removeRiskCode(this);
    }
    public MyWebView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, boolean privateBrowsing) {
        super(context, attrs, defStyleAttr, privateBrowsing);
        NbWebviewUtils.removeRiskCode(this);
    }
}
