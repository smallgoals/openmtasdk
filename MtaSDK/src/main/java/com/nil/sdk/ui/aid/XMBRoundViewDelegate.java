package com.nil.sdk.ui.aid;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.view.View;

import com.nil.sdk.utils.DensityUtils;

/**
 * 圆角View 代理
 *如果想改为圆形view，修改rect_adius为很大的数字，比如10000
 */
public class XMBRoundViewDelegate {
    private final RectF roundRect = new RectF();
    private float rect_adius = 10;  //单位为dp
    private final Paint maskPaint = new Paint();
    private final Paint zonePaint = new Paint();
    private View mView;
    private Context mContext;

    public XMBRoundViewDelegate(View view, Context context){
        this(view,context,10);
    }
    public XMBRoundViewDelegate(View view, Context context, float adius){
        this.mView = view;
        this.mContext = context;
        rect_adius = adius;
        init();
    }

    private void init(){
        maskPaint.setAntiAlias(true);
        maskPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        //
        zonePaint.setAntiAlias(true);
        zonePaint.setColor(Color.WHITE);
    }

    /**
     * 从新设置圆角
     * @param adius
     */
    public void setRectAdius(float adius) {
        rect_adius = adius;
        if(mView != null){
            mView.invalidate();
        }
    }

    /**
     * 圆角区域设置
     * @param width
     * @param height
     */
    public void roundRectSet(int width,int height){
        roundRect.set(0, 0, width, height);
    }

    /**
     * 画布区域裁剪
     * @param canvas
     */
    public void canvasSetLayer(Canvas canvas){
        canvas.saveLayer(roundRect, zonePaint, Canvas.ALL_SAVE_FLAG);
        int px = DensityUtils.dp2px(mContext, rect_adius);
        canvas.drawRoundRect(roundRect, px, px, zonePaint);
        //
        canvas.saveLayer(roundRect, maskPaint, Canvas.ALL_SAVE_FLAG);
    }
}
