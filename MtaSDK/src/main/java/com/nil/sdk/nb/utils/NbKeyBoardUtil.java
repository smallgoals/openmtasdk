package com.nil.sdk.nb.utils;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

/**
 * @declaration 操作输入法的工具类。可以方便的关闭和显示输入法.<br>
 * @author ksA2y511xh@163.com<br>
 *2016-7-12 下午4:39:12<br>
 */
public class NbKeyBoardUtil {
	private static NbKeyBoardUtil instance;
	private InputMethodManager mInputMethodManager;
	private static Activity mActivity;

	private NbKeyBoardUtil() {
		try {
			mInputMethodManager = (InputMethodManager) mActivity
					.getSystemService(Context.INPUT_METHOD_SERVICE);
		}catch (Exception e){}
	}

	public static NbKeyBoardUtil getInstance(Activity activity) {
		mActivity = activity;
		if (instance == null) {
			instance = new NbKeyBoardUtil();
		}
		return instance;
	}

	/**
	 * 强制显示输入法
	 */
	public void show() {
		try{
			show(mActivity.getWindow().getCurrentFocus());
		}catch(Exception e){
			//e.printStackTrace();
		}
	}

	public void show(View view) {
		try{
			mInputMethodManager.showSoftInput(view, InputMethodManager.SHOW_FORCED);
		}catch(Exception e){
			//e.printStackTrace();
		}
	}

	/**
	 * 强制关闭输入法
	 */
	public void hide() {
		try{
			hide(mActivity.getWindow().getCurrentFocus());
		}catch(Exception e){
			//e.printStackTrace();
		}
	}

	public void hide(View view) {
		try{
			mInputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
		}catch(Exception e){
			//e.printStackTrace();
		}
	}

	/**
	 * 如果输入法已经显示，那么就隐藏它；如果输入法现在没显示，那么就显示它
	 */
	public void showOrHide() {
		mInputMethodManager.toggleSoftInput(0,InputMethodManager.HIDE_NOT_ALWAYS);
	}
}
