package com.nil.sdk.nb.utils;

public class NbView {
	public abstract class NoDoubleClickListener implements android.view.View.OnClickListener {
		public static final int MIN_CLICK_DELAY_TIME = 1000;// 这里设置不能超过多长时间
		private long lastClickTime = 0;
		protected abstract void onNoDoubleClick(android.view.View v);
		public void onClick(android.view.View v) {
			long currentTime = System.currentTimeMillis();
			if (currentTime - lastClickTime > MIN_CLICK_DELAY_TIME) {
				lastClickTime = currentTime;
				onNoDoubleClick(v);
			}
		}
	}
}
