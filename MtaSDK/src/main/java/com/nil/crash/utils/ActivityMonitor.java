package com.nil.crash.utils;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

import com.blankj.utilcode.util.LogUtils;
import com.nil.sdk.ui.BaseUtils;
import com.nil.sdk.utils.AppUtils;
import com.nil.sdk.utils.Spu;
import com.nil.sdk.utils.StringUtils;
import com.nil.vvv.utils.AdSwitchUtils;
import com.nil.vvv.utils.Mtas;

/**
 * 界面监听器<br>
 */
public class ActivityMonitor {
    protected final static String TAG = ActivityMonitor.class.getSimpleName();
    protected int mFinalCount = 0;
    protected long mStartTime = -1;
    protected long mGapSecond = Mtas.defKpGapSecond;

    protected static Application mApp;
    protected static ActivityMonitor activityMonitor;
    public static ActivityMonitor getInstance(Application app){
        if(activityMonitor == null){
            activityMonitor = new ActivityMonitor();
            mApp = app;
        }
        return activityMonitor;
    }

    public void addActivityMonitor(IActivityMonitor activityMonitor){
        if(mApp == null) return;
        mActivityMonitor = activityMonitor;
        addActivityMonitor();
    }
    public void addActivityMonitor(){
        if(mApp == null) return;
        mApp.registerActivityLifecycleCallbacks(new Application.ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
                if(mActivityMonitor != null){
                    mActivityMonitor.onActivityCreated(activity,savedInstanceState);
                }
            }
            public void onActivityResumed(Activity activity) {
                if(mActivityMonitor != null){
                    mActivityMonitor.onActivityResumed(activity);
                }

                try {
                    mGapSecond = Integer.parseInt(AdSwitchUtils.Vs.kp_gap_second.value);
                } catch (NumberFormatException e) {
                    if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, StringUtils.getExcetionInfo(e));
                    //e.printStackTrace();
                }

                //debug模式忽略最小间隔的秒数，用于0-30秒的测试
                if(mGapSecond < 30 && !BaseUtils.getIsDebug(mApp)){
                    mGapSecond = Mtas.defKpGapSecond;
                }
            }
            public void onActivityPaused(Activity activity) {
                if(mActivityMonitor != null){
                    mActivityMonitor.onActivityPaused(activity);
                }
            }
            public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
                if(mActivityMonitor != null){
                    mActivityMonitor.onActivitySaveInstanceState(activity,outState);
                }
            }
            public void onActivityDestroyed(Activity activity) {
                if(mActivityMonitor != null){
                    mActivityMonitor.onActivityDestroyed(activity);
                }

                if(BaseUtils.isMainUI(activity)){
                    mStartTime = -1;
                }
            }
            public void onActivityStarted(Activity activity) {
                if(mActivityMonitor != null){
                    mActivityMonitor.onActivityStarted(activity);
                }

                if(Spu.isSucK(TAG)) LogUtils.dTag(TAG,mFinalCount +"-->"+activity.getClass().getName());
                //如果mFinalCount ==1，说明是从后台到前台
                if (++mFinalCount == 1){
                    if(mStartTime != -1) {
                        long curGapSecond = (System.currentTimeMillis() - mStartTime) / 1000;
                        if (AppUtils.hasGdtKpPermission(mApp) && curGapSecond > mGapSecond) {
                            //说明从后台回到了前台
                            if(mActivityMonitor != null){
                                mActivityMonitor.bootSplash(mApp, activity);
                                mStartTime = -1;
                            }
                        }
                        String n = activity.getClass().getSimpleName();
                        BaseUtils.popDebugToast(n+"回到App-->"+curGapSecond+"/"+mGapSecond);
                        if(Spu.isSucK(TAG)) LogUtils.dTag(TAG,n+"回到App-->"+curGapSecond+"/"+mGapSecond);
                    }

                    if(mActivityMonitor != null){
                        mActivityMonitor.backApp(mApp, activity);
                    }
                }
            }
            public void onActivityStopped(Activity activity) {
                if(mActivityMonitor != null){
                    mActivityMonitor.onActivityStopped(activity);
                }

                mFinalCount--;
                //如果mFinalCount ==0，说明是前台到后台
                if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, mFinalCount +"-->"+activity.getClass().getName());
                if (mFinalCount == 0){
                    //说明从前台回到了后台
                    String n = activity.getClass().getSimpleName();
                    BaseUtils.popDebugToast(n+"离开App-->"+"/间隔："+mGapSecond+"秒");
                    if(Spu.isSucK(TAG)) LogUtils.dTag(TAG,n+"离开APP-->"+"/间隔："+mGapSecond+"秒");
                    mStartTime = System.currentTimeMillis();

                    if(mActivityMonitor != null){
                        mActivityMonitor.leaveApp(mApp, activity);
                    }
                }
            }
        });
    }

    protected IActivityMonitor mActivityMonitor;
    public interface IActivityMonitor{
        void onActivityCreated(Activity activity, Bundle savedInstanceState);
        void onActivityResumed(Activity activity);
        void onActivityPaused(Activity activity);
        void onActivitySaveInstanceState(Activity activity, Bundle outState);
        void onActivityDestroyed(Activity activity);
        void onActivityStarted(Activity activity);
        void onActivityStopped(Activity activity);

        void leaveApp(Application app, Activity activity);
        void backApp(Application app, Activity activity);
        void bootSplash(Application app, Activity activity);
    }
    public static abstract class DefActivityMonitor implements IActivityMonitor{
        public void onActivityCreated(Activity activity, Bundle savedInstanceState) {}
        public void onActivityResumed(Activity activity) {}
        public void onActivityPaused(Activity activity) {}
        public void onActivitySaveInstanceState(Activity activity, Bundle outState) {}
        public void onActivityDestroyed(Activity activity) {}
        public void onActivityStarted(Activity activity) {}
        public void onActivityStopped(Activity activity) {}

        public void leaveApp(Application app, Activity activity){}
        public void backApp(Application app, Activity activity){}
        public abstract void bootSplash(Application app, Activity activity);
    }

    public static void main(String[] args) {
        Application app = null;
        ActivityMonitor.getInstance(app).addActivityMonitor(new DefActivityMonitor() {
            @Override
            public void bootSplash(Application app, Activity activity) {
                // do some thing...
            }
        });
    }
}
