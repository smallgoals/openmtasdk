package com.xmb.mta.util;

import com.android.core.XSEUtils;
import com.blankj.utilcode.util.LogUtils;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.nil.sdk.utils.Spu;
import com.nil.sdk.utils.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import okhttp3.Response;

public class XmbDataSign {
    private final static String TAG = XmbDataSign.class.getSimpleName();

    // 数据解析（解密、序列化）
    public static ResultBean deSignByResult4Obj(Response response) throws IOException {
        return (ResultBean) deSign2Obj(response, ResultBean.class);
    }
    public static Object deSign2Obj(Response response, Class<?> clazz) throws IOException {
        if(Spu.isSucK(TAG)) LogUtils.wTag(TAG, response.toString());
        Object resultBean = null;
        try {
            String data = deSign(response.body().string());
            resultBean = XMBGson.getGson().fromJson(data, clazz);
        }catch (Exception e){
            e.printStackTrace();
            if(ResultBean.class.isAssignableFrom(clazz)){
                resultBean = new ResultBean(ResultBean.CODE_FAIL, e.toString());
            }
        }
        return resultBean;
    }

    public static <T> ArrayList<T> deSignByResult4List(Class<T> clazz, Response response) throws IOException {
        if(Spu.isSucK(TAG)) LogUtils.wTag(TAG, response.toString());
        String data = null;
        try {
            assert response.body() != null;
            data = deSign(response.body().string());
        }catch (Exception e){
            e.printStackTrace();
        }
        return deSignByResult4List(clazz, data);
    }
    public static <T> ArrayList<T> deSignByResult4List(Class<T> clazz, String result) {
        ArrayList<T> list = null;
        try {
            ResultBean resultBean = XMBGson.getGson().fromJson(result, ResultBean.class);
            String s = resultBean.getResult_data();

            list = new ArrayList<T>();
            JsonArray array = new JsonParser().parse(s).getAsJsonArray();
            for (JsonElement elem : array) {
                list.add(new Gson().fromJson(elem, clazz));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return list;
    }

    public static String deSign(String str) {
        String result = null;
        if(StringUtils.noNullStr(str)){
            result = XSEUtils.dataDecode(str);
            if(Spu.isSucK(TAG)) LogUtils.wTag(TAG+"deSign", str+"\n"+result);
        }
        return result;
    }

    // 数据封装（序列化、加密）
    public static String enSign(String str) {
        String result = null;
        if(StringUtils.noNullStr(str)){
            result = XSEUtils.dataEncode(str);
            if(Spu.isSucK(TAG)) LogUtils.wTag(TAG+"enSign", str+"\n"+result);
        }
        return result;
    }

    public static String buildUrlData(Map<String, String> map){
        map = addDataTrail(map);
        return enSign(XMBGson.getGson().toJson(map));
    }
    public static Map<String, String> addDataTrail(Map<String, String> map){
        return XMBSign.addDataTrail(map);
    }
}
