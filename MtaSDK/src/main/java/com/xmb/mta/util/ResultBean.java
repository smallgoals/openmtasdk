package com.xmb.mta.util;

import android.support.annotation.Keep;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

@Keep
public class ResultBean implements Serializable {
	private static final long serialVersionUID = 5507665592182407382L;

	public static final int CODE_SUC = 200;
	public static final int CODE_FAIL = -1;

	@Expose
	private int result_code;
	@Expose
	private String result_data;

	public ResultBean() {
	}

	public ResultBean(int result_data, String result_des) {
		super();
		this.result_code = result_data;
		this.result_data = result_des;
	}

	@Override
	public String toString() {
		return "ResultBean [result_code=" + result_code + ", result_data=" + result_data + "]";
	}

	public int getResult_code() {
		return result_code;
	}

	public void setResult_code(int result_code) {
		this.result_code = result_code;
	}

	public String getResult_data() {
		return result_data;
	}

	public void setResult_data(String result_data) {
		this.result_data = result_data;
	}

}
