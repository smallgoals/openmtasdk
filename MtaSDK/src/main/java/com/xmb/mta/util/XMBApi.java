package com.xmb.mta.util;


import android.app.ui.FeedbackService;
import android.app.ui.FeedbackUI;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.Utils;
import com.google.gson.reflect.TypeToken;
import com.nil.sdk.ui.vo.FeedBackBean;
import com.nil.sdk.utils.ACacheUtils;
import com.nil.sdk.utils.AppUtils;
import com.nil.sdk.utils.Spu;
import com.nil.sdk.utils.StringUtils;
import com.nil.vvv.utils.AdSwitchUtils;
import com.xmb.mta.vo.OnlineConfigBean;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * API接口<br>
 * 1.把Map数据转换为Josn字符串<br>
 * 2.数据进行UrlEncode处理<br>
 * 3.数据进行加密处理<br>
 */
public class XMBApi {
    private final static String TAG = XMBApi.class.getSimpleName();
    private static final String TYPE_NAME = FeedbackUI.SenderType.user.name();
    private static final String URL_DATA_HEAD = "?data=";

    public static void sendFeedback(final XMBApiCallback<ResultBean> xmbApiCallback, Map<String, String> map, String req_mode) {
        if(!AppUtils.hasAgreePrivacyPolicy()) return;

        try {
            String data = XMBSign.buildUrlData(map);
            if (XMBOkHttp.REQ_MODE_POST.equals(req_mode)) {//pos请求
                Map<String, String> postMap = new HashMap<>();
                postMap.put("data", URLDecoder.decode(data, "UTF-8"));  //POS请求不需要进行Url编码
                XMBOkHttp.doPost(Urls.URL_MTA_FEEDBACK, postMap, new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        if (xmbApiCallback != null) xmbApiCallback.onFailure(call, e);
                    }
                    public void onResponse(Call call, Response response) throws IOException {
                        ResultBean resultBean = XMBSign.deSignByResult4Obj(response);
                        if (xmbApiCallback != null)
                            xmbApiCallback.onResponse(resultBean, call, response);
                    }
                });
            } else {
                XMBOkHttp.doGet(Urls.URL_MTA_FEEDBACK + URL_DATA_HEAD + data, new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        if (xmbApiCallback != null) xmbApiCallback.onFailure(call, e);
                    }
                    public void onResponse(Call call, Response response) throws IOException {
                        ResultBean resultBean = XMBSign.deSignByResult4Obj(response);
                        if (xmbApiCallback != null)
                            xmbApiCallback.onResponse(resultBean, call, response);
                    }
                });
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public static void sendFeedback(final XMBApiCallback<ResultBean> xmbApiCallback, Map<String, String> map) {
        sendFeedback(xmbApiCallback, map, XMBOkHttp.REQ_MODE_POST);
    }
    public static void sendFeedback(Map<String, String> map) {
        sendFeedback(null, map);
    }
    public static void sendFeedback(String contact, String content) {
        Map<String, String> map = new HashMap<>();
        if(contact != null) map.put("contact", contact);
        if(content != null) map.put("content",content);
        map.put("type",TYPE_NAME);
        sendFeedback(map);
    }
    public static void sendFeedback(String type, String diy1, String contact, String content) {
        Map<String, String> map = new HashMap<>();
        if(contact != null) map.put("contact", contact);
        if(content != null) map.put("content",content);
        map.put("type",type);
        map.put("diy1", diy1);
        sendFeedback(map);
    }
    public static void sendFeedback(String type, String diy1, String contact, String content, Map<String, String> map) {
        if(map == null) map = new HashMap<>();
        if(contact != null) map.put("contact", contact);
        if(content != null) map.put("content",content);
        map.put("type",type);
        map.put("diy1", diy1);
        sendFeedback(map);
    }

    public static void sendFeedback(String content) {
        sendFeedback(null, content);
    }

    public static void sendEvent(final XMBApiCallback<ResultBean> xmbApiCallback, Map<String, String> map, String req_mode) {
        if(!AppUtils.hasAgreePrivacyPolicy()) return;

        try {
            String data = XMBSign.buildUrlData(map);

            if (XMBOkHttp.REQ_MODE_POST.equals(req_mode)) {//pos请求
                Map<String, String> postMap = new HashMap<>();
                postMap.put("data", URLDecoder.decode(data, "UTF-8"));  //POS请求不需要进行Url编码
                XMBOkHttp.doPost(Urls.URL_MTA_EVENT, postMap, new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        if (xmbApiCallback != null) xmbApiCallback.onFailure(call, e);
                    }
                    public void onResponse(Call call, Response response) throws IOException {
                        ResultBean resultBean = XMBSign.deSignByResult4Obj(response);
                        if (xmbApiCallback != null)
                            xmbApiCallback.onResponse(resultBean, call, response);
                    }
                });
            } else {
                XMBOkHttp.doGet(Urls.URL_MTA_EVENT + URL_DATA_HEAD + data, new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        if (xmbApiCallback != null) xmbApiCallback.onFailure(call, e);
                    }
                    public void onResponse(Call call, Response response) throws IOException {
                        ResultBean resultBean = XMBSign.deSignByResult4Obj(response);
                        if (xmbApiCallback != null)
                            xmbApiCallback.onResponse(resultBean, call, response);
                    }
                });
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public static void sendEvent(final XMBApiCallback<ResultBean> xmbApiCallback, Map<String, String> map){
        sendEvent(xmbApiCallback, map, XMBOkHttp.REQ_MODE_POST);
    }
    public static void sendEvent(Map<String, String> map) {
        sendEvent(null, map);
    }
    public static void sendEvent(String event_name, String content) {
        Map<String, String> map = new HashMap<>();
        if(event_name != null) map.put("event_name", event_name);
        if(content != null) map.put("content",content);
        map.put("type",TYPE_NAME);
        sendEvent(map);
    }
    public static void sendEvent(String event_name) {
        sendEvent(event_name, null);
    }
    public static void sendEvent(String type, String diy1, String event_name, String content) {
        Map<String, String> map = new HashMap<>();
        if(event_name != null) map.put("event_name", event_name);
        if(content != null) map.put("content",content);
        map.put("type",type);
        map.put("diy1", diy1);
        sendEvent(map);
    }
    public static void sendEvent(String type, String diy1, String event_name, String content, Map<String, String> map) {
        if(map == null) map = new HashMap<>();
        if(event_name != null) map.put("event_name", event_name);
        if(content != null) map.put("content",content);
        map.put("type",type);
        map.put("diy1", diy1);
        sendEvent(map);
    }

    //使用Get形式分段发送大数据，加入事件统计中去：【Post暂时有问题，搁置】
    public static void reportBigData(String content){
        if(!AppUtils.hasAgreePrivacyPolicy()) return;
        reportBigData("event_big_data", content);
    }
    public static void reportBigData(String event_name, String content){
        if(!AppUtils.hasAgreePrivacyPolicy()) return;
        reportBigData(event_name,content,null);
    }
    public static void reportBigData(String event_name, String content, final XMBApiCallback<ResultBean> callback){
        if(!AppUtils.hasAgreePrivacyPolicy()) return;

        long millis = System.currentTimeMillis();
        int perLen = 1500;  //每次最大发送的字符数
        int len = (content == null)? 0 : content.length();
        int nums = (int) (Math.ceil(len*1.0f/perLen));  //向上取整
        for(int i = 0; i < nums; i++){
            int begin = perLen * i;
            int end = begin + perLen;
            if (end >= len) end = len;
            String section = content.substring(begin, end);

            Map<String, String> map = new HashMap<>();
            map.put("event_name", event_name);
            map.put("content",section);
            map.put("type", "event_big_data");
            map.put("diy1", "len="+len+",perLen="+perLen);
            map.put("diy2", millis+"_"+(i+1)+"/"+nums);
            sendEvent(callback, map);
        }
    }

    public static void queryFeedback(final XMBApiCallback<ArrayList<FeedBackBean>> xmbApiCallback) {
        queryFeedback(xmbApiCallback, 10);
    }
    public static void queryFeedback(final XMBApiCallback<ArrayList<FeedBackBean>> xmbApiCallback, int size) {
        if(!AppUtils.hasAgreePrivacyPolicy()) return;

        try {
            Map<String, String> map = new HashMap<>();
            map.put(XMBSign.Cjs.size.name(), size+"");

            String data = XMBSign.buildUrlData(map);
            String url = Urls.URL_MTA_QUERY_FEEDBACK_NEWS + URL_DATA_HEAD + data;

            if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "url-->" + url);

            XMBOkHttp.doGet(url, new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    ArrayList<FeedBackBean> list = (ArrayList<FeedBackBean>) ACacheUtils.getCacheObject(FeedBackBean.CACHE_ARY_KEY);
                    if(list != null) {
                        if (xmbApiCallback != null) xmbApiCallback.onResponse(list, call, null);
                    }else{
                        if (xmbApiCallback != null) xmbApiCallback.onFailure(call, e);
                    }
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    try {
                        final ArrayList<FeedBackBean> list = XMBSign.deSignByResult4List(FeedBackBean.class, response);
                        if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "list-->" + list);
                        //缓存为空时，开一个线程进行文件缓存
                        new Thread(new Runnable() {
                            public void run() {
                                ACacheUtils.setCacheObject(FeedBackBean.CACHE_ARY_KEY, list);
                            }
                        }).start();

                        if (xmbApiCallback != null) xmbApiCallback.onResponse(list, call, response);
                    } catch (IOException e) {
                        e.printStackTrace();
                        if (xmbApiCallback != null) xmbApiCallback.onFailure(call, e);
                    }catch (Exception e){
                        if (xmbApiCallback != null) xmbApiCallback.onFailure(call, e);
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            // 出现异常，清除全部通知，并暂停后台服务
            FeedbackService.cleanNotification();
            e.printStackTrace();
        }
    }

    public static void updateOnlineConfig(final XMBApiCallback<ArrayList<OnlineConfigBean>> xmbApiCallback, final String app_key) {
        //未同意隐私政策 且 禁用提前加载xmb在线参数，直接返回（不拉取在线参数）
        if(!AppUtils.hasAgreePrivacyPolicy() && XmbOnlineConfigAgent.noPreloadXmbConfig()) return;

        if(StringUtils.isNullStr(app_key)) return;
        if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "updateOnlineConfig--->begin::"+Thread.currentThread().getId());
        try {
            Map<String, String> map = new HashMap<>();
            map.put(XMBSign.Cjs.app_key.name(), app_key);

            String data = XMBSign.buildUrlData(map);
            String url = Urls.URL_MTA_QUERY_CONFIG_BY_APP_KEY + URL_DATA_HEAD + data;
            if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "url-->" + url);
            //if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "map-->" + map);

            final String cacheKey = XmbOnlineConfigAgent.getCacheKey(app_key);   //缓存Key名称为app_key的MD5
            //LogUtils.eTag(TAG, "cacheKey.write="+cacheKey);

            XMBOkHttp.doGet(url, new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    ArrayList<OnlineConfigBean> list = XmbOnlineConfigAgent.getCacheDatas(cacheKey);
                    if(list != null) {
                        if (xmbApiCallback != null) xmbApiCallback.onResponse(list, call, null);
                    }else{
                        if (xmbApiCallback != null) xmbApiCallback.onFailure(call, e);
                    }
                }
                public void onResponse(Call call, Response response){
                    try {
                        String r = response.body().string();
                        if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "updateOnlineConfig.r-->" + r);
                        ResultBean resultBean = XMBGson.getGson().fromJson(r, ResultBean.class);
                        if(resultBean.getResult_code() == ResultBean.CODE_SUC) {
                            //if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "updateOnlineConfig.resultBean-->" + resultBean);
                            String result = resultBean.getResult_data();

                            Type type = new TypeToken<List<OnlineConfigBean>>() {}.getType();
                            final ArrayList<OnlineConfigBean> list = XMBGson.getGson().fromJson(result, type);
                            //if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "updateOnlineConfig.list-->" + list);

                            //缓存为空时，开一个线程进行文件缓存
                            new Thread(new Runnable() {
                                public void run() {
                                    // 仅缓存在APP包名目录下，不缓存SD卡
                                    ACacheUtils.setAppCacheObject(cacheKey, list);
                                    // 缓存最近更新时间
                                    String tKey = XmbOnlineConfigAgent.getLastUpdateTimeKey(app_key, XmbOnlineConfigAgent.LAST_UPDATE_KEY);
                                    ACacheUtils.setAppCacheValue(tKey, System.currentTimeMillis()+"");

                                    // 【同步更新，永久缓存】只缓存请求成功的数据，并且存储在SD卡和包名目录下
                                    //ACacheUtils.setCacheObject(XmbOnlineConfigAgent.PRE_CACHE_KEY, list);

                                    // 缓存Map对象，避免多次转换
                                    HashMap<String, String> maps = XmbOnlineConfigAgent.ary2Map(list);
                                    ACacheUtils.setAppCacheObject(XmbOnlineConfigAgent.getCacheMapKey(app_key), maps);

                                    // 在线参数获取成功，进行开关配置
                                    AdSwitchUtils.getInstance(Utils.getApp()).initAdInfo(false);
                                }
                            }).start();

                            if (xmbApiCallback != null) xmbApiCallback.onResponse(list, call, response);
                        }else if(resultBean.getResult_code() == ResultBean.CODE_FAIL){
                            if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "updateOnlineConfig.resultBean-->" + resultBean);
                            final ArrayList<OnlineConfigBean> list = new ArrayList<>();
                            String result = resultBean.getResult_data();
                            if(StringUtils.equalsIgnoreCase(result, XmbOnlineConfigAgent.CODE_APP_KEY_ERROR)
                                || StringUtils.equalsIgnoreCase(result, XmbOnlineConfigAgent.CODE_UNKNOW_ERROR)){
                                // 缓存最近更新时间
                                String tKey = XmbOnlineConfigAgent.getLastUpdateTimeKey(app_key, XmbOnlineConfigAgent.LAST_UPDATE_ERROR_KEY);
                                ACacheUtils.setAppCacheValue(tKey, System.currentTimeMillis()+"");
                            }

                            if (xmbApiCallback != null) xmbApiCallback.onResponse(list, call, response);
                        }
                    }catch (Exception e){
                        if (xmbApiCallback != null) xmbApiCallback.onFailure(call, e);
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(Spu.isSucK(TAG)) LogUtils.dTag(TAG, "updateOnlineConfig--->end");
    }
}
