## 需要定期更新的内容：
### 第三方库：
【已更新的库】
// 友盟统计SDK：https://developer.umeng.com/docs/119267/detail/118584#h3-maven-1
api 'com.umeng.umsdk:common:9.6.6' // 统计基础库(必选)
api 'com.umeng.umsdk:asms:1.8.0' // asms包依赖(必选)
api 'com.umeng.umsdk:abtest:1.0.1' //在线参数依赖ABTest模块(必选)
//应用性能监控SDK：U-APM合规指南 https://developer.umeng.com/docs/193624/detail/194588
api 'com.umeng.umsdk:apm:1.9.4' //崩溃异常使用U-APM产品包依赖(必选)

【暂不更新的库】
//android util:https://github.com/Blankj/AndroidUtilCode
api 'com.blankj:utilcode:1.30.6'

//AgentWeb 是一个基于的 Android WebView:https://github.com/Justson/AgentWeb
api 'com.github.Justson.AgentWeb:agentweb-core:v5.0.0-alpha' // (必选)
api 'com.github.Justson.AgentWeb:agentweb-filechooser:v5.0.0-alpha' // (可选)
api 'com.github.Justson:Downloader:v5.0.0' // (可选)

### APP说明
存放APP内部缓存：在线参数、ViPConfigVO
存放APP内部File：登录信息、订单信息
存放外部SD卡：FeedBackBean.CACHE_ARY_KEY/NEWS_CONTACT_KEY/CHECK_VIP_TYPE_KEY/IMEI/login_state/app_key/OrderBeanV2.Cache_KEY_V2_ARY/Install_Date_Key

### 友盟事件
event_name, comment, 0
install_date, 安装日期, 0
pay_success, 支付成功, 0
rate_ok, 确认好评, 0
rate_no, 取消好评, 0
update_apk, APP更新, 0
share_apk, APP分享, 0
user_destroy_success, 销户成功, 0
user_destroy_fail, 销户失败, 0

### 引用说明
//XMB的SDK统计库：
implementation 'org.bitbucket.smallgoals:openmtasdk:23.12.12'

//XMB的SDK统计库(Androidx)：
implementation 'org.bitbucket.smallgoals:openmtasdkx:23.12.12'