package com.android.MtaDemo;

import android.Manifest;
import android.app.Activity;
import android.app.ui.AboutUI;
import android.app.ui.AgreementUI;
import android.app.ui.FeedbackService;
import android.app.ui.OnlineWebUI;
import android.app.ui.PermissionTransferUI;
import android.app.ui.VersionLimitUI;
import android.app.ui.WebHtmlUI;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bd.utils.BdSspWeb;
import com.blankj.utilcode.util.ToastUtils;
import com.nil.crash.utils.CrashApp;
import com.nil.sdk.nb.utils.NbFileUtils;
import com.nil.sdk.ui.BaseAppCompatActivity;
import com.nil.sdk.ui.BaseUtils;
import com.nil.sdk.ui.aid.MyTipDialog;
import com.nil.sdk.utils.ACacheUtils;
import com.nil.sdk.utils.AppUtils;
import com.nil.sdk.utils.AskPermissionUtil;
import com.nil.sdk.utils.DateUtils;
import com.nil.sdk.utils.MobileInfoUtil;
import com.nil.sdk.utils.Spu;
import com.nil.sdk.utils.StringUtils;
import com.nil.vvv.utils.AdSwitchUtils;
import com.nil.vvv.utils.AdSwitchUtils.Ads;
import com.nil.vvv.utils.AdSwitchUtils.Sws;
import com.nil.vvv.utils.ZzAdUtils;
import com.xmb.mta.util.ResultBean;
import com.xmb.mta.util.Urls;
import com.xmb.mta.util.XMBApi;
import com.xmb.mta.util.XMBApiCallback;
import com.xvx.sdk.payment.PayVipActivity;
import com.xvx.sdk.payment.UserLoginActivity;
import com.xvx.sdk.payment.UserRegisterActivity;
import com.xvx.sdk.payment.UserUpdatePskActivity;
import com.xvx.sdk.payment.db.UserDb;
import com.xvx.sdk.payment.db.UserLoginDb;
import com.xvx.sdk.payment.utils.PayUtils;
import com.xvx.sdk.payment.vo.OrderBeanV2;
import com.xvx.sdk.payment.web.PayWebAPI;

import java.util.Date;
import java.util.List;

import okhttp3.Call;
import okhttp3.Response;

public class PayMainActivity extends BaseAppCompatActivity {
    TextView tvContent;
    EditText etUserDataType,etUserDataContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_pay);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*String appName = AppUtils.getAppName(getActivity());
                String appInfo = AppUtils.getPackageInfo(getActivity(), AppUtils.getPackageInfo(getActivity(), getPackageName()));
                MyTipDialog.popNilDialog(getActivity(), "欢迎使用\n"+appInfo);*/

                BaseUtils.showInnerLog(getActivity());
            }
        });
        tvContent = findViewById(R.id.tvContent);
        //tvContent.setText(MobileInfoUtil.getInstance(getActivity()).getAllInfo());
        tvContent.setText(getSws());
        tvContent.setMovementMethod(ScrollingMovementMethod.getInstance());
        tvContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TO-DO:
            }
        });

        // 启动反馈消息的接收，启动后台服务
        //FeedbackService.addNotification();

        //提前查询或更新订单信息，并本地缓存
        /*OrderBeanV2.updateOrderBean(new PayWebApiCallback<ResultBean>() {
            @Override
            public void onFailure(Call call, Exception e) {}
            public void onResponse(ResultBean obj, Call call, Response response) {
                tvContent.setText(getSws());
            }
        }, null);*/

        EditText etUrl = findViewById(R.id.et_server_url);
        String dUrl = ACacheUtils.getAppFileCacheValue("debug_xmb_server_base_url");
        if(StringUtils.isNullStr(dUrl)){
            dUrl = Urls.URL_TEST;
        }
        etUrl.setText(dUrl);

        Button btnUrl = findViewById(R.id.btn_server_url);
        btnUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String u = etUrl.getText().toString();
                if(StringUtils.isNullStr(u)) {
                    ToastUtils.showLong("服务器地址不能为空");
                } else if(!NbFileUtils.isUrl(u)){
                    ToastUtils.showLong("服务器地址不合法");
                }else{
                    ACacheUtils.setAppFileCacheValue("debug_xmb_server_base_url", u);
                    ToastUtils.showLong("服务器地址更新成功");
                }
            }
        });

        etUserDataType = findViewById(R.id.et_user_data_type);
        etUserDataContent = findViewById(R.id.et_user_data_content);
    }

    protected void onResume() {
        super.onResume();

        PayUtils.onResume(this);
        tvContent.setText(getSws());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        FeedbackService.cleanNotification();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_rate) {//市场好评
            if(!BaseUtils.hasGoodRate(getActivity())){
                BaseUtils.popMyToast(getActivity(), "已经好评了或者好评开关已关！");
            }
            return true;
        }else if(id == R.id.action_appinfo){//应用信息
            BaseUtils.showInnerLog(getActivity());
            return true;
        }else if(id == R.id.action_crash){//异常捕获测试
            System.loadLibrary("abc.so");
            Activity act = null;
            act.runOnUiThread(null);
            return true;
        }else if(id == R.id.action_feedback){//反馈建议
            BaseUtils.gotoFeedbackUI(getActivity());
            return true;
        }else if(id == R.id.action_update){//在线更新
            BaseUtils.checkUpdate(getActivity());
            return true;
        }else if(id == R.id.action_gradleinfo){//gradle信息获取
            MyTipDialog.popNilDialog(getActivity(), "gradleinfo="+BaseUtils.getGradleValue(getActivity(),
                    "gradle.properties", "versionName"));
            return true;
        }else if(id == R.id.action_open_url){//打开指定Url
            AdSwitchUtils.Sws.Open_url.flag = true;
            if(StringUtils.isNullStr(AdSwitchUtils.Vs.open_url.value)){
                AdSwitchUtils.Vs.open_url.value = "http://app.qq.com/";
            }
            Spu.saveVMd5(getActivity(), AdSwitchUtils.Vs.open_url.value, "");
            BaseUtils.openUrl(getActivity());
            return true;
        }else if(id == R.id.action_download_url){//下载一个文件
            AdSwitchUtils.Sws.Download_url.flag = true;
            if(StringUtils.isNullStr(AdSwitchUtils.Vs.download_url.value)) {
                AdSwitchUtils.Vs.download_url.value = "http://imtt.dd.qq.com/16891/channel_70316555_1000047_1537235490271.apk";
            }
            Spu.saveVMd5(getActivity(), AdSwitchUtils.Vs.download_url.value, "");
            BaseUtils.downloadUrl(getActivity());
            return true;
        }else if(id == R.id.action_get_apps){//获取全部应用信息
            BaseUtils.popNilAppsDlg(getActivity());
            return true;
        }else if(id == R.id.action_get_hongbao){//每日领红包
            AdSwitchUtils.Sws.Zfb_url.flag = true;
            Spu.saveVDay(getActivity(), Spu.VSpu.zfb_url.name(), "");
            BaseUtils.openZfbUrl(getActivity());
            return true;
        }else if(id == R.id.action_big_data){//大数据测试
            BaseUtils.popEditDlg(getActivity(), "大数据测试", new MyTipDialog.DialogMethod() {
                @Override
                public void sure(){}
                public void sure(Object obj) {
                    XMBApi.reportBigData("demo_test", (String) obj);
                    BaseUtils.popMyToast(getActivity(),"大数据测试发送完成！");
                }
            });
            return true;
        }else if(id == R.id.action_get_moblieinfo){//查看手机版本信息
            BaseUtils.showInnerLog(this, MobileInfoUtil.getInstance(this).getAllBuildInformation());
            return true;
        }else if(id == R.id.action_version_limit){//进入版本限制
            VersionLimitUI.start(getActivity());
            return true;
        }
        else if(id == R.id.action_open_crazy_url){//打开疯狂Url
            BaseUtils.openCrazyUrl(getActivity());
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /*@Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        int rd = new Random().nextInt(100);
        if(rd % 2 == 0){
            BaseUtils.onExitNormal(getActivity(), true);
            return true;
        }
        return BaseUtils.onKeyDown(getActivity(), keyCode, event);
    }*/

    public void onClickCustom(View view) {
        if(view instanceof Button){
            String v = ((Button) view).getText().toString().trim();
            if(v.equals("备用")){
                BaseUtils.popMyToast(getActivity(), "备用按钮，没任何事件，请自行添加！");
            }else if(v.equals("启动")){
                PermissionTransferUI.start(getActivity(),
                        new PermissionTransferUI.PtVO()
                                .setTitle("前往支付")
                                .setImgLogoID(R.drawable.xpay_pay_icon_vip)
                                .setiBgColor(Color.TRANSPARENT).setiScaleType(ImageView.ScaleType.CENTER_CROP).setiVisibility(View.VISIBLE)
                                .setLayoutResID(R.layout.xmta_permission_transfer_ui)
                                .setcTextSize(20).setcTextColor(0xaf000000/*Color.GRAY*/).setcBgColor(Color.TRANSPARENT).setcGravity(Gravity.LEFT)
                                .setContent("\n\n\n\n温馨提醒：为了正常进行支付，需要您授权存储和相机权限~\n\n\n\n")
                                .setCancel("返回")
                                .setOk("去付款"),
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                        new AskPermissionUtil.DefFullCallback() {
                            @Override
                            public void onGranted(List<String> list) {
                                PayVipActivity.start(getActivity());
                            }
                        });
            }else if(v.equals("条款")){
                String [] pers = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
                if(!AppUtils.hasPermission(this, pers)) {
                    AskPermissionUtil.popPermissionTipDlg(getActivity(), "权限申请", "需要存储权限", pers
                            , new AskPermissionUtil.DefFullCallback() {
                                @Override
                                public void onGranted(List<String> permissionsGranted) {
                                    ToastUtils.showLong("存储权限申请成功！");
                                }
                            });
                    return;
                }
                
                Spu.saveV(getActivity(), Spu.VSpu.agreement.name(), "no");
                BaseUtils.gotoActivity(getActivity(), AgreementUI.class.getName(), false);
            }else if(v.equals("WEB")){
//                BaseUtils.gotoActivity(getActivity(), OnlineWebUI.class.getName(), false);
                OnlineWebUI.start(getActivity(), BdSspWeb.sppd, "小视频");
            }else if(v.equals("关于")){
                BaseUtils.gotoActivity(getActivity(), AboutUI.class.getName(), false);
            }else if(v.equals("反馈")){
//                BaseUtils.gotoActivity(getActivity(), FeedbackUI.class.getName(), false);
                BaseUtils.gotoFeedbackUI(getActivity());
            }else if(v.equals("政策")){
                WebHtmlUI.startWithAssets(getActivity(), "用户隐私政策");
            }else if(v.equals("升级会员")){
                //PayUtils.gotoBuyViPUI(this);

                //直接进入支付：
                PayVipActivity.start(this);
            }else if(v.equals("登录")){
                BaseUtils.startActivity(UserLoginActivity.class);
            }else if(v.equals("注销")){
                MyTipDialog.popDialog(this, "注销", "确认要退出当前登录的账号？","确认","取消",new MyTipDialog.DialogMethod(){
                    public void sure() {
                        UserLoginActivity.logoutToLoginUI();
                    }
                });
            }else if(v.equals("注册")){
                BaseUtils.startActivity(UserRegisterActivity.class);
            }else if(v.equals("改密")){
                BaseUtils.startActivity(UserUpdatePskActivity.class);
            }else if(v.equals("个人中心")){
                PayUtils.gotoPersonalDataUI(this);
            }else if(v.equals("开启推送")){
                ZzAdUtils.openPushAd(getActivity());
            }else if(v.equals("关闭推送")){
                ZzAdUtils.closePushAd(getActivity());
            }


            if(v.equals("Kp")){
                AdSwitchUtils.getInstance(this).setCurChannelValue(Ads.Qq.open(), Sws.Kp.open(), Sws.Hf.close(),Sws.Cp.close());
            }else if(v.equals("KpHf")){
                AdSwitchUtils.getInstance(this).setCurChannelValue(Sws.Kp.open(), Sws.Hf.open(),Sws.Cp.close());
            }else if(v.equals("KpHfCp")){
                AdSwitchUtils.getInstance(this).setCurChannelValue(Sws.Kp.open(), Sws.Hf.open(),Sws.Cp.open());
            }else if(v.equals("Open")){
                AdSwitchUtils.getInstance(this).setVersionSpValue("open");
            }else if(v.equals("Close")){
                //AdSwitchUtils.getInstance(this).setVersionSpValue("close");
                AdSwitchUtils.getInstance(this).setCurChannelValue(Sws.All.close(), Ads.Qq.close());
            }else if(v.equals("Clear")){
                AdSwitchUtils.getInstance(this).setVersionSpValue(null);
            }else if(v.equals("更")){
                String data_type = etUserDataType.getText().toString();
                String userdata = etUserDataContent.getText().toString();
                if(StringUtils.noNullStr(data_type) && StringUtils.noNullStr(userdata)) {
                    PayWebAPI.updateUserData(new XMBApiCallback<ResultBean>() {
                        @Override
                        public void onFailure(Call call, Exception e) {
                            BaseUtils.popMyToast("updateUserData is error!");
                        }
                        public void onResponse(ResultBean obj, Call call, Response response) {
                            CrashApp.mainThread(new Runnable() {
                                @Override
                                public void run() {
                                    if(obj != null){
                                        MyTipDialog.popDialog(getActivity(), obj);
                                    }
                                }
                            });
                        }
                    }, data_type, userdata);
                }else{
                    BaseUtils.popMyToast("data_type or userdata is null.");
                }
            }else if(v.equals("删")){
                String data_type = etUserDataType.getText().toString();
                if(StringUtils.noNullStr(data_type)) {
                    PayWebAPI.deleteUserData(new XMBApiCallback<ResultBean>() {
                        @Override
                        public void onFailure(Call call, Exception e) {
                            BaseUtils.popMyToast("deleteUserData is error!");
                        }
                        public void onResponse(ResultBean obj, Call call, Response response) {
                            CrashApp.mainThread(new Runnable() {
                                @Override
                                public void run() {
                                    if(obj != null){
                                        MyTipDialog.popDialog(getActivity(), obj);
                                    }
                                }
                            });
                        }
                    }, data_type);
                }else{
                    BaseUtils.popMyToast("data_type is null.");
                }
            }else if(v.equals("查")){
                String data_type = etUserDataType.getText().toString();
                if(StringUtils.noNullStr(data_type)) {
                    PayWebAPI.getUserData(new XMBApiCallback<ResultBean>() {
                        @Override
                        public void onFailure(Call call, Exception e) {
                            BaseUtils.popMyToast("getUserData is error!");
                        }
                        public void onResponse(ResultBean obj, Call call, Response response) {
                            CrashApp.mainThread(new Runnable() {
                                @Override
                                public void run() {
                                    if(obj != null){
                                        MyTipDialog.popDialog(getActivity(), obj);
                                    }
                                }
                            });
                        }
                    }, data_type);
                }else{
                    BaseUtils.popMyToast("data_type is null.");
                }
            }

            tvContent.setText(getSws());
        }
    }

    public String getSws(){
        StringBuffer buf =  new StringBuffer();
        /*buf.append(Sws.toAllString()).append("\n");
        buf.append("\n");
        buf.append(AdSwitchUtils.getInstance(this).getVersionValueInfo()).append("\n");
        buf.append("\n");*/

        Date curDate = new Date();
        Date netDate = (Date) ACacheUtils.getAppFileCacheObject(PayUtils.XPAY_NET_TIME_KEY);
        String dd = String.format("当前时间：%s\n网络时间：%s（缓存一天，清缓存刷新）\n",
                DateUtils.getStringByFormat(curDate, DateUtils.dateFormatYMDHMS),
                DateUtils.getStringByFormat(netDate, DateUtils.dateFormatYMDHMS));
        buf.append(dd).append("\n");

        buf.append("用户登录信息：").append("\n");
        buf.append(UserLoginDb.get()).append("\n");
        buf.append("\n");

        buf.append("用户信息：").append("\n");
        buf.append(UserDb.get()).append("\n");
        buf.append("\n");

        buf.append("用户订单信息：").append("\n");
        buf.append(OrderBeanV2.getOrderBeans()).append("\n");
        buf.append("\n");

        return buf.toString();
    }
}
