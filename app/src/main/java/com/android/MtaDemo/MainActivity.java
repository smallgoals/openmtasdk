package com.android.MtaDemo;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ui.AboutUI;
import android.app.ui.AgreementUI;
import android.app.ui.FeedbackService;
import android.app.ui.LaunchUI;
import android.app.ui.OnlineWebUI;
import android.app.ui.WebHtmlActivity;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.bd.utils.BdSspWeb;
import com.nil.sdk.ui.BaseAppCompatActivity;
import com.nil.sdk.ui.BaseUtils;
import com.nil.sdk.ui.aid.MyTipDialog;
import com.nil.sdk.utils.ACacheUtils;
import com.nil.sdk.utils.AppUtils;
import com.nil.sdk.utils.MobileInfoUtil;
import com.nil.sdk.utils.Spu;
import com.nil.sdk.utils.StringUtils;
import com.nil.vvv.utils.AdSwitchUtils;
import com.nil.vvv.utils.AdSwitchUtils.Ads;
import com.nil.vvv.utils.AdSwitchUtils.Sws;
import com.xmb.mta.util.XMBApi;
import com.xmb.mta.util.XmbOnlineConfigAgent;

@Deprecated
public class MainActivity extends BaseAppCompatActivity {
    TextView tvContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String appName = AppUtils.getAppName(getActivity());
                String appInfo = AppUtils.getPackageInfo(getActivity(), AppUtils.getPackageInfo(getActivity(), getPackageName()));
                MyTipDialog.popNilDialog(getActivity(), "欢迎使用\n"+appInfo);
            }
        });
        tvContent = findViewById(R.id.tvContent);
        //tvContent.setText(MobileInfoUtil.getInstance(getActivity()).getAllInfo());
        tvContent.setText(getSws());
        tvContent.setMovementMethod(ScrollingMovementMethod.getInstance());
        tvContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TO-DO:
            }
        });

        // 启动反馈消息的接收，启动后台服务
        //FeedbackService.addNotification();

        //BaseUtils.openLogOut(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        FeedbackService.cleanNotification();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @SuppressLint("MissingPermission")
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_rate) {//市场好评
            if(!BaseUtils.hasGoodRate(getActivity())){
                BaseUtils.popMyToast(getActivity(), "已经好评了或者好评开关已关！");
            }
            return true;
        }else if(id == R.id.action_appinfo){//应用信息
            BaseUtils.showInnerLog(getActivity(),
                    XmbOnlineConfigAgent.ary2Map(),
                    "\n",
                    BaseUtils.getAppInfo(getActivity()),
                    /*PhoneUtils.getPhoneStatus(),*/ACacheUtils.getCode()
            );
            return true;
        }else if(id == R.id.action_crash){//异常捕获测试
            System.loadLibrary("abc.so");
            Activity act = null;
            act.runOnUiThread(null);
            return true;
        }else if(id == R.id.action_feedback){//反馈建议
            /*BaseUtils.gotoFeedbackUI(getActivity(), new FeedbackUI.DefFeedbackCallback() {
                @Override
                public void handleFeedback(Activity ctx, String relation, String feedback) {
                    NbToast.showToast("testss...");
                    BaseUtils.handleFeedback(getActivity(), relation+"##"+feedback);
                }
            });*/

            /*FeedbackUI.FeedbackVo fd = new FeedbackUI.FeedbackVo(FeedbackUI.FeedbackType.Integrated.name(),
                    FeedbackUI.FeedbackDiy1.Feedback.name(), "用户反馈");
            fd.setContentTitle("* 反馈内容");
            fd.setContentHint("请在此描述您反馈的详细内容（必填）");
            fd.setButtonName("提交");
            FeedbackUI.start(getActivity(), fd);*/

            BaseUtils.gotoFeedbackUI(getActivity());
            return true;
        }else if(id == R.id.action_update){//在线更新
            BaseUtils.checkUpdate(getActivity());
            return true;
        }else if(id == R.id.action_gradleinfo){//gradle信息获取
            MyTipDialog.popNilDialog(getActivity(), "gradleinfo="+BaseUtils.getGradleValue(getActivity(),
                    "gradle.properties", "versionName"));
            return true;
        }else if(id == R.id.action_open_url){//打开一个url
            AdSwitchUtils.Sws.Open_url.flag = true;
            if(StringUtils.isNullStr(AdSwitchUtils.Vs.open_url.value)){
                AdSwitchUtils.Vs.open_url.value = "http://app.qq.com/";
            }
            Spu.saveVMd5(getActivity(), AdSwitchUtils.Vs.open_url.value, "");
            BaseUtils.openUrl(getActivity());
            return true;
        }else if(id == R.id.action_download_url){//下载一个文件
            AdSwitchUtils.Sws.Download_url.flag = true;
            if(StringUtils.isNullStr(AdSwitchUtils.Vs.download_url.value)) {
                AdSwitchUtils.Vs.download_url.value = "http://imtt.dd.qq.com/16891/channel_70316555_1000047_1537235490271.apk";
            }
            Spu.saveVMd5(getActivity(), AdSwitchUtils.Vs.download_url.value, "");
            BaseUtils.downloadUrl(getActivity());
            return true;
        }else if(id == R.id.action_get_apps){//获取全部应用信息
            BaseUtils.popNilAppsDlg(getActivity());
            return true;
        }else if(id == R.id.action_get_hongbao){//每日领红包
            AdSwitchUtils.Sws.Zfb_url.flag = true;
            Spu.saveVDay(getActivity(), Spu.VSpu.zfb_url.name(), "");
            BaseUtils.openZfbUrl(getActivity());
            return true;
        }else if(id == R.id.action_big_data){//大数据测试
            BaseUtils.popEditDlg(getActivity(), "大数据测试", new MyTipDialog.DialogMethod() {
                @Override
                public void sure(){}
                public void sure(Object obj) {
                    XMBApi.reportBigData("demo_test", (String) obj);
                    BaseUtils.popMyToast(getActivity(),"大数据测试发送完成！");
                }
            });
            return true;
        }else if(id == R.id.action_get_moblieinfo){//查看手机版本信息
            BaseUtils.showInnerLog(this, MobileInfoUtil.getInstance(this).getAllBuildInformation());
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /*@Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        int rd = new Random().nextInt(100);
        if(rd % 2 == 0){
            BaseUtils.onExitNormal(getActivity(), true);
            return true;
        }
        return BaseUtils.onKeyDown(getActivity(), keyCode, event);
    }*/

    public void onClickCustom(View view) {
        if(view instanceof Button){
            String v = ((Button) view).getText().toString().trim();
            if(v.equals("备用")){
                BaseUtils.popMyToast(getActivity(), "备用按钮，没任何事件，请自行添加！");
            }else if(v.equals("启动")){
                BaseUtils.gotoActivity(getActivity(), LaunchUI.class.getName());
            }else if(v.equals("条款")){
                Spu.saveV(getActivity(), Spu.VSpu.agreement.name(), "no");
                BaseUtils.gotoActivity(getActivity(), AgreementUI.class.getName(), false);
            }else if(v.equals("WEB")){
//                BaseUtils.gotoActivity(getActivity(), OnlineWebUI.class.getName(), false);
                OnlineWebUI.start(getActivity(), BdSspWeb.sppd, "小视频");
            }else if(v.equals("关于")){
                BaseUtils.gotoActivity(getActivity(), AboutUI.class.getName(), false);
            }else if(v.equals("反馈")){
//                BaseUtils.gotoActivity(getActivity(), FeedbackUI.class.getName(), false);
                BaseUtils.gotoFeedbackUI(getActivity());
            }else if(v.equals("政策")){
                WebHtmlActivity.startWithAssets(getActivity(), "用户隐私政策");
            }

            if(v.equals("Kp")){
                AdSwitchUtils.getInstance(this).setCurChannelValue(Ads.Qq.open(), Sws.Kp.open(), Sws.Hf.close(),Sws.Cp.close());
            }else if(v.equals("KpHf")){
                AdSwitchUtils.getInstance(this).setCurChannelValue(Sws.Kp.open(), Sws.Hf.open(),Sws.Cp.close());
            }else if(v.equals("KpHfCp")){
                AdSwitchUtils.getInstance(this).setCurChannelValue(Sws.Kp.open(), Sws.Hf.open(),Sws.Cp.open());
            }else if(v.equals("Open")){
                AdSwitchUtils.getInstance(this).setVersionSpValue("open");
            }else if(v.equals("Close")){
                //AdSwitchUtils.getInstance(this).setVersionSpValue("close");
                AdSwitchUtils.getInstance(this).setCurChannelValue(Sws.All.close(), Ads.Qq.close());
            }else if(v.equals("Clear")){
                AdSwitchUtils.getInstance(this).setVersionSpValue(null);
            }

            tvContent.setText(getSws());
        }
    }

    public String getSws(){
        StringBuffer buf =  new StringBuffer();
        buf.append(Sws.toAllString()).append("\n");
        buf.append("\n");
        buf.append(AdSwitchUtils.getInstance(this).getVersionValueInfo()).append("\n");
        buf.append("\n");
        return buf.toString();
    }
}
